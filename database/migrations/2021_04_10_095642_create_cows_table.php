<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cows', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_detail_id')->nullable();
            $table->integer('user_id');
            $table->integer('farm_id');
            $table->integer('specie_id');
            $table->string('rfid')->nullable()->length(50);
            $table->string('name');
            $table->enum('type', ['mom', 'dad', 'cattle']);
            $table->string('name_dad')->nullable();
            $table->integer('specie_dad')->nullable();
            $table->string('name_dad_dad')->nullable();
            $table->integer('specie_dad_dad')->nullable();
            $table->string('name_mom_dad')->nullable();
            $table->integer('specie_mom_dad')->nullable();
            $table->string('name_mom')->nullable();
            $table->integer('specie_mom')->nullable();
            $table->string('name_mom_mom')->nullable();
            $table->integer('specie_mom_mom')->nullable();
            $table->string('name_dad_mom')->nullable();
            $table->integer('specie_dad_mom')->nullable();
            $table->date('birth');
            $table->enum('sex', ['male', 'female']);
            $table->enum('status', ['alive', 'dead'])->default('alive');
            $table->string('F5name_dad')->nullable();
            $table->integer('F5specie_dad')->nullable();
            $table->string('f5blood_dad')->nullable();
            $table->string('F5name_mom')->nullable();
            $table->integer('F5specie_mom')->nullable();
            $table->string('f5blood_mom')->nullable();
            $table->string('f4name_dad')->nullable();
            $table->integer('f4specie_dad')->nullable();
            $table->string('f4blood_dad')->nullable();
            $table->string('f4name_mom')->nullable();
            $table->integer('f4specie_mom')->nullable();
            $table->string('f4blood_mom')->nullable();
            $table->string('f3blood_dad')->nullable();
            $table->string('f3blood_mom')->nullable();
            $table->string('f2blood_dad')->nullable();
            $table->string('f2blood_mom')->nullable();
            $table->string('f1blood_dad')->nullable();
            $table->string('f1blood_mom')->nullable();
            $table->integer('pedigree_level')->default(0);
            $table->string('grade')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cows');
    }
}
