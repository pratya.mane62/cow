<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFarmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('farms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_detail_id')->nullable();
            $table->integer('user_id');
            $table->text('standard')->nullable();
            $table->text('traceback')->nullable();
            $table->text('photo')->nullable();
            $table->float('latlong')->nullable();
            $table->enum('type', ['farm', 'slaughterhouse']);
            $table->string('title');
            $table->string('address');
            $table->string('district');
            $table->string('amphoe');
            $table->string('province');
            $table->string('zipcode');
            $table->string('enterpriseser_id')->nullable();
            $table->string('land_use')->nullable();
            $table->string('canalization');
            $table->string('quantity');
            $table->string('model');
            $table->string('parenting');
            $table->string('mom');
            $table->string('breed');
            $table->string('semen_source');
            $table->string('first_mixed_mom_old');
            $table->string('time_from_birth_to_mating');
            $table->string('cattle');
            $table->string('old_start_cattle');
            $table->string('old_end_cattle');
            $table->string('sale_location');
            $table->string('sale_model');
            $table->string('food_type1');
            $table->string('thick_food1');
            $table->string('protein1');
            $table->string('volume1');
            $table->string('thick_food2');
            $table->string('protein2');
            $table->string('volume2');
            $table->string('food_type2');
            $table->string('thick_food3');
            $table->string('protein3');
            $table->string('volume3');
            $table->string('thick_food4');
            $table->string('protein4');
            $table->string('volume4');
            $table->string('source');
            $table->string('area');
            $table->string('seed');
            $table->string('keep');
            $table->string('cheris');
            $table->boolean('headwaters');
            $table->boolean('middle');
            $table->boolean('downstream');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('farms');
    }
}
