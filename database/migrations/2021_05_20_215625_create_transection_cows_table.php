<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransectionCowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transection_cows', function (Blueprint $table) {
            $table->integer('cow_id');
            $table->integer('farm_id');
            $table->text('photo')->nullable();
            $table->text('photorfid')->nullable();
            $table->float('weight')->nullable();
            $table->integer('deworming_id')->nullable();
            $table->integer('vaccination_id')->nullable();
            $table->date('examination_id')->nullable();
            $table->timestamps();
            $table->primary(['farm_id', 'cow_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transection_cows');
    }
}
