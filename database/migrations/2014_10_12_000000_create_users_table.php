<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone')->unique()->length(10);
            $table->enum('status', ['admin', 'farm', 'slaugh']);
            $table->string('password')->nullable();
            $table->string('name')->nullable();
            $table->enum('sex', ['male', 'female'])->nullable();
            $table->string('card_id')->length(13)->unique();
            $table->string('old')->nullable();
            $table->string('study')->nullable();
            $table->string('profession')->nullable();
            $table->string('earning')->nullable();
            $table->string('address')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
