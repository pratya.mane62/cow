<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class CreatedAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
        	'phone' => '0800000000',
            'card_id' => '0000000000000',
            'status' => 'admin',
        	'password' => bcrypt('12345678')
        ]);
    }
}
