<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isAdmin', function ($user) {
            return $user->status == 'admin';
        });

        Gate::define('isUser', function ($user) {
            return $user->status != 'admin';
        });

        Gate::define('isFarm', function ($user) {
            if ($user->status == 'admin') {
                return true;
            }
            if ($user->status != 'farm') {
                return false;
            }
            if ($user->farms->count() > 0) {
                return  true;
            } else {
                Session::flush();
                Auth::logout();
                return redirect('/')->with('error', 'กรุณาให้แอดมินเพิ่มฟามให้ก่อน');
            }
        });

        Gate::define('isSlaughterhouse', function ($user) {
            if ($user->status == 'admin') {
                return true;
            }
            if ($user->status != 'slaugh') {
                return false;
            }
            if ($user->farms->count() > 0) {
                return  true;
            } else {
                Session::flush();
                Auth::logout();
                return redirect('/')->with('error', 'กรุณาให้แอดมินเพิ่มฟามให้ก่อน');
            }
        });

        Gate::define('isEtp', function ($user) {
            return $user->status == 'admin' || $user->etps->count() > 0;
        });
    }
}
