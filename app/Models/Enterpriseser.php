<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enterpriseser extends Model
{
    use HasFactory;

    protected $fillable = [
        "name",
        "location",
        "coordinates",
    ];

    public function users()
    {
        return $this->BelongsToMany(User::class);
    }
    public function farms()
    {
        return $this->hasMany(Farm::class);
    }
}
