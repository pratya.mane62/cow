<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transection extends Model
{
    use HasFactory;
    protected $fillable = [
        'cow_id',
        'farm_id',
        'photo',
        'weight',
    ];

    public function cows()
    {
        return $this->hasMany(Cow::class, 'id', 'cow_id')->where('status', 'alive')->orderBy('created_at', 'desc');
    }

    public function farms()
    {
        return $this->hasMany(Farm::class, 'id', 'farm_id')->orderBy('created_at', 'desc');
    }
}
