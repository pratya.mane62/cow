<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EnterpriseserUser extends Model
{
    use HasFactory;
    protected $table = 'enterpriseser_user';
    protected $fillable = [
        "user_id",
        "enterpriseser_id",
    ];
}
