<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Breeding extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'cow_id',
        'sperm_id',
        'status',
        'date',
        'time'
    ];
    public function cow()
    {
        return $this->belongsTo(Cow::class);
    }
    public function sperm()
    {
        return $this->belongsTo(Sperm::class);
    }
}
