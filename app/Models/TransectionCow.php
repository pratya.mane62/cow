<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransectionCow extends Model
{
    use HasFactory;
    protected $fillable = [
        'cow_id',
        'farm_id',
        'photo',
        'photorfid',
        'weight',
        'deworming_id',
        'vaccination_id',
        'examination_id',
    ];

    // public function dewormings()
    // {
    //     return $this->select('dewormings.*')->join('dewormings', 'transection_cows.farm_id', 'dewormings.farm_id')->where('transection_cows.cow_id', 'dewormings.cow_id');
    // }
    // public function vaccinations()
    // {
    //     return $this->select('vaccinations.*')->join('vaccinations', 'transection_cows.farm_id', 'vaccinations.farm_id')->where('transection_cows.cow_id', 'vaccinations.cow_id');
    // }
    // public function examinations()
    // {
    //     return $this->select('examinations.*')->join('examinations', 'transection_cows.farm_id', 'examinations.farm_id')->where('transection_cows.cow_id', 'examinations.cow_id');
    // }
    public function cows()
    {
        return $this->hasMany(Cow::class, 'id', 'cow_id')->where('status', 'alive')->orderBy('created_at', 'desc');
    }

    public function farms()
    {
        return $this->hasMany(Farm::class, 'id', 'farm_id')->orderBy('created_at', 'desc');
    }
    public function histories()
    {
        return $this->hasMany(Treatment::class);
    }
}
