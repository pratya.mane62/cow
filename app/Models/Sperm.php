<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sperm extends Model
{
    use HasFactory;
    protected $fillable = ['specie_id', 'bull', 'resource', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function Breedings()
    {
        return $this->hasMany(Breeding::class);
    }
    public function specie()
    {
        return $this->belongsTo(Specie::class);
    }
}
