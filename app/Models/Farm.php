<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Farm extends Model
{
    use HasFactory;
    protected $fillable = [
        'admin_detail_id',
        'user_id',
        'standard',
        'traceback',
        'photo',
        'latlong',
        'type',
        'title',
        'address',
        'enterpriseser_id',
        'land_use',
        'canalization',
        'quantity',
        'model',
        'parenting',
        'mom',
        'breed',
        'semen_source',
        'first_mixed_mom_old',
        'time_from_birth_to_mating',
        'cattle',
        'old_start_cattle',
        'old_end_cattle',
        'sale_location',
        'sale_model',
        'food_type1',
        'thick_food1',
        'protein1',
        'volume1',
        'thick_food2',
        'protein2',
        'volume2',
        'food_type2',
        'thick_food3',
        'protein3',
        'volume3',
        'thick_food4',
        'protein4',
        'volume4',
        'source',
        'area',
        'seed',
        'keep',
        'cheris',
        'district',
        'amphoe',
        'province',
        'zipcode',
        'headwaters',
        'middle',
        'downstream',
    ];

    public function cows()
    {
        return $this->hasMany(Cow::class)->where('status', 'alive')->orderBy('created_at', 'desc');
    }

    public function transection()
    {
        return $this->belongsTo(TransectionCow::class, 'id', 'farm_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function etp()
    {
        return $this->belongsTo(Enterpriseser::class, 'enterpriseser_id');
    }
}
