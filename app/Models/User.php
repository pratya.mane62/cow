<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'card_id',
        'status',
        'password',
        "sex",
        "old",
        "study",
        "profession",
        "earning",
        "address",
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

    // public function admin()
    // {
    //     return $this->hasMany(AdminDetail::class, 'user_id', 'id');
    // }
    public function etps()
    {
        return $this->BelongsToMany(Enterpriseser::class);
    }

    public function farms()
    {
        return $this->hasMany(Farm::class)->orderBy('created_at', 'desc');
    }
    public function sperms()
    {
        return $this->hasMany(Sperm::class, 'id', 'admin_detail_id')->orderBy('created_at', 'desc');
    }
    public function cows()
    {
        return $this->hasMany(Cow::class)->where('status', 'alive');
    }
}
