<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Son extends Model
{
    use HasFactory;
    protected $fillable = [
        'birth',
        'gender',
        'weight',
        'photo',
        'cow_id',
    ];
    public function mom()
    {
        return $this->belongsTo(Cow::class);
    }

    public function age()
    {
        return Carbon::parse($this->attributes['birth'])->diff(Carbon::now())->format('%y ปี, %m เดือน และอีก   %d วัน');
    }
}
