<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cow extends Model
{
    use HasFactory;
    protected $fillable = [
        'admin_detail_id', 'user_id', 'farm_id', 'specie_id', 'rfid', 'name', 'type', 'specie_dad', 'specie_dad_dad', 'specie_mom_dad', 'specie_mom', 'specie_mom_mom', 'specie_dad_mom', 'birth', 'sex', 'status', 'created_at', 'updated_at', 'name_dad', 'name_dad_dad', 'name_mom_dad', 'name_mom', 'name_mom_mom', 'name_dad_mom', 'F5name_dad', 'grade',
        'F5specie_dad',
        'f5blood_dad',
        'F5name_mom',
        'F5specie_mom',
        'f5blood_mom',
        'f4name_dad',
        'f4specie_dad',
        'f4blood_dad',
        'f4name_mom',
        'f4specie_mom',
        'f4blood_mom',
        'f3blood_dad',
        'f3blood_mom',
        'f2blood_dad',
        'f2blood_mom',
        'f1blood_dad',
        'f1blood_mom',
        'pedigree_level'
    ];

    public function specie()
    {
        return $this->belongsTo(Specie::class);
    }

    public function farm()
    {
        return $this->belongsTo(Farm::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function breedings()
    {
        return $this->hasMany(Breeding::class);
    }

    public function sons()
    {
        return $this->hasMany(Son::class);
    }

    public function transection()
    {
        return $this->belongsTo(TransectionCow::class, 'id', 'cow_id');
    }

    public function age()
    {
        return Carbon::parse($this->attributes['birth'])->diff(Carbon::now())->format('%y ปี, %m เดือน และอีก   %d วัน');
    }
}
