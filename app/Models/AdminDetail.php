<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminDetail extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'sex',
        'card_id',
        'old',
        'study',
        'profession',
        'earning',
        'address',
        'user_id'
    ];
    public function farms()
    {
        return $this->hasMany(Farm::class)->orderBy('created_at', 'desc');
    }
    public function sperms()
    {
        return $this->hasMany(Sperm::class)->orderBy('created_at', 'desc');
    }
    public function cows()
    {
        return $this->hasMany(Cow::class)->where('status', 'alive');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
