<?php

namespace App\Http\Controllers;

use App\Models\Cow;
use App\Models\Transection;
use App\Models\TransectionCow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class HistoryCowController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::allows('isAdmin')) {
            $cows = Cow::where('status', 'dead')->orderBy('updated_at', 'desc')->get();
        } else {
            $user =  auth()->user();
            if (Gate::allows('isEtp')) {

                $cows = Cow::select('cows.*')
                    ->join('farms', 'cows.farm_id', 'farms.id')
                    ->join('enterprisesers', 'farms.enterpriseser_id', 'enterprisesers.id')
                    ->join('enterpriseser_user', 'enterprisesers.id', 'enterpriseser_user.enterpriseser_id')
                    ->where('enterpriseser_user.user_id', $user->id)
                    // ->where('transection_cows.farm_id', 'farms.id')
                    ->where('cows.status', 'dead')
                    ->orderBy('cows.updated_at', 'desc')
                    ->get();
                    
            } else {
                $cows = Cow::select('cows.*')
                    ->join('farms', 'cows.farm_id', 'farms.id')
                    ->where('farms.user_id', $user->id)
                    ->where('status', 'dead')
                    ->orderBy('cows.updated_at', 'desc')
                    ->get();
            }
        }
        return view('admin.histories.index', compact('cows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Cow  $history)
    {
        return view('admin.histories.grad', compact('history'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cow $history)
    {
        $request->validate([
            'grade' => 'required'
        ]);
        $history->update($request->all());
        return redirect(route('admin.dashboard.index'))->with('message', 'ดำเนินการสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
