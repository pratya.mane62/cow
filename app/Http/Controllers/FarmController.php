<?php

namespace App\Http\Controllers;

use App\Models\Enterpriseser;
use App\Models\User;
use App\Models\Farm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class FarmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user =  auth()->user();
        if (Gate::allows('isAdmin')) {
            $farms = Farm::where('type', 'farm')->get();
        } else {
            $farms = $user->farms()->where('type', 'farm')->get();
        }
        return view('admin.farms.index', compact('farms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('isFarm');
        $users = User::where('status', '<>', 'admin')->get();
        $etps = Enterpriseser::get();
        return view('admin.farms.create', compact('users', 'etps'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isFarm');
        $request->validate([
            'title' => 'required|max:255',
            'address' => 'required|max:255',

            'type' => 'in:farm,slaughterhouse',
        ]);

        $user =  User::find($request->user_id);
        if ($user->status == 'farm' && $request->type != 'farm') {
            return back()->withErrors(['user_id' => 'แอดมิน ' .  $user->name . ' ไม่สามารถเพิ่มโรงเชือดได้']);
        } else
            if ($user->status == 'slaugh' && $request->type != 'slaughterhouse') {
            return back()->withErrors(['user_id' => 'แอดมิน ' .  $user->name . ' ไม่สามารถเพิ่มฟาร์มได้']);
        }
        $data = $request->all();
        if ($request->hasFile('photo')) {
            $fileName = time() . '_' . $request->photo->getClientOriginalName();
            $request->file('photo')->storeAs('uploads', $fileName, 'public');
        }
        $data['photo'] = $fileName ?? NULL;
        $data['headwaters'] = !empty($request->headwaters) ? 1 : 0 ;
        $data['middle'] = !empty($request->middle) ? 1 : 0 ;
        $data['downstream'] = !empty($request->downstream) ? 1 : 0 ;
        // dd($data);
        Farm::create($data);
        return redirect(route('admin.farms.index'))->with('message', 'เพิ่มข้อมูลสำเร็จ !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Farm  $farm
     * @return \Illuminate\Http\Response
     */
    public function show(Farm $farm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Farm  $farm
     * @return \Illuminate\Http\Response
     */
    public function edit(Farm $farm)
    {
        $users = User::where('status', '<>', 'admin')->get();
        $etps = Enterpriseser::get();
        return view('admin.farms.edit', compact('farm', 'users', 'etps'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Farm  $farm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Farm $farm)
    {
        $request->validate([
            'title' => 'required|max:255',
            'address' => 'required|max:255',

            'type' => 'in:farm,slaughterhouse',
        ]);
        $user =  User::find($request->user_id);
        if ($user->status == 'farm' && $request->type != 'farm') {
            return back()->withErrors(['user_id' => 'แอดมิน ' .  $user->name . ' ไม่สามารถเพิ่มโรงเชือดได้']);
        } else
            if ($user->status == 'slaugh' && $request->type != 'slaughterhouse') {
            return back()->withErrors(['user_id' => 'แอดมิน ' .  $user->name . ' ไม่สามารถเพิ่มฟาร์มได้']);
        }
        $data = $request->all();
        if ($request->hasFile('photo')) {
            $fileName = time() . '_' . $request->photo->getClientOriginalName();
            $request->file('photo')->storeAs('uploads', $fileName, 'public');
            $data['photo'] = $fileName ? $fileName : $farm->photo;
        }
        $data['headwaters'] = !empty($request->headwaters) ? 1 : 0 ;
        $data['middle'] = !empty($request->middle) ? 1 : 0 ;
        $data['downstream'] = !empty($request->downstream) ? 1 : 0 ;
        $farm->update($data);
        return redirect(route('admin.farms.index'))->with('message', 'บันทึกข้อมูลสำเร็จ !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Farm  $farm
     * @return \Illuminate\Http\Response
     */
    public function destroy(Farm $farm)
    {
        $farm->delete();
        return redirect(route('admin.farms.index'))->with('message', 'ลบข้อมูลสำเร็จ !!');
    }

    public function cows(Farm $farm)
    {
        $cows = $farm->cows;
        return view('admin.farms.cows', compact('farm', 'cows'));
    }
}
