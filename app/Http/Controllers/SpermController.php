<?php

namespace App\Http\Controllers;

use App\Models\Cow;
use App\Models\Specie;
use App\Models\Sperm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class SpermController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('isFarm');
        $user =  auth()->user();
        if (Gate::allows('isAdmin')) {
            $sperms = Sperm::get();
        } else {
            $sperms = Sperm::get();
            // $sperms = $user->sperms ?? null;
        }
        return view('admin.sperms.index', compact('sperms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('isFarm');
        $user =  auth()->user();
        if (Gate::allows('isAdmin')) {
            $cows = Cow::where('status', 'alive')->where('type', 'dad')->get();
        } else {
            $cows = Cow::select('cows.*')
                ->join('farms', 'cows.farm_id', 'farms.id')
                ->where('farms.user_id', $user->id)
                ->where('status', 'alive')
                ->get();
        }
        $species = Specie::get();
        return view('admin.sperms.create', compact('cows', 'species'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isFarm');
        $user =  auth()->user();
        $request->validate(['specie_id' => 'required|max:255', 'bull' => 'required|max:255', 'resource' => 'required|max:255']);
        $data = $request->all();
        $data['admin_detail_id'] = $user->id ?? null;
        Sperm::create($data);
        return redirect(route('admin.sperms.index'))->with('message', 'เพิ่มข้อมูลเรียบร้อย');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sperm  $sperm
     * @return \Illuminate\Http\Response
     */
    public function show(Sperm $sperm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sperm  $sperm
     * @return \Illuminate\Http\Response
     */
    public function edit(Sperm $sperm)
    {
        $this->authorize('isFarm');
        $user =  auth()->user();
        if (Gate::allows('isAdmin')) {
            $cows = Cow::where('status', 'alive')->where('type', 'dad')->get();
        } else {
            $cows = Cow::select('cows.*')
                ->join('farms', 'cows.farm_id', 'farms.id')
                ->where('farms.user_id', $user->id)
                ->where('status', 'alive')
                ->get();
        }
        $species = Specie::get();
        return view('admin.sperms.edit', compact('cows', 'sperm', 'species'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sperm  $sperm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sperm $sperm)
    {
        $this->authorize('isFarm');
        $request->validate(['specie_id' => 'required|max:255', 'bull' => 'required|max:255', 'resource' => 'required|max:255']);
        $data = $request->all();
        $sperm->update($data);
        return redirect(route('admin.sperms.index'))->with('message', 'บันทึกข้อมูลเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sperm  $sperm
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sperm $sperm)
    {
        $this->authorize('isFarm');
        $sperm->delete();
        return back()->with('message', 'ลบข้อมูลสำเร็จ !!');
    }
}
