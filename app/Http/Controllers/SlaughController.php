<?php

namespace App\Http\Controllers;

use App\Models\Enterpriseser;
use App\Models\Farm;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class SlaughController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user =  auth()->user();
        if (Gate::allows('isAdmin')) {
            $slaughs = Farm::where('type', 'slaughterhouse')->get();
        } else {
            $slaughs = $user->farms()->where('type', 'slaughterhouse')->get();
        }
        return view('admin.slaugh.index', compact('slaughs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('status', '<>', 'admin')->get();
        return view('admin.slaugh.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'address' => 'required|max:255',
            'type' => 'in:farm,slaughterhouse',
        ]);

        $user =  User::find($request->user_id);
        if ($user->status == 'farm' && $request->type != 'farm') {
            return back()->withErrors(['user_id' => 'แอดมิน ' .  $user->name . ' ไม่สามารถเพิ่มโรงเชือดได้']);
        } else
            if ($user->status == 'slaugh' && $request->type != 'slaughterhouse') {
            return back()->withErrors(['user_id' => 'แอดมิน ' .  $user->name . ' ไม่สามารถเพิ่มฟาร์มได้']);
        }
        $data = $request->all();
        if ($request->hasFile('photo')) {
            $fileName = time() . '_' . $request->photo->getClientOriginalName();
            $request->file('photo')->storeAs('uploads', $fileName, 'public');
        }
        $data['photo'] = $fileName ?? NULL;
        Farm::create($data);
        return redirect(route('admin.slaugh.index'))->with('message', 'เพิ่มข้อมูลสำเร็จ !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Farm  $farm
     * @return \Illuminate\Http\Response
     */
    public function show(Farm $slaugh)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Farm  $farm
     * @return \Illuminate\Http\Response
     */
    public function edit(Farm $slaugh)
    {
        $users = User::where('status', '<>', 'admin')->get();
        return view('admin.slaugh.edit', compact('slaugh', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Farm  $farm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Farm $slaugh)
    {
        $request->validate([
            'title' => 'required|max:255',
            'address' => 'required|max:255',
            'type' => 'in:farm,slaughterhouse',
        ]);
        $user =  User::find($request->user_id);
        if ($user->status == 'farm' && $request->type != 'farm') {
            return back()->withErrors(['user_id' => 'แอดมิน ' .  $user->name . ' ไม่สามารถเพิ่มโรงเชือดได้']);
        } else
            if ($user->status == 'slaugh' && $request->type != 'slaughterhouse') {
            return back()->withErrors(['user_id' => 'แอดมิน ' .  $user->name . ' ไม่สามารถเพิ่มฟาร์มได้']);
        }
        $data = $request->all();
        if ($request->hasFile('photo')) {
            $fileName = time() . '_' . $request->photo->getClientOriginalName();
            $request->file('photo')->storeAs('uploads', $fileName, 'public');
            $data['photo'] = $fileName ? $fileName : $slaugh->photo;
        }
        $slaugh->update($data);
        return redirect(route('admin.slaugh.index'))->with('message', 'บันทึกข้อมูลสำเร็จ !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Farm  $farm
     * @return \Illuminate\Http\Response
     */
    public function destroy(Farm $slaugh)
    {
        $slaugh->delete();
        return redirect(route('admin.slaugh.index'))->with('message', 'ลบข้อมูลสำเร็จ !!');
    }

    public function cows(Farm $slaugh)
    {
        $cows = $slaugh->cows;
        return view('admin.slaugh.cows', compact('slaugh', 'cows'));
    }
}
