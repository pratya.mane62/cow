<?php

namespace App\Http\Controllers;

use App\Models\Farm;
use App\Models\Rfid;
use Illuminate\Http\Request;

class RfidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('isAdmin');
        $farms = Farm::get();
        $tags  = session()->get('tags');
        return view('admin.rfid.index', compact('farms', 'tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function remove_tag($tag)
    {
        $this->authorize('isAdmin');
        $tags = session()->get('tags');
        $key = array_search($tag, $tags);
        // dd($key);
        if ($key === false) {
            return back();
        }
        // dd($tags[$key]);
        unset($tags[$key]);
        session()->put('tags', $tags);
        return back();
    }

    public function clear_session()
    {
        $this->authorize('isAdmin');
        session()->remove('tags');
        return back();
    }

    public function create_session(Request $request)
    {
        $this->authorize('isAdmin');
        $request->validate([
            'tag_id' => 'required|max:255',
        ]);
        $tags = session()->get('tags');
        $data = array();
        if (!empty($tags)) {
            $data = $tags;
            $key = array_search($request->tag_id, $data);
            if ($key === false) {
                array_push($data, $request->tag_id);
            } else {
                return back()->with('error', 'มีหมายเลข RFID นี้แล้ว');
            }
        } else {
            array_push($data, $request->tag_id);
        }
        session()->put('tags', $data);
        return back();
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isAdmin');
        $request->validate([
            'farm_id' => 'required'
        ]);
        $tags = session()->get('tags');
        if (empty($tags)) {
            return back()->with('error', 'กรุณาใส่หมายเลข TAG ID !!');
        }
        foreach ($tags as $value) {
            Rfid::create(['farm_id' => $request->farm_id, 'tag_id' => $value]);
        }
        session()->remove('tags');
        return back()->with('message', 'เพิ่มข้อมูลสำเร็จ !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Rfid  $rfid
     * @return \Illuminate\Http\Response
     */
    public function show(Rfid $rfid)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Rfid  $rfid
     * @return \Illuminate\Http\Response
     */
    public function edit(Rfid $rfid)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Rfid  $rfid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rfid $rfid)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Rfid  $rfid
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rfid $rfid)
    {
        //
    }
}
