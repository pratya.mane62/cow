<?php

namespace App\Http\Controllers;

use App\Models\Cow;
use App\Models\Deworming;
use App\Models\Examination;
use App\Models\Vaccination;
use Illuminate\Http\Request;

class CowHealthController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:isFarm');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Cow $cow, $health = 'dewormings')
    {
        $dewormings = array();
        $examinations = array();
        $vaccinations = array();
        switch ($health) {
            case 'examinations':
                $examinations = Examination::where('cow_id', $cow->id)->where('farm_id', $cow->farm->id)->get();
                break;
            case 'vaccinations':
                $vaccinations =  Vaccination::where('cow_id', $cow->id)->where('farm_id', $cow->farm->id)->get();
                break;
            default:
                $dewormings = Deworming::where('cow_id', $cow->id)->where('farm_id', $cow->farm->id)->get();
                break;
        }
        return view('admin.cows.health', compact(
            'cow',
            'dewormings',
            'examinations',
            'vaccinations',
            'health'
        ));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Cow $cow)
    {
        if (!empty($request->deworming)) {
            Deworming::create([
                'cow_id' => $cow->id,
                'farm_id' => $cow->farm_id,
                'deworming' => $request->deworming
            ]);
        } else if (!empty($request->vaccination) || !empty($request->vaccination_date)) {
            $request->validate([
                'vaccination' => 'required|max:255',
                'vaccination_date' => 'required|max:255',
            ]);
            Vaccination::create([
                'cow_id' => $cow->id,
                'farm_id' => $cow->farm_id,
                'vaccination_date' => $request->vaccination_date,
                'vaccination' => $request->vaccination,
            ]);
        } else if (!empty($request->examination) || !empty($request->examination_date)) {
            $request->validate([
                'examination' => 'required|max:255',
                'examination_date' => 'required|max:255',
            ]);
            Examination::create([
                'cow_id' => $cow->id,
                'farm_id' => $cow->farm_id,
                'examination_date' => $request->examination_date,
                'examination' => $request->examination,
            ]);
        }
        return back()->with('message', 'ดำเนินการสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
