<?php

namespace App\Http\Controllers;

use App\Models\AdminDetail;
use App\Models\Enterpriseser;
use App\Models\EnterpriseserUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminDetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = User::where('status', '<>', 'admin')->get();
        return view('admin.details.index', compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $etps = Enterpriseser::get();
        return view('admin.details.create', compact('etps'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'card_id' => 'required|max:13|min:13|unique:users',
            'old' => 'required|max:255',
            'study' => 'required|max:255',
            'profession' => 'required|max:255',
            'earning' => 'required|max:255',
            'sex' => 'required',
            'address' => 'required|max:255',
            'phone' => 'required|regex:/(0)[0-9]{9}/|unique:users',
            'status' => 'required|in:admin,farm,slaugh',
        ]);
        $data = $request->all();
        $data['password'] = Hash::make($request->card_id);
        $user =  User::create($data);
        if (isset($request->etp_id) && count($request->etp_id) > 0) {
            for ($i = 0; $i < count($request->etp_id); $i++) {
                $ent =  EnterpriseserUser::where([
                    ["user_id", $user->id],
                    ["enterpriseser_id", $request->etp_id[$i]],
                ])->first();
                if (empty($ent)) {
                    EnterpriseserUser::create([
                        "user_id" => $user->id,
                        "enterpriseser_id" => $request->etp_id[$i]
                    ]);
                }
            }
        }
        return redirect(route('admin.details.index'))->with('message', 'เพิ่มข้อมูลสำเร็จ !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AdminDetail  $detail
     * @return \Illuminate\Http\Response
     */
    public function show(AdminDetail $detail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $detail
     * @return \Illuminate\Http\Response
     */
    public function edit(User $detail)
    {
        $etps = Enterpriseser::get();
        return view('admin.details.edit', compact('detail', 'etps'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $detail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $detail)
    {
        $request->validate([
            'name' => 'required|max:255',
            'card_id' => 'required|unique:admin_details,card_id,' . $detail->id,
            'old' => 'required|max:255',
            'study' => 'required|max:255',
            'profession' => 'required|max:255',
            'earning' => 'required|max:255',
            'sex' => 'required|max:255',
            'address' => 'required|max:255',
        ]);

        if (isset($request->etp_id) && count($request->etp_id) > 0) {
            EnterpriseserUser::where(
                "user_id",
                $detail->id,
            )->delete();
            for ($i = 0; $i < count($request->etp_id); $i++) {

                EnterpriseserUser::create([
                    "enterpriseser_id" => $request->etp_id[$i],
                    "user_id" => $detail->id,
                ]);
            }
        } else {
            EnterpriseserUser::where(
                "user_id",
                $detail->id,
            )->delete();
        }
        $data = $request->all();
        $detail->update($data);
        return redirect(route('admin.details.index'))->with('message', 'บันทึกข้อมูลสำเร็จ !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $detail
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $detail)
    {
        if ($detail->etps->count() > 0) {
            EnterpriseserUser::where(
                "user_id",
                $detail->id,
            )->delete();
        }
        $detail->delete();
        return redirect(route('admin.details.index'))->with('message', 'ลบข้อมูลสำเร็จ !!');
    }
}
