<?php

namespace App\Http\Controllers;

use App\Models\Breeding;
use App\Models\Cow;
use App\Models\Sperm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class BreedingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('isFarm');
        $user =  auth()->user();
        if (Gate::allows('isAdmin')) {
            $cows = Cow::where('status', 'alive')->where('type', 'mom')->get();
        } else {
            $cows = Cow::select('cows.*')
                ->join('farms', 'cows.farm_id', 'farms.id')
                ->where('farms.user_id', $user->id)
                ->where('status', 'alive')
                ->get();
        }
        return view('admin.breeding.index', compact('cows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('isFarm');
        $cow = Cow::find($request->cow);
        $sperms = Sperm::get();
        // if (Gate::allows('isAdmin')) {
        //     $sperms = Sperm::get();
        // } else {
        //     $user =  auth()->user();
        //     $sperms = $user->sperms ?? null;
        // }
        return view('admin.breeding.create', compact('cow', 'sperms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isFarm');
        $request->validate([
            'cow_id' => 'required',
            'sperm_id' => 'required',
            'date' => 'required',
            'time' => 'required',
        ]);
        $breeding = Breeding::create($request->all());
        return redirect(route('admin.breeding.show', $breeding))->with('message', 'เพิ่มข้อมูลเรียบร้อย');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Breeding  $breeding
     * @return \Illuminate\Http\Response
     */
    public function show(Breeding $breeding)
    {
        $this->authorize('isFarm');
        $breedings = Breeding::where('cow_id', $breeding->cow_id)->get();
        return view('admin.breeding.show', compact('breedings'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Breeding  $breeding
     * @return \Illuminate\Http\Response
     */
    public function edit(Breeding $breeding)
    {
        $this->authorize('isFarm');
        return view('admin.breeding.edit', compact('breeding'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Breeding  $breeding
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Breeding $breeding)
    {
        $this->authorize('isFarm');
        $breeding->update(['status' => $request->success == 1 ? 'success' : 'fail']);
        return redirect(route('admin.breeding.show', $breeding))->with('message', 'บันทึกข้อมูลเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Breeding  $breeding
     * @return \Illuminate\Http\Response
     */
    public function destroy(Breeding $breeding)
    {
        $this->authorize('isFarm');
        $breeding->delete();
        return back()->with('message', 'ลบข้อมูลสำเร็จ !!');
    }
}
