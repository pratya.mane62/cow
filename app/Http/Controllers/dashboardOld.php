<?php

namespace App\Http\Controllers;

use App\Models\Cow;
use App\Models\Enterpriseser;
use App\Models\Farm;
use App\Models\Specie;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth()->user();
        $cows = array();
        $farms = array();
        $slaugs = array();
        $users = array();
        $etps = array();

        $headwaters = $request->headwaters ? 1 : 0;
        $middle = $request->middle ? 1 : 0;
        $downstream = $request->downstream ? 1 : 0;
        $specie_id = $request->specie_id ?? null;
        $cow_type = $request->type ? $request->type : 'dad';

        if (Gate::allows('isAdmin')) {
            $cowAlls = Cow::where('status', 'alive')->get();
            $cows = Cow::where('status', 'alive')
                ->where('specie_id', !empty($request->specie_id) ? '=' : '<>', !empty($request->specie_id) ? $request->specie_id : null)
                ->where('type', $cow_type)
                ->get();
            $farmAlls = Farm::get();
            $farms = Farm::where('headwaters', $headwaters)
                ->where('middle', $middle)
                ->where('downstream', $downstream)
                ->get();
            $slaugs = Farm::where('type', 'slaughterhouse')->get();
            $users = DB::table('users')
                ->select('status', DB::raw('count(*) as total'))
                ->groupBy('status')
                ->get();
            $etps = Enterpriseser::get();
        } else {
            $cowAlls = Cow::select('cows.*')
                ->join('farms', 'cows.farm_id', 'farms.id')
                ->where('farms.user_id', $user->id)
                ->where('cows.status', 'alive')
                ->get();
            $cows = Cow::select('cows.*')
                ->join('farms', 'cows.farm_id', 'farms.id')
                ->where('farms.user_id', $user->id)
                ->where('cows.specie_id', !empty($request->specie_id) ? '=' : '<>', !empty($request->specie_id) ? $request->specie_id : null)
                ->where('cows.type', $cow_type)
                ->where('cows.status', 'alive')
                ->get();
            $farmAlls = $user->farms;
            $farms = $user->farms()->where('headwaters', $headwaters)
                ->where('middle', $middle)
                ->where('downstream', $downstream)
                ->get();
            $slaugs = $user->farms()->where('type', 'slaughterhouse')->get();
            $etps = $user->etps;
        }
        $species = Specie::get();
        return view('admin.dashboard.index', compact(
            'cows',
            'farms',
            'slaugs',
            'users',
            'etps',
            'headwaters',
            'middle',
            'downstream',
            'farmAlls',
            'cow_type',
            'cowAlls',
            'species',
            'specie_id'
        ));
    }
    // public function farms(Request $request)
    // {
    //     $user = auth()->user();
    //     if (Gate::allows('isAdmin')) {
    //         $farms = Farm::get();
    //     } else {
    //         $farms = $user->farms;
    //     }
    //     $choices = array();
    //     $choice = $request->choice;
    //     $value = null;
    //     $value = $request->value;
    //     if (!empty($choice)) {
    //         switch ($choice) {
    //             case 'etp':
    //                 # กลุ่มวิสาหกิจ
    //                 $choices = Farm::select('enterpriseser_id')->distinct()->get();
    //                 break;
    //             case 'province':
    //                 # จังหวัด
    //                 $choices  = Gate::allows('isAdmin') ? Farm::select('province')->distinct()->get() : $user->farms()->select('province')->distinct()->get();
    //                 break;
    //             case 'amphoe':
    //                 # อำเภอ
    //                 $choices  = Gate::allows('isAdmin') ? Farm::select('province', 'amphoe')->distinct()->get() :  $user->farms()->select('province', 'amphoe')->distinct()->get();
    //                 break;
    //             case 'district':
    //                 # ตำบล
    //                 $choices  = Gate::allows('isAdmin') ? Farm::select('province', 'amphoe', 'district')->distinct()->get() :  $user->farms()->select('province', 'amphoe', 'district')->distinct()->get();
    //                 break;
    //             default:
    //                 # code...
    //                 return back()->with('error', 'กรุณาเลือกข้อมูลให้ถูกต้อง');
    //                 break;
    //         }
    //     }
    //     if (!empty($value)) {
    //         switch ($choice) {
    //             case 'etp':
    //                 # กลุ่มวิสาหกิจ
    //                 $farms = Farm::where('enterpriseser_id', $value)->get();
    //                 $choices = Farm::select('enterpriseser_id')->distinct()->get();
    //                 $cows = Cow::select('cows.*')
    //                     ->join('farms', 'cows.farm_id', '=', 'farms.id')
    //                     ->where('farms.enterpriseser_id', $value)
    //                     ->get();
    //                 break;
    //             case 'province':
    //                 # จังหวัด
    //                 if (Gate::allows('isAdmin')) {
    //                     $farms = Farm::where('province', $value)->get();
    //                     $cows = Cow::select('cows.*')
    //                         ->join('farms', 'cows.farm_id', '=', 'farms.id')
    //                         ->where('farms.province', $value)
    //                         ->get();
    //                     $choices  = Farm::select('province')->distinct()->get();
    //                 } else {
    //                     $choices  = $user->farms()->select('province')->distinct()->get();
    //                     $farms = $user->farms()->where('province', $value)->get();
    //                     if ($user->cows->count() > 0) {
    //                         $cows = $user->cows()->select('cows.*')
    //                             ->join('farms', 'cows.farm_id', '=', 'farms.id')
    //                             ->where('farms.province', $value)
    //                             ->get();
    //                     }
    //                 }
    //                 break;
    //             case 'amphoe':
    //                 # อำเภอ
    //                 if (Gate::allows('isAdmin')) {
    //                     $farms = Farm::where('amphoe', $value)->get();
    //                     $cows = Cow::select('cows.*')
    //                         ->join('farms', 'cows.farm_id', '=', 'farms.id')
    //                         ->where('farms.amphoe', $value)
    //                         ->get();
    //                     $choices  = Farm::select('amphoe')->distinct()->get();
    //                 } else {
    //                     $choices  = $user->farms()->select('amphoe')->distinct()->get();
    //                     $farms = $user->farms()->where('amphoe', $value)->get();
    //                     if ($user->cows->count() > 0) {
    //                         $cows = $user->cows()->select('cows.*')
    //                             ->join('farms', 'cows.farm_id', '=', 'farms.id')
    //                             ->where('farms.amphoe', $value)
    //                             ->get();
    //                     }
    //                 }
    //                 break;
    //             case 'district':
    //                 # ตำบล
    //                 if (Gate::allows('isAdmin')) {
    //                     $farms = Farm::where('district', $value)->get();
    //                     $cows = Cow::select('cows.*')
    //                         ->join('farms', 'cows.farm_id', '=', 'farms.id')
    //                         ->where('farms.district', $value)
    //                         ->get();
    //                     $choices  = Farm::select('district')->distinct()->get();
    //                 } else {
    //                     $choices  = $user->farms()->select('district')->distinct()->get();
    //                     $farms = $user->farms()->where('district', $value)->get();
    //                     if ($user->cows->count() > 0) {
    //                         $cows = $user->cows()->select('cows.*')
    //                             ->join('farms', 'cows.farm_id', '=', 'farms.id')
    //                             ->where('farms.district', $value)
    //                             ->get();
    //                     }
    //                 }
    //                 break;
    //             default:
    //                 # code...
    //                 return back()->with('error', 'กรุณาเลือกข้อมูลให้ถูกต้อง');
    //                 break;
    //         }
    //     }
    //     return view('admin.dashboard.farms', compact('farms', 'choice', 'choices', 'value'));
    // }
    public function selectChoice($choice)
    {
        $user = auth()->user();
        if (Gate::allows('isAdmin')) {
            $cows = Cow::where('status', 'alive')->get();
            $farms = Farm::get();
        } else {
            $cows = Cow::select('cows.*')
                ->join('farms', 'cows.farm_id', 'farms.id')
                ->where('farms.user_id', $user->id)
                ->where('status', 'alive')
                ->get();
            $farms = $user->farms;
        }
        $choices = array();
        switch ($choice) {
            case 'etp':
                # กลุ่มวิสาหกิจ
                $choices = Farm::select('enterpriseser_id')->distinct()->get();
                break;
            case 'province':
                # จังหวัด
                $choices  = Gate::allows('isAdmin') ? Farm::select('province')->distinct()->get() : $user->farms()->select('province')->distinct()->get();
                break;
            case 'amphoe':
                # อำเภอ
                $choices  = Gate::allows('isAdmin') ? Farm::select('province', 'amphoe')->distinct()->get() :  $user->farms()->select('province', 'amphoe')->distinct()->get();
                break;
            case 'district':
                # ตำบล
                $choices  = Gate::allows('isAdmin') ? Farm::select('province', 'amphoe', 'district')->distinct()->get() :  $user->farms()->select('province', 'amphoe', 'district')->distinct()->get();
                break;
            case 'cow':
                # กลุ่มวิสาหกิจ
                $choices = [
                    '1' => [1, 10],
                    '2' => [11, 20],
                    '3' => [21, 50],
                    '4' => [51, 100],
                    '5' => [101, 200],
                    '6' => [201, 1000],
                ];
                break;
            case 'cow_type':
                # ประเภทของวัว
                if (Gate::allows('isAdmin')) {
                    $choices  = Cow::select('cows.type')
                        ->distinct()
                        ->join('farms', 'cows.farm_id', '=', 'farms.id')
                        ->get();
                } else {
                    if ($user->cows->count() > 0) {
                        $choices  =  Cow::select('cows.type')->join('farms', 'cows.farm_id', '=', 'farms.id')
                            ->distinct()
                            ->where('farms.user_id', $user->id)
                            ->get();
                    } else {
                        return back()->with('error', 'ฟาร์มที่คุณดูแลยังไม่มีข้อมูลวัว');
                    }
                }
                break;
            default:
                # code...
                return back()->with('error', 'กรุณาเลือกข้อมูลให้ถูกต้อง');
                break;
        }
        return view('admin.dashboard.index', compact('cows', 'farms', 'choice', 'choices'));
    }

    public function selectFilter($choice, $value)
    {
        $user = auth()->user();
        if (Gate::allows('isAdmin')) {
            $cows = Cow::where('status', 'alive')->get();
            $farms = Farm::get();
        } else {
            $cows = Cow::select('cows.*')
                ->join('farms', 'cows.farm_id', 'farms.id')
                ->where('farms.user_id', $user->id)
                ->where('status', 'alive')
                ->get();
            $farms = $user->farms;
        }
        if (empty($value)) {
            return back()->with('error', 'กรุณาเลือกข้อมูลให้ถูกต้อง');
        }
        $choices = array();
        switch ($choice) {
            case 'etp':
                # กลุ่มวิสาหกิจ
                $farms = Farm::where('enterpriseser_id', $value)->get();
                $choices = Farm::select('enterpriseser_id')->distinct()->get();
                $cows = Cow::select('cows.*')
                    ->join('farms', 'cows.farm_id', '=', 'farms.id')
                    ->where('farms.enterpriseser_id', $value)
                    ->get();
                break;
            case 'province':
                # จังหวัด
                if (Gate::allows('isAdmin')) {
                    $farms = Farm::where('province', $value)->get();
                    $cows = Cow::select('cows.*')
                        ->join('farms', 'cows.farm_id', '=', 'farms.id')
                        ->where('farms.province', $value)
                        ->get();
                    $choices  = Farm::select('province')->distinct()->get();
                } else {
                    $choices  = $user->farms()->select('province')->distinct()->get();
                    $farms = $user->farms()->where('province', $value)->get();
                    if ($user->cows->count() > 0) {
                        $cows = $user->cows()->select('cows.*')
                            ->join('farms', 'cows.farm_id', '=', 'farms.id')
                            ->where('farms.province', $value)
                            ->get();
                    }
                }
                break;
            case 'amphoe':
                # อำเภอ
                if (Gate::allows('isAdmin')) {
                    $farms = Farm::where('amphoe', $value)->get();
                    $cows = Cow::select('cows.*')
                        ->join('farms', 'cows.farm_id', '=', 'farms.id')
                        ->where('farms.amphoe', $value)
                        ->get();
                    $choices  = Farm::select('amphoe')->distinct()->get();
                } else {
                    $choices  = $user->farms()->select('amphoe')->distinct()->get();
                    $farms = $user->farms()->where('amphoe', $value)->get();
                    if ($user->cows->count() > 0) {
                        $cows = $user->cows()->select('cows.*')
                            ->join('farms', 'cows.farm_id', '=', 'farms.id')
                            ->where('farms.amphoe', $value)
                            ->get();
                    }
                }
                break;
            case 'district':
                # ตำบล
                if (Gate::allows('isAdmin')) {
                    $farms = Farm::where('district', $value)->get();
                    $cows = Cow::select('cows.*')
                        ->join('farms', 'cows.farm_id', '=', 'farms.id')
                        ->where('farms.district', $value)
                        ->get();
                    $choices  = Farm::select('district')->distinct()->get();
                } else {
                    $choices  = $user->farms()->select('district')->distinct()->get();
                    $farms = $user->farms()->where('district', $value)->get();
                    if ($user->cows->count() > 0) {
                        $cows = $user->cows()->select('cows.*')
                            ->join('farms', 'cows.farm_id', '=', 'farms.id')
                            ->where('farms.district', $value)
                            ->get();
                    }
                }
                break;
            case 'cow':
                # จำนวนวัว
                switch ($value) {
                    case '1':
                        $farms = Gate::allows('isAdmin') ?
                            Farm::has('cows', '>', 1)->has('cows', '<=', 10)->get() :
                            $user->farms()->has('cows', '>', 1)->has('cows', '<=', 10)->get();
                        break;
                    case '2':
                        $farms = Gate::allows('isAdmin') ?
                            Farm::has('cows', '>', 11)->has('cows', '<=', 20)->get() :
                            $user->farms()->has('cows', '>', 11)->has('cows', '<=', 20)->get();
                        break;
                    case '3':
                        $farms = Gate::allows('isAdmin') ?
                            Farm::has('cows', '>', 21)->has('cows', '<=', 50)->get() :
                            $user->farms()->has('cows', '>', 21)->has('cows', '<=', 50)->get();
                        break;
                    case '4':
                        $farms = Gate::allows('isAdmin') ?
                            Farm::has('cows', '>', 51)->has('cows', '<=', 100)->get() :
                            $user->farms()->has('cows', '>', 51)->has('cows', '<=', 100)->get();
                        break;
                    case '5':
                        $farms = Gate::allows('isAdmin') ?
                            Farm::has('cows', '>', 101)->has('cows', '<=', 200)->get() :
                            $user->farms()->has('cows', '>', 101)->has('cows', '<=', 200)->get();
                        break;
                    case '6':
                        $farms = Gate::allows('isAdmin') ?
                            Farm::has('cows', '>', 200)->get() :
                            $user->farms()->has('cows', '>', 200)->get();
                        break;
                    default:
                        break;
                }
                $choices = [
                    '1' => [1, 10],
                    '2' => [11, 20],
                    '3' => [21, 50],
                    '4' => [51, 100],
                    '5' => [101, 200],
                    '6' => [201, 1000],
                ];
                break;
            case 'cow_type':
                # ประเภทของวัว
                if (Gate::allows('isAdmin')) {
                    $choices  = Cow::select('cows.type')
                        ->distinct()
                        ->join('farms', 'cows.farm_id', '=', 'farms.id')
                        ->get();
                } else {
                    if ($user->cows->count() > 0) {
                        $choices  =  Cow::select('cows.type')->join('farms', 'cows.farm_id', '=', 'farms.id')
                            ->distinct()
                            ->where('farms.user_id', $user->id)
                            ->get();
                    } else {
                        return back()->with('error', 'ยังไม่มีข้อมูล');
                    }
                }
                switch ($value) {
                    case 'cattle':
                        if (Gate::allows('isAdmin')) {
                            $farms = Farm::whereHas('cows', function (Builder $query) {
                                $query->where('type', 'cattle');
                            })->get();
                            $cows = Cow::where('type', $value)->get();
                        } else {
                            if ($user->cows->count() > 0) {
                                $cows = Cow::select('cows.*')
                                    ->join('farms', 'cows.farm_id', '=', 'farms.id')
                                    ->where('farms.user_id', $user->id)
                                    ->where('type', $value)
                                    ->get();
                            }
                        }
                        break;
                    case 'mom':
                        if (Gate::allows('isAdmin')) {
                            $farms = Farm::whereHas('cows', function (Builder $query) {
                                $query->where('type', 'mom');
                            })->get();
                            $cows = Cow::where('type', $value)->get();
                        } else {
                            if ($user->cows->count() > 0) {
                                $cows = Cow::select('cows.*')
                                    ->join('farms', 'cows.farm_id', '=', 'farms.id')
                                    ->where('farms.user_id', $user->id)
                                    ->where('type', $value)
                                    ->get();
                            }
                        }
                        break;
                    case 'dad':
                        if (Gate::allows('isAdmin')) {
                            $farms = Farm::whereHas('cows', function (Builder $query) {
                                $query->where('type', 'dad');
                            })->get();
                            $cows = Cow::where('type', $value)->get();
                        } else {
                            if ($user->cows->count() > 0) {
                                $cows = Cow::select('cows.*')
                                    ->join('farms', 'cows.farm_id', '=', 'farms.id')
                                    ->where('farms.user_id', $user->id)
                                    ->where('type', $value)
                                    ->get();
                            }
                        }
                        break;
                }
                break;
            default:
                # code...
                return back()->with('error', 'กรุณาเลือกข้อมูลให้ถูกต้อง');
                break;
        }
        // dd($choices);
        return view('admin.dashboard.index', compact('cows', 'farms', 'choice', 'choices', 'value'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
