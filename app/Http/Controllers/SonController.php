<?php

namespace App\Http\Controllers;

use App\Models\Cow;
use App\Models\Son;
use Illuminate\Http\Request;

class SonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Cow $cow)
    {
        $sons = $cow->sons;
        return view('admin.cows.sons.index', compact('sons','cow'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Cow $cow)
    {
        return view('admin.cows.sons.create', compact('cow'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Cow $cow)
    {
        $request->validate([
            'birth' => 'required',
            'gender' => 'required',
            'weight' => 'required',
            'photo' => 'image|mimes:jpg,jpeg,png|max:2048|required',
        ]);

        $fileName = time() . '_' . $request->photo->getClientOriginalName();
        $request->file('photo')->storeAs('uploads', $fileName, 'public');
        $data = $request->all();
        $data['cow_id'] = $cow->id;
        $data['photo'] = $fileName;
        Son::create($data);
        return redirect(route('admin.sons',$cow))->with('message', 'เพิ่มข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Son  $son
     * @return \Illuminate\Http\Response
     */
    public function show(Son $son)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Son  $son
     * @return \Illuminate\Http\Response
     */
    public function edit(Son $son)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Son  $son
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Son $son)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Son  $son
     * @return \Illuminate\Http\Response
     */
    public function destroy(Son $son)
    {
        //
    }
}
