<?php

namespace App\Http\Controllers;

use App\Models\Cow;
use App\Models\Deworming;
use App\Models\Enterpriseser;
use App\Models\Examination;
use App\Models\Farm;
use App\Models\Specie;
use App\Models\TransectionCow;
use App\Models\Vaccination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class CowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user =  auth()->user();
        if (Gate::allows('isAdmin')) {
            $cows = Cow::where('status', 'alive')->orderBy('created_at', 'desc')->get();
        } else {
            $cows = Cow::select('cows.*')
                ->join('farms', 'cows.farm_id', 'farms.id')
                ->where('farms.user_id', $user->id)
                ->where('cows.status', 'alive')
                ->get();
        }
        $farms = Farm::get();
        return view('admin.cows.index', compact('cows', 'farms'));
    }

    public function waiting()
    {
        $user =  auth()->user();
        if (Gate::allows('isAdmin')) {
            $cows = Cow::select('cows.*')
                ->join('farms', 'cows.farm_id', 'farms.id')
                ->where('farms.type', 'slaughterhouse')
                ->where('cows.status', 'alive')->orderBy('created_at', 'desc')->get();
        } else {
            $cows = Cow::select('cows.*')
                ->join('farms', 'cows.farm_id', 'farms.id')
                ->where('farms.type', 'slaughterhouse')
                ->where('farms.user_id', $user->id)
                ->where('status', 'alive')
                ->get();
        }
        $farms = Farm::get();
        return view('admin.cows.waiting', compact('cows', 'farms'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('isFarm');
        $user =  auth()->user();
        if (Gate::allows('isAdmin')) {
            $farms = Farm::get();
        } else {
            $farms = $user->farms;
        }
        $secies = Specie::get();
        return view('admin.cows.create', compact('secies', 'farms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isFarm');
        $request->validate([
            'weight' => 'required|numeric|between:0,9999.99',
            'specie_id' => 'required',
            'farm_id' => 'required',
            'rfid' => 'nullable|unique:cows',
            'name' => 'required|max:255',
            'type' => 'required',
            'birth' => 'required',
            'sex' => 'required',
            'photo' => 'image|mimes:jpg,jpeg,png|max:2048',
            'photorfid' => 'image|mimes:jpg,jpeg,png|max:2048'
        ]);
        $farm = Farm::find($request->farm_id);
        $fileName = null;
        $fileNameRfid = null;
        if ($request->hasFile('photo')) {
            $fileName = time() . '_' . $request->photo->getClientOriginalName();
            $request->file('photo')->storeAs('uploads', $fileName, 'public');
        }
        if ($request->hasFile('photorfid')) {
            $fileNameRfid = time() . '_' . $request->photorfid->getClientOriginalName();
            $request->file('photorfid')->storeAs('uploads', $fileNameRfid, 'public');
        }

        $data = $request->all();
        $data['user_id'] = $farm->user_id;
        $newCow = Cow::create($data);

        $dataTransection = [
            'cow_id' => $newCow->id,
            'farm_id' => $request->farm_id,
            'weight' => $request->weight,
            'photo' =>  $fileName,
            'photorfid' =>  $fileNameRfid
        ];

        TransectionCow::create($dataTransection);
        return redirect(route('admin.cows.index'))->with('message', "เพิ่มข้อมูลสำเร็จ !!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cow  $cow
     * @return \Illuminate\Http\Response
     */
    public function show(Cow $cow)
    {
        $user =  auth()->user();
        if (Gate::allows('isAdmin')) {
            $farms = Farm::get();
        } else {
            $farms = Enterpriseser::select('farms.*')
                ->join('farms', 'enterpriseser.id', 'farms.enterpriseser_id')
                ->join('enterpriseser_user', 'enterpriseser.id', 'enterpriseser_user.enterpriseser_id')
                ->where('enterpriseser_user.user_id', $user->id)
                ->get();
        }
        return view('admin.cows.farmupdate', compact('cow', 'farms'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cow  $cow
     * @return \Illuminate\Http\Response
     */
    public function edit(Cow $cow)
    {
        $secies = Specie::get();
        $dewormings = Deworming::where('cow_id', $cow->id)->where('farm_id', $cow->farm->id)->get();
        $examinations = Examination::where('cow_id', $cow->id)->where('farm_id', $cow->farm->id)->get();
        $vaccinations =  Vaccination::where('cow_id', $cow->id)->where('farm_id', $cow->farm->id)->get();

        return view('admin.cows.edit', compact(
            'cow',
            'secies',
            'dewormings',
            'examinations',
            'vaccinations'
        ));
    }


    public function cow_qrcode(Cow $cow)
    {
        if ($cow->farm->type == 'slaughterhouse') {
            return view('admin.cows.cow_qrcode', compact('cow'));
        } else {
            abort(403, 'Unauthorized action.');
        }
    }

    public function qrcode(Cow $cow)
    {
        if ($cow->farm->type == 'slaughterhouse') {
            return view('admin.cows.qrcode', compact('cow'));
        } else {
            abort(403, 'Unauthorized action.');
        }
    }

    public function beefcode(Request $request, Cow $cow)
    {
        if ($cow->farm->type == 'slaughterhouse') {
            return view('admin.cows.beefcode', compact('cow'));
        } else {
            abort(403, 'Unauthorized action.');
        }
    }


    public function timeline(Cow $cow)
    {
        $transections = TransectionCow::where('cow_id', $cow->id)->orderBy('created_at', 'desc')->get();
        $last = TransectionCow::where('cow_id', $cow->id)->orderBy('created_at', 'desc')->first();
        return view('admin.cows.timeline', compact('transections', 'last', 'cow'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cow  $cow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cow $cow)
    {
        $request->validate([
            'specie_id' => 'required',
            'rfid' => 'nullable|unique:cows,rfid,' . $cow->id,
            'name' => 'required|max:255',
            'type' => 'required',
            'birth' => 'required',
            'sex' => 'required',
            'farm_id' => 'required',
            'photo' => 'image|mimes:jpg,jpeg,png|max:2048',
            'photorfid' => 'image|mimes:jpg,jpeg,png|max:2048',
            'weight' => 'required|numeric|between:0,9999.99',
        ]);
        $data = $request->all();
        $cow->update($data);

        $fileName = null;
        $fileNameRfid = null;

        if ($request->hasFile('photo')) {
            $fileName = time() . '_' . $request->photo->getClientOriginalName();
            $request->file('photo')->storeAs('uploads', $fileName, 'public');
        }
        if ($request->hasFile('photorfid')) {
            $fileNameRfid = time() . '_' . $request->photorfid->getClientOriginalName();
            $request->file('photorfid')->storeAs('uploads', $fileNameRfid, 'public');
        }

        if (!$request->hasFile('photo')) {
            $fileName = TransectionCow::where('cow_id', $cow->id)
                ->where('farm_id', $request->farm_id)
                ->first()
                ->photo;
        }

        if (!$request->hasFile('photorfid')) {
            $fileNameRfid = TransectionCow::where('cow_id', $cow->id)
                ->where('farm_id', $request->farm_id)
                ->first()
                ->photorfid;
        }

        $cow->update(['farm_id' => $request->farm_id]);

        TransectionCow::where('cow_id', $cow->id)
            ->where('farm_id', $request->farm_id)
            ->update([
                'weight' => $request->weight,
                'photo' =>  $fileName,
            ]);
        Deworming::where('cow_id', $cow->id)->where('farm_id', $request->farm_id)->delete();
        Vaccination::where('cow_id', $cow->id)->where('farm_id', $request->farm_id)->delete();
        Examination::where('cow_id', $cow->id)->where('farm_id', $request->farm_id)->delete();
        if (!empty($request->dewormings[0]) && count($request->dewormings) > 0) {
            foreach ($request->dewormings as  $deworming) {
                Deworming::create([
                    'cow_id' => $cow->id,
                    'farm_id' => $request->farm_id,
                    'deworming' => $deworming
                ]);
            }
        }
        if (!empty($request->vaccination_date[0]) && count($request->vaccination_date) > 0) {
            for ($i = 0; $i < count($request->vaccination_date); $i++) {
                Vaccination::create([
                    'cow_id' => $cow->id,
                    'farm_id' => $request->farm_id,
                    'vaccination_date' => $request->vaccination_date[$i],
                    'vaccination' => $request->vaccination[$i],
                ]);
            }
        }
        if (!empty($request->examination_date[0]) && count($request->examination_date) > 0) {
            for ($i = 0; $i < count($request->examination_date); $i++) {
                Examination::create([
                    'cow_id' => $cow->id,
                    'farm_id' => $request->farm_id,
                    'examination_date' => $request->examination_date[$i],
                    'examination' => $request->examination[$i],
                ]);
            }
        }

        return redirect(route('admin.cows.index'))->with('message', "บันทึกข้อมูลสำเร็จ !!");
    }


    public function updateFarm(Request $request, Cow $cow)
    {
        $request->validate([
            // 'photo' => 'image|mimes:jpg,jpeg,png|max:2048',
            // 'weight' => 'required|numeric|between:0,9999.99',
            'farm_id' => 'required',
        ]);
        $cow->update(['farm_id' => $request->farm_id]);
        $data = $request->all();
        $data['cow_id'] = $cow->id;

        TransectionCow::create($data);
        return redirect(route('admin.cows.index'))->with('message', "บันทึกข้อมูลสำเร็จ !!");
    }

    public function updateStatus(Cow $cow)
    {
        $cows =  TransectionCow::where('cow_id', $cow->id)
            ->orderBy('created_at', 'desc')
            ->first();
        if (empty($cows->weight)) {
            return back()->with('error', 'กรุณาใส่น้ำหนักวัว');
        }
        if ($cow->farm->type == 'slaughterhouse') {
            $cow->update(['status' => 'dead']);
            return back()->with('message', 'ดำเนินการสำเร็จ !!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cow  $cow
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cow $cow)
    {
        Deworming::where('cow_id', $cow->id)->delete();
        Vaccination::where('cow_id', $cow->id)->delete();
        Examination::where('cow_id', $cow->id)->delete();
        $cow->delete();
        return back()->with('message', 'ลบข้อมูลสำเร็จ !!');
    }
}
