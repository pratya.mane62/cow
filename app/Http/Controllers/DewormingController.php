<?php

namespace App\Http\Controllers;

use App\Models\Deworming;
use Illuminate\Http\Request;

class DewormingController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:isFarm');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Deworming  $deworming
     * @return \Illuminate\Http\Response
     */
    public function show(Deworming $deworming)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Deworming  $deworming
     * @return \Illuminate\Http\Response
     */
    public function edit(Deworming $deworming)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Deworming  $deworming
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Deworming $deworming)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Deworming  $deworming
     * @return \Illuminate\Http\Response
     */
    public function destroy(Deworming $deworming)
    {
        $deworming->delete();
        return back()->with('message','ดำเนินการสำเร็จ');
    }
}
