<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'phone' => 'required|regex:/(0)[0-9]{9}/|unique:users',
            'card_id' => 'required|max:13|min:13|unique:users',
            'password' => 'required|string|confirmed|min:8',
        ]);

        $adminFarm = User::where('card_id', $request->card_id)->get()->first();
        if (empty($adminFarm)) {
            return back()->withErrors(['card_id' => 'ไม่พบผู้ใช้หมายเลขบัตรประชาชนนี้']);
        }
        if ($adminFarm->farms->count() <= 0) {
            return back()->withErrors(['card_id' => 'กรุณาให้แอดมินเพิ่มฟาร์มให้คุณก่อน']);
        }
        Auth::login($user = User::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'card_id' => $request->card_id,
            'password' => Hash::make($request->password),
        ]));
        $adminFarm->user_id = $user->id;
        $adminFarm->save();
        event(new Registered($user));

        return redirect(RouteServiceProvider::HOME);
    }
}
