<?php

namespace App\Http\Controllers;

use App\Models\Enterpriseser;
use App\Models\EnterpriseserUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class EnterpriseserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user =  auth()->user();
        if (Gate::allows('isAdmin')) {
            $etps = Enterpriseser::get();
        } else {
            $etps = $user->etps;
        }
        return view('admin.enterprisesers.index', compact('etps'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('status', 'farm')->get();
        return view('admin.enterprisesers.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'location' => 'max:255',
            'coordinates' => 'max:255'
        ]);
        $etp = Enterpriseser::create($request->all());
        if (isset($request->user_id) && count($request->user_id) > 0) {
            for ($i = 0; $i < count($request->user_id); $i++) {
                $ent =  EnterpriseserUser::where([
                    ["user_id", $request->user_id[$i]],
                    ["enterpriseser_id", $etp->id],
                ])->first();
                if (empty($ent)) {
                    EnterpriseserUser::create([
                        "user_id" => $request->user_id[$i],
                        "enterpriseser_id" => $etp->id,
                    ]);
                }
            }
        }
        return redirect(route('admin.enterpriseser.index'))->with('message', 'ดำเนินการสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Enterpriseser $enterpriseser)
    {
        $users = User::where('status', 'farm')->get();
        return view('admin.enterprisesers.edit', compact('enterpriseser', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Enterpriseser $enterpriseser)
    {
        $request->validate([
            'name' => 'required|max:255',
            'location' => 'max:255',
            'coordinates' => 'max:255'
        ]);

        if (isset($request->user_id) && count($request->user_id) > 0) {
            EnterpriseserUser::where(
                "enterpriseser_id",
                $enterpriseser->id,
            )->delete();
            for ($i = 0; $i < count($request->user_id); $i++) {

                EnterpriseserUser::create([
                    "user_id" => $request->user_id[$i],
                    "enterpriseser_id" => $enterpriseser->id,
                ]);
            }
        } else {
            EnterpriseserUser::where(
                "enterpriseser_id",
                $enterpriseser->id,
            )->delete();
        }

        $enterpriseser->update($request->all());
        return redirect(route('admin.enterpriseser.index'))->with('message', 'ดำเนินการสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Enterpriseser $enterpriseser)
    {
        if ($enterpriseser->users->count() > 0) {
            EnterpriseserUser::where(
                "enterpriseser_id",
                $enterpriseser->id,
            )->delete();
        }
        $enterpriseser->delete();
        return back()->with('message', 'ดำเนินการสำเร็จ');
    }
}
