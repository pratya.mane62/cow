<div class="third-bar d-flex justify-content-between d-md-none d-lg-none">
	<!--<div class="align-self-center text-center">
		<a href="#">
			<img src="<?php /*echo base_url(); */?>assets/images/back.png?t=<?php echo date('Y-m-d') ?>" width="25" alt="">
		</a>
	</div>-->
	<div class="align-self-center text-center">
		<span style="font-size:1.3rem;" v-if="tab_active.sport">กีฬา</span>
		<span style="font-size:1.3rem;" v-else-if="tab_active.casino">คาสิโน</span>
		<span style="font-size:1.3rem;" v-else-if="tab_active.event">กิจกรรม</span>
		<span style="font-size:1.3rem;" v-else-if="tab_active.game">เกมส์</span>
		<span style="font-size:1.3rem;" v-else-if="tab_active.account">บัญชี</span>
		<span style="font-size:1.3rem;" v-else-if="tab_active.new">ข่าวสาร</span>
		<span style="font-size:1.3rem;" v-else-if="tab_active.promotion">โปรโมชั่น</span>
		<span style="font-size:1.3rem;" v-else-if="tab_active.esport">อีสปอร์ต</span>
	</div>
	<div class="align-self-center text-center" style="line-height:5px;">
		<a href="#" data-toggle="modal" data-target="#logoutModal">
			<img src="<?php echo base_url(); ?>assets/images/logout.png?t=<?php echo date('Y-m-d') ?>" width="25" alt="">
			<div class="text-white">
				<span style="font-size:10px;">ล็อคเอ้าท์</span>
			</div>
		</a>
	</div>
</div>
<section class="">
	<div class="container-lg">
		<div class="mb-2">
			<div class="d-md-flex d-none justify-content-center menu-bar-desktop">


				<div class="menu-bar align-self-center text-center mx-1 mx-lg-2" :class="tab_active.casino ? 'active' : ''">
					<a href="javascript:{}" v-on:click="changeTab('casino')" class="text-decoration-none d-block">
						<img class="mb-2" v-if="tab_active.casino" src="<?php echo base_url(); ?>assets/images/icons/casino-active.png?t=<?php echo date('Y-m-d') ?>" />
						<img class="mb-2" v-else src="<?php echo base_url(); ?>assets/images/icons/casino.png?t=<?php echo date('Y-m-d') ?>" />
						<div class="text-white">
							คาสิโน
						</div>
					</a>
				</div>

				<div class="menu-bar align-self-center text-center mx-1 mx-lg-21" :class="tab_active.game ? 'active' : ''">
					<a href="javascript:{}" v-on:click="changeTab('game')" class="text-decoration-none d-block">
						<img class="mb-2" v-if="tab_active.game" src="<?php echo base_url(); ?>assets/images/icons/slot-active.png?t=<?php echo date('Y-m-d') ?>" />
						<img class="mb-2" v-else src="<?php echo base_url(); ?>assets/images/icons/slot.png?t=<?php echo date('Y-m-d') ?>" />
						<div class="text-white font-weight-bold">
							เกมส์
						</div>
					</a>
				</div>

				<div class="menu-bar align-self-center text-center mx-1 mx-lg-2" :class="tab_active.sport ? 'active' : ''">
					<a href="javascript:{}" v-on:click="changeTab('sport')" class="text-decoration-none d-block">
						<img class="mb-2" v-if="tab_active.sport" src="<?php echo base_url(); ?>assets/images/icons/football-active.png?t=<?php echo date('Y-m-d') ?>" />
						<img class="mb-2" v-else src="<?php echo base_url(); ?>assets/images/icons/football.png?t=<?php echo date('Y-m-d') ?>" />
						<div class="text-white">
							กีฬา
						</div>
					</a>
				</div>

				<div class="menu-bar align-self-center text-center mx-1 mx-lg-2" :class="tab_active.esport ? 'active' : ''">
					<a href="javascript:{}" v-on:click="changeTab('esport')" class="text-decoration-none d-block">
						<img class="mb-2" v-if="tab_active.esport" src="<?php echo base_url(); ?>assets/images/icons/esport-active.png?t=<?php echo date('Y-m-d') ?>" />
						<img class="mb-2" v-else src="<?php echo base_url(); ?>assets/images/icons/esport.png?t=<?php echo date('Y-m-d') ?>" />
						<div class="text-white">
							อีสปอร์ต
						</div>
					</a>
				</div>

				<div class="menu-bar align-self-center text-center mx-1 mx-lg-2">
					<a href="<?php echo $user['agent'] == "1" ? base_url('agent') : base_url('ref'); ?>" class="text-decoration-none d-block">
						<img class="mb-2" src="<?php echo base_url(); ?>assets/images/icons/friend.png?t=<?php echo date('Y-m-d') ?>" />
						<div class="text-white font-weight-bold">
							<?php echo $user['agent'] == "1" ? "พันธมิตร" : "แนะนำเพื่อน"; ?>
						</div>
					</a>
				</div>

				<div class="menu-bar align-self-center text-center mx-1 mx-lg-21" :class="tab_active.event ? 'active' : ''">
					<a href="javascript:{}" v-on:click="changeTab('event')" class="text-decoration-none d-block">
						<img class="mb-2" v-if="tab_active.event" src="<?php echo base_url(); ?>assets/images/icons/activity-active.png?t=<?php echo date('Y-m-d') ?>" />
						<img class="mb-2" v-else src="<?php echo base_url(); ?>assets/images/icons/activity.png?t=<?php echo date('Y-m-d') ?>" />
						<div class="text-white font-weight-bold">
							กิจกรรม
						</div>
					</a>
				</div>

				<div class="menu-bar align-self-center text-center mx-1 mx-lg-2" :class="tab_active.account ? 'active' : ''">
					<a href="javascript:{}" v-on:click="changeTab('account')" class="text-decoration-none d-block">
						<img class="mb-2" v-if="tab_active.account" src="<?php echo base_url(); ?>assets/images/icons/user-active.png?t=<?php echo date('Y-m-d') ?>" />
						<img class="mb-2" v-else src="<?php echo base_url(); ?>assets/images/icons/user.png?t=<?php echo date('Y-m-d') ?>" />
						<div class="text-white font-weight-bold">
							บัญชี
						</div>
					</a>
				</div>

				<div class="menu-bar align-self-center text-center mx-1 mx-lg-2" :class="tab_active.new ? 'active' : ''">
					<a href="javascript:{}" v-on:click="changeTab('new')" class="text-decoration-none d-block">
						<img class="mb-2" v-if="tab_active.new" src="<?php echo base_url(); ?>assets/images/icons/annou-active.png?t=<?php echo date('Y-m-d') ?>" />
						<img class="mb-2" v-else src="<?php echo base_url(); ?>assets/images/icons/annou.png?t=<?php echo date('Y-m-d') ?>" />
						<div class="text-white font-weight-bold">
							ข่าวสาร
						</div>
					</a>
				</div>

				<!-- <div class="menu-bar align-self-center text-center mx-1 mx-lg-2" :class="tab_active.promotion ? 'active' : ''">
					<a href="<?php echo base_url('dashboard?tab=promotion'); ?>" class="text-decoration-none d-block">
						<img class="mb-2" v-if="tab_active.promotion" src="<?php echo base_url(); ?>assets/images/icons/annou-active.png?t=<?php echo date('Y-m-d') ?>" />
						<img class="mb-2" v-else src="<?php echo base_url(); ?>assets/images/icons/gift.png?t=<?php echo date('Y-m-d') ?>" />
						<div class="text-white font-weight-bold">
							โปรโมชั่น
						</div>
					</a>
				</div> -->



				<?php if($_SESSION['user']['role'] == roleAdmin() || $_SESSION['user']['role'] == roleSuperAdmin()): ?>
				<div class="menu-bar active align-self-center text-center mx-1 mx-lg-2">
					<a href="<?php echo base_url('admin'); ?>" class="text-decoration-none d-block">
						<img class="mb-2" src="<?php echo base_url(); ?>assets/images/icons/setting.png?t=<?php echo date('Y-m-d') ?>" />
						<div class="text-white font-weight-bold">
							แอดมิน
						</div>
					</a>
				</div>
				<?php endif; ?>
			</div>

			<div class="row">
				<div class="col-3 pl-0 pr-1 d-lg-none d-md-none">

					<div class="menu-bar align-self-center text-center mb-1" :class="tab_active.casino ? 'active' : ''">
						<a href="javascript:{}" v-on:click="changeTab('casino')" class="text-decoration-none d-block">
							<img class="mb-2" v-if="tab_active.casino" src="<?php echo base_url(); ?>assets/images/icons/casino-active.png?t=<?php echo date('Y-m-d') ?>" />
							<img class="mb-2" v-else src="<?php echo base_url(); ?>assets/images/icons/casino.png?t=<?php echo date('Y-m-d') ?>" />
							<div class="text-white">
								คาสิโน
							</div>
						</a>
					</div>
					<div class="menu-bar align-self-center text-center mb-1" :class="tab_active.game ? 'active' : ''">
						<a href="javascript:{}" v-on:click="changeTab('game')" class="text-decoration-none d-block">
							<img class="mb-2" v-if="tab_active.game" src="<?php echo base_url(); ?>assets/images/icons/slot-active.png?t=<?php echo date('Y-m-d') ?>" />
							<img class="mb-2" v-else src="<?php echo base_url(); ?>assets/images/icons/slot.png?t=<?php echo date('Y-m-d') ?>" />
							<div class="text-white font-weight-bold">
								เกมส์
							</div>
						</a>
					</div>

					<div class="menu-bar align-self-center text-center mb-1" :class="tab_active.sport ? 'active' : ''">
						<a href="javascript:{}" v-on:click="changeTab('sport')" class="text-decoration-none d-block">
							<img class="mb-2" v-if="tab_active.sport" src="<?php echo base_url(); ?>assets/images/icons/football-active.png?t=<?php echo date('Y-m-d') ?>" />
							<img class="mb-2" v-else src="<?php echo base_url(); ?>assets/images/icons/football.png?t=<?php echo date('Y-m-d') ?>" />
							<div class="text-white">
								กีฬา
							</div>
						</a>
					</div>

					<div class="menu-bar align-self-center text-center mb-1" :class="tab_active.esport ? 'active' : ''">
						<a href="javascript:{}" v-on:click="changeTab('esport')" class="text-decoration-none d-block">
							<img class="mb-2" v-if="tab_active.esport" src="<?php echo base_url(); ?>assets/images/icons/esport-active.png?t=<?php echo date('Y-m-d') ?>" />
							<img class="mb-2" v-else src="<?php echo base_url(); ?>assets/images/icons/esport.png?t=<?php echo date('Y-m-d') ?>" />
							<div class="text-white">
								อีสปอร์ต
							</div>
						</a>
					</div>

					<div class="menu-bar align-self-center text-center mb-1">
						<a href="<?php echo $user['agent'] == "1" ? base_url('agent') : base_url('ref'); ?>" class="text-decoration-none d-block">
							<img class="mb-2" src="<?php echo base_url(); ?>assets/images/icons/friend.png?t=<?php echo date('Y-m-d') ?>" />
							<div class="text-white font-weight-bold">
								<?php echo $user['agent'] == "1" ? "พันธมิตร" : "แนะนำเพื่อน"; ?>
							</div>
						</a>
					</div>

					<div class="menu-bar align-self-center text-center mb-1" :class="tab_active.event ? 'active' : ''">
						<a href="javascript:{}" v-on:click="changeTab('event')" class="text-decoration-none d-block">
							<img class="mb-2" v-if="tab_active.event" src="<?php echo base_url(); ?>assets/images/icons/activity-active.png?t=<?php echo date('Y-m-d') ?>" />
							<img class="mb-2" v-else src="<?php echo base_url(); ?>assets/images/icons/activity.png?t=<?php echo date('Y-m-d') ?>" />
							<div class="text-white font-weight-bold">
								กิจกรรม
							</div>
						</a>
					</div>



					<div class="menu-bar align-self-center text-center mb-1" :class="tab_active.account ? 'active' : ''">
						<a href="javascript:{}" v-on:click="changeTab('account')" class="text-decoration-none d-block">
							<img class="mb-2" v-if="tab_active.account" src="<?php echo base_url(); ?>assets/images/icons/user-active.png?t=<?php echo date('Y-m-d') ?>" />
							<img class="mb-2" v-else src="<?php echo base_url(); ?>assets/images/icons/user.png?t=<?php echo date('Y-m-d') ?>" />
							<div class="text-white font-weight-bold">
								บัญชี
							</div>
						</a>
					</div>

					<div class="menu-bar align-self-center text-center mb-1" :class="tab_active.new ? 'active' : ''">
						<a href="javascript:{}" v-on:click="changeTab('new')" class="text-decoration-none d-block">
							<img class="mb-2" v-if="tab_active.new" src="<?php echo base_url(); ?>assets/images/icons/annou-active.png?t=<?php echo date('Y-m-d') ?>" />
							<img class="mb-2" v-else src="<?php echo base_url(); ?>assets/images/icons/annou.png?t=<?php echo date('Y-m-d') ?>" />
							<div class="text-white font-weight-bold">
								ข่าวสาร
							</div>
						</a>
					</div>

				<!--	<div class="menu-bar align-self-center text-center mb-1" :class="tab_active.promotion ? 'active' : ''">
						<a href="<?php echo base_url('dashboard?tab=promotion'); ?>" class="text-decoration-none d-block">
							<img class="mb-2" v-if="tab_active.promotion" src="<?php echo base_url(); ?>assets/images/icons/annou-active.png?t=<?php echo date('Y-m-d') ?>" />
							<img class="mb-2" v-else src="<?php echo base_url(); ?>assets/images/icons/gift.png?t=<?php echo date('Y-m-d') ?>" />
							<div class="text-white font-weight-bold">
								โปรโมชั่น
							</div>
						</a>
					</div> -->



					<?php if($_SESSION['user']['role'] == roleAdmin() || $_SESSION['user']['role'] == roleSuperAdmin()): ?>
						<div class="menu-bar active align-self-center text-center mb-1">
							<a href="<?php echo base_url('admin'); ?>" target="_blank" class="text-decoration-none d-block">
								<img class="mb-2" src="<?php echo base_url(); ?>assets/images/icons/setting.png?t=<?php echo date('Y-m-d') ?>" />
								<div class="text-white font-weight-bold">
									แอดมิน
								</div>
							</a>
						</div>
					<?php endif; ?>
				</div>

				<div class="col-9 col-md-9 col-lg-12 pr-0 pl-0 mx-md-auto" v-if="tab_active.sport">
					<div class="row mx-auto center-menu">
						<div class="col-12 col-lg-6 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
							<a target="_blank" href="<?php echo base_url('home/play_game_once/M8')?>" class="text-decoration-none">
								<img class="game-img" src="<?php echo base_url(); ?>assets/images/menu/aksport.png?t=<?php echo date('Y-m-d') ?>" />
							</a>
						</div>
						<div class="col-12 col-lg-6 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
							<a target="_blank" href="<?php echo base_url('home/play_game_once/BC/SBO')?>" class="text-decoration-none">
								<img class="game-img" src="<?php echo base_url(); ?>assets/images/menu/sbobet.png?t=<?php echo date('Y-m-d') ?>"/>
							</a>
						</div>
						<div class="col-12 col-lg-6 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
							<a target="_blank" href="<?php echo base_url('home/play_game_once/SB')?>" class="text-decoration-none">
								<img class="game-img" src="<?php echo base_url(); ?>assets/images/menu/ts911.png?t=<?php echo date('Y-m-d') ?>" />
							</a>
						</div>

					</div>
				</div>

				<div class="col-9 col-md-9 col-lg-12 pr-0 pl-0 mx-md-auto" v-if="tab_active.esport">
					<div class="row mx-auto center-menu">
						<div class="col-12 col-lg-6 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
							<a target="_blank" href="<?php echo base_url('home/play_game_once/IA')?>" class="text-decoration-none">
								<img class="game-img" src="<?php echo base_url(); ?>assets/images/menu/iaesports.png?t=<?php echo date('Y-m-d') ?>" />
							</a>
						</div>
					</div>
				</div>

				<div class="col-9 col-md-9 col-lg-12 pr-0 pl-0 mx-md-auto" v-else-if="tab_active.casino">
					<div class="row mx-auto center-menu">
						<div class="col-12 col-lg-6 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
							<a  target="_blank" href="<?php echo base_url('home/play_game_once/SA')?>" class="text-decoration-none">
								<img class="game-img" src="<?php echo base_url(); ?>assets/images/menu/sa.png?t=<?php echo date('Y-m-d') ?>"/>
							</a>
						</div>
						<div class="col-12 col-lg-6 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
							<a  target="_blank" href="<?php echo base_url('home/play_game_once/AE')?>" class="text-decoration-none">
								<img class="game-img" src="<?php echo base_url(); ?>assets/images/menu/sexy.png?t=<?php echo date('Y-m-d') ?>" />
							</a>
						</div>
						<div class="col-12 col-lg-6 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
							<a  target="_blank" href="<?php echo base_url('home/play_game_once/WM')?>" class="text-decoration-none">
								<img class="game-img" src="<?php echo base_url(); ?>assets/images/menu/wm.png?t=<?php echo date('Y-m-d') ?>" />
							</a>
						</div>
						<div class="col-12 col-lg-6 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
							<a  target="_blank" href="<?php echo base_url('home/play_game_once/DG')?>" class="text-decoration-none">
								<img class="game-img" src="<?php echo base_url(); ?>assets/images/menu/dg.png?t=<?php echo date('Y-m-d') ?>" />
							</a>
						</div>
					</div>
				</div>

				<div class="col-9 col-md-9 col-lg-12 pr-0 pl-0 mx-md-auto" v-else-if="tab_active.game">
					<div class="row mx-auto center-menu">
						<div class="col-12 col-lg-6 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
							<a href="<?php echo base_url('home/play_game_once/AE/PG'); ?>" class="text-decoration-none">
								<img class="game-img" src="<?php echo base_url(); ?>assets/images/gamemobile/pg.png?t=<?php echo date('Y-m-d') ?>"/>
							</a>
						</div>
						<div class="col-12 col-lg-6 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
							<a href="<?php echo base_url('home/play_game_once/AE/PP'); ?>" class="text-decoration-none">
								<img class="game-img" src="<?php echo base_url(); ?>assets/images/gamemobile/pp.png?t=<?php echo date('Y-m-d') ?>"/>
							</a>
						</div>
						<div class="col-12 col-lg-6 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
							<a href="<?php echo base_url('lobby/SG'); ?>" class="text-decoration-none">
								<img class="game-img" src="<?php echo base_url(); ?>assets/images/gamemobile/h2sg.png?t=<?php echo date('Y-m-d') ?>" />
							</a>
						</div>
						<div class="col-12 col-lg-6 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
							<a href="<?php echo base_url('lobby/JK'); ?>" class="text-decoration-none">
								<img class="game-img" src="<?php echo base_url(); ?>assets/images/gamemobile/joker-game-1.png?t=<?php echo date('Y-m-d') ?>" />
							</a>
						</div>
						<div class="col-12 col-lg-6 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
							<a href="<?php echo base_url('lobby/XE'); ?>" class="text-decoration-none">
								<img class="game-img" src="<?php echo base_url(); ?>assets/images/gamemobile/xe88.png?t=<?php echo date('Y-m-d') ?>" />
							</a>
						</div>
						<div class="col-12 col-lg-6 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
							<a href="<?php echo base_url('home/play_game_once/AE/KM'); ?>" class="text-decoration-none">
								<img class="game-img" src="<?php echo base_url(); ?>assets/images/gamemobile/km.png?t=<?php echo date('Y-m-d') ?>" />
							</a>
						</div>
						<div class="col-12 col-lg-6 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
							<a href="<?php echo base_url('home/play_game_once/AE/FC'); ?>" class="text-decoration-none">
								<img class="game-img" src="<?php echo base_url(); ?>assets/images/gamemobile/fachai.png?t=<?php echo date('Y-m-d') ?>" />
							</a>
						</div>
					</div>
				</div>

				<div class="col-9 col-md-9 col-lg-12 pr-0 pl-0 mx-md-auto" v-else-if="tab_active.event">
					<div class="row mx-auto center-menu">
						<div class="col-12 col-lg-8 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
							<a href="<?php echo base_url('play_wheel'); ?>" class="text-decoration-none">
								<img class="game-img" src="<?php echo base_url(); ?>assets/images/wheel.png?t=<?php echo date('Y-m-d') ?>"/>
							</a>
						</div>
						<div class="col-12 col-lg-8 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
							<a href="<?php echo base_url('coupon'); ?>" class="text-decoration-none">
								<img class="game-img" src="<?php echo base_url(); ?>assets/images/coupon.png?t=<?php echo date('Y-m-d') ?>"/>
							</a>
						</div>
					</div>
				</div>

				<div class="col-9 col-md-9 col-lg-12 pr-0 pl-0 mx-md-auto" v-else-if="tab_active.account">
					<div class="row mx-auto center-menu">
						<div class="col-12 col-lg-8 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
							<a href="<?php echo base_url('profile'); ?>" class="text-decoration-none">
								<img class="game-img" src="<?php echo base_url(); ?>assets/images/changepass.png?t=<?php echo date('Y-m-d') ?>"/>
							</a>
						</div>
						<div class="col-12 col-lg-8 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
							<a href="<?php echo base_url('history'); ?>" class="text-decoration-none">
								<img class="game-img" src="<?php echo base_url(); ?>assets/images/history.png?t=<?php echo date('Y-m-d') ?>" />
							</a>
						</div>
					</div>
				</div>

				<div class="col-9 col-md-9 col-lg-12 pr-0 pl-0 mx-md-auto" v-else-if="tab_active.new">
					<div class="row mx-auto center-menu">
						<?php foreach($news as $new): ?>
							<?php if(!empty($new['image'])): ?>
								<div class="col-12 col-lg-8 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
									<div class="" style="width:100%;">
										<a href="#" data-toggle="modal" data-target="#newModal<?php echo $new['id']; ?>">
											<img  src="<?php echo $new['image_url']; ?>" class="game-img" alt="<?php echo $new['name']; ?>">
										</a>
									</div>
								</div>
								<div class="modal" id="newModal<?php echo $new['id']; ?>" data-keyboard="false" data-backdrop="static" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-body">
												<div class="text-center">
													<img style="width: 100%" src="<?php echo $new['image_url']; ?>" class="img-fluid" alt="<?php echo $new['name']; ?>">
												</div>
												<div class="row" style="">
													<div class="col-12 text-center">
														<p class="text-white mt-2 mb-2"><?php echo $new['name']; ?>
														</p>
													</div>
													<div class="col-12 text-right">
														<a href="#" data-dismiss="modal" data-target="#newModal<?php echo $new['id']; ?>" class="btn btn-submit">ปิด</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php endif; ?>
						<?php endforeach; ?>
					</div>
				</div>


				<div class="col-9 col-md-9 col-lg-12 pr-0 pl-0 mx-md-auto" v-else-if="tab_active.promotion">
					<div class="row mx-auto center-menu">
						<?php foreach($promotions as $promotion): ?>
							<?php if(!empty($promotion['image']) && ($promotion['percent'] > 0 || ($promotion['fix_amount_deposit'] > 0  && $promotion['category'] == "2"))): ?>
								<div class="col-12 col-lg-8 col-offset-md-4 mx-auto text-center center-menu-box p-2 p-lg-2">
									<div class="" style="width:100%;">
										<a href="#" data-toggle="modal" data-target="#promotionModal<?php echo $promotion['id']; ?>">
											<img  src="<?php echo $promotion['image_url']; ?>" class="game-img" alt="<?php echo $promotion['name']; ?>">
										</a>
									</div>
								</div>
								<div class="modal" id="promotionModal<?php echo $promotion['id']; ?>" data-keyboard="false" data-backdrop="static" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-body">
												<div class="text-center">
													<img style="width: 100%" src="<?php echo $promotion['image_url']; ?>" class="img-fluid" alt="<?php echo $promotion['name']; ?>">
												</div>
												<div class="row" style="">
													<div class="col-12">
														<p class="text-white text-center mt-2 mb-2"><?php echo $promotion['name']; ?>&nbsp;
															<?php if ($promotion['max_value']>0 && $promotion['category'] == "1"): ?>
																สูงสุด <?php echo number_format($promotion['max_value']) ?> บาท (ทำเทิร์น <?php echo $promotion['turn'] ?> เท่า)
															<?php elseif ($promotion['category'] == "2"): ?>
																(ทำเทิร์น <?php echo $promotion['turn'] ?> เท่า)
															<?php endif; ?>
														</p>
													</div>
													<div class="col-12 text-right">
														<a href="#" data-dismiss="modal" data-target="#promotionModal<?php echo $promotion['id']; ?>" class="btn btn-submit">ปิด</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php endif; ?>
						<?php endforeach; ?>
					</div>
				</div>

			</div>

		</div>
	</div>
</section>
<loading :active.sync="pre_loader"
		 :can-cancel="false"
		 :width="80"
		 :height="60"
		 :opacity="0.2"
		 color="#fff"
		 :is-full-page="true"></loading>
<?php if(!isset($_SESSION['alert_notify_promotion'])): ?>
    <?php
		$_SESSION['alert_notify_promotion'] = true;
	?>
	<div class="modal" id="promotionModal" tabindex="-1" aria-labelledby="exampleModalLabelass" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content" style="background:inherit;">
				<div class="modal-body">
					<div class="text-right mb-3" style="margin-right:.5rem; cursor:pointer;" data-dismiss="modal">
						<span class="close-modal">X</span>
					</div>
					<div class="row" style="">
						<div class="col-12">
							<div class="mb-2">
								<img src="<?php echo base_url() ?>/assets/images/promotions/300400.png?t=<?php echo date('Y-m-d') ?>" class="d-block w-100 rounded" alt="...">
							</div>
							<div class="mb-2">
								<img src="<?php echo base_url() ?>/assets/images/promotions/Pro 10_-4.png?t=<?php echo date('Y-m-d') ?>" class="d-block w-100 rounded" alt="...">
							</div>
							<div class="mb-2">
								<img src="<?php echo base_url() ?>/assets/images/promotions/3_-2.png?t=<?php echo date('Y-m-d') ?>" class="d-block w-100 rounded" alt="...">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>   
        $(document).ready(function() {
			function alignModal() {
				var modalDialog = $(this).find(".modal-dialog");
				modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 1.1));
			}
			$(".modal").on("shown.bs.modal", alignModal);
			$(window).on("resize", function() {
				$(".modal:visible").each(alignModal);
			});
			//$("#promotionModal").modal('show')
		});
	</script>
<?php endif; ?>
<script>
	const dashboard_tab = "<?php echo isset($_GET['tab']) ? $_GET['tab'] : 'casino'; ?>";
</script>
<script src="<?php echo base_url('assets/scripts/dashboard.js?').time() ?>"></script>