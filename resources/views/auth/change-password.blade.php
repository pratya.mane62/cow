<x-guest-layout>
    <div class="container pt-1">
        <div class="row justify-content-center mt-5">
            <div class="col-12 col-md-7 col-lg-5">
                <div class="card py-1 px-3">
                    <div class="card-body">
                        <x-auth-session-status class="mb-4" :status="session('status')" />
                        <x-auth-validation-errors class="mb-4" :errors="$errors" />
                        <div class="d-flex justify-content-center align-items-center">
                            <img src="{{ asset('images/sut.png') }}" height="150px">
                            <img src="{{ asset('images/techno.png') }}" height="80px">
                        </div>
                        <div class="d-flex justify-content-center">
                            <h4 class=" text-center">{{ __('NewPassword') }}</h4>
                        </div>

                        <div class="card-content mt-4">
                            <form method="POST" action="{{ route('change.password') }}">
                                @csrf

                                <!-- Password -->
                                <div class="form-group">
                                    <x-label for="old_password" :value="__('OldPassword')" />

                                    <x-input id="old_password"
                                        class="form-control {{ $errors->first('old_password') ? 'border-danger' : '' }}"
                                        type="password" name="old_password" required autocomplete="new-password" />
                                    @error('old_password')
                                        <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <!-- Password -->
                                <div class="form-group">
                                    <x-label for="password" :value="__('Password')" />

                                    <x-input id="password"
                                        class="form-control {{ $errors->first('password') ? 'border-danger' : '' }}"
                                        type="password" name="password" required autocomplete="new-password" />
                                    @error('password')
                                        <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <!-- Confirm Password -->
                                <div class="form-group">
                                    <x-label for="password_confirmation" :value="__('RePassword')" />

                                    <x-input id="password_confirmation"
                                        class="form-control {{ $errors->first('password_confirmation') ? 'border-danger' : '' }}"
                                        type="password" name="password_confirmation" required />

                                    @error('password_confirmation')
                                        <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="flex items-center justify-end mt-4">
                                    <button class="btn btn-success">
                                        {{ __('EditData') }}
                                    </button>
                                    <a class="btn btn-danger" href="{{ route('admin.dashboard.index') }}">
                                        {{ __('Cancel') }}
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</x-guest-layout>
