<guest-layout>
    <auth-card>
        <slot name="logo">
            <a href="/">
                <application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </slot>

        <!-- Validation Errors -->
        <auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('password.update') }}">
            @csrf

            <!-- Password Reset Token -->
            <input type="hidden" name="token" value="{{ $request->route('token') }}">

            <!-- Email Address -->
            <div>
                <x-label for="phone" :value="__('Phone')" />

                <input id="phone" class="block mt-1 w-full" type="text" min="10" mxx="10" name="phone" :value="old('phone', $request->phone)" required autofocus />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />
                <input id="password" class="block mt-1 w-full" type="password" name="password" required />
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-label for="password_confirmation" :value="__('Confirm Password')" />

                <input id="password_confirmation" class="block mt-1 w-full"
                                    type="password"
                                    name="password_confirmation" required />
            </div>

            <div class="flex items-center justify-end mt-4">
                <button>
                    {{ __('Reset Password') }}
                </button>
            </div>
        </form>
    </auth-card>
</guest-layout>
