<x-guest-layout>
    <div class="container pt-1">
        <div class="row justify-content-center mt-5">
            <div class="col-12 col-md-7 col-lg-5">
                <div class="card py-1 px-3">
                    <div class="card-body">
                        <x-auth-session-status class="mb-4" :status="session('status')" />
                        <x-auth-validation-errors class="mb-4" :errors="$errors" />
                        <div class="d-flex justify-content-center align-items-center">
                            <img src="{{ asset('images/sut.png') }}" height="150px">
                            <img src="{{ asset('images/techno.png') }}" height="80px">
                        </div>
                        <div class="d-flex justify-content-center">
                            <h4 class=" text-center">
                                ระบบสืบย้อนกลับผลิตภัณฑ์<br>โคขุนพันธุ์โคราชวากิว</h4>
                        </div>

                        <div class="card-content mt-4">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class=" form-group">
                                    <x-label for="phone" :value="__('Phone')" />
                                    <input id="phone" class="form-control" type="text" name="phone"
                                        :value="old('phone')" required autofocus />
                                </div>

                                <!-- Password -->
                                <div class="form-group">
                                    <x-label for="password" :value="__('Password')" />

                                    <input id="password" class="form-control" type="password" name="password" required
                                        autocomplete="current-password" />
                                </div>

                                <!-- Remember Me -->
                                <div class="form-group">
                                    <label for="remember_me" class="inline-flex items-center">
                                        <input id="remember_me" type="checkbox"
                                            class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                            name="remember">
                                        <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                                    </label>
                                </div>

                                <div class="flex items-center justify-end mt-4">
                                    <button class="btn btn-success">
                                        {{ __('Login') }}
                                    </button>
                                    {{-- <a class="underline text-sm text-gray-600 hover:text-gray-900"
                                        href="{{ route('register') }}">
                                        {{ __('สมัครสมาชิก') }}
                                    </a> --}}

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
