<x-guest-layout>
    <div class="container pt-1">
        <div class="row justify-content-center mt-5">
            <div class="col-12 col-md-7 col-lg-5">
                <div class="card py-1 px-3">
                    <div class="card-body">
                        <x-auth-session-status class="mb-4" :status="session('status')" />
                        <x-auth-validation-errors class="mb-4" :errors="$errors" />
                        <div class="d-flex justify-content-center align-items-center">
                            <img src="{{ asset('images/sut.png') }}" height="150px">
                            <img src="{{ asset('images/techno.png') }}" height="80px">
                        </div>
                        <div class="d-flex justify-content-center">
                            <h4 class=" text-center">
                                ระบบสืบย้อนกลับผลิตภัณฑ์<br>โคขุนพันธุ์โคราชวากิว</h4>
                        </div>

                        <div class="card-content mt-4">
                            <form method="POST" action="{{ route('register') }}">
                                @csrf

                                <!-- Name -->
                                <div>
                                    <x-label for="card_id" :value="__('IdCard')" />
                                    <x-input id="card_id"
                                        class="form-control {{ $errors->first('card_id') ? 'border-danger' : '' }}"
                                        type="text" name="card_id" :value="old('card_id')" required autofocus />
                                    @error('card_id')
                                        <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <!-- Name -->
                                <div>
                                    <x-label for="name" :value="__('Name')" />

                                    <x-input id="name"
                                        class="form-control {{ $errors->first('name') ? 'border-danger' : '' }}"
                                        type="text" name="name" :value="old('name')" required autofocus />
                                    @error('name')
                                        <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <!-- Email Address -->
                                <div class="form-group">
                                    <x-label for="phone" :value="__('Phone')" />

                                    <x-input id="phone"
                                        class="form-control {{ $errors->first('phone') ? 'border-danger' : '' }}"
                                        type="text" name="phone" :value="old('phone')" required />
                                    @error('phone')
                                        <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <!-- Password -->
                                <div class="form-group">
                                    <x-label for="password" :value="__('Password')" />

                                    <x-input id="password"
                                        class="form-control {{ $errors->first('password') ? 'border-danger' : '' }}"
                                        type="password" name="password" required autocomplete="new-password" />
                                    @error('password')
                                        <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <!-- Confirm Password -->
                                <div class="form-group">
                                    <x-label for="password_confirmation" :value="__('RePassword')" />

                                    <x-input id="password_confirmation"
                                        class="form-control {{ $errors->first('password_confirmation') ? 'border-danger' : '' }}"
                                        type="password" name="password_confirmation" required />

                                    @error('password_confirmation')
                                        <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="flex items-center justify-end mt-4">
                                    <button class="btn btn-success">
                                        {{ __('Register') }}
                                    </button>
                                    <a class="underline text-sm text-gray-600 hover:text-gray-900"
                                        href="{{ route('login') }}">
                                        {{ __('มีบัญชีผู้ใช้แล้ว?') }}
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</x-guest-layout>
