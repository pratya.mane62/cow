<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $title }}</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href=" {{ asset('plugins/fontawesome-free/css/all.min.css') }} ">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet"
        href=" {{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }} ">
    <!-- iCheck -->
    <link rel="stylesheet" href=" {{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }} ">
    <!-- JQVMap -->
    <link rel="stylesheet" href=" {{ asset('plugins/jqvmap/jqvmap.min.css') }} ">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href=" {{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }} ">
    <!-- Daterange picker -->
    <link rel="stylesheet" href=" {{ asset('plugins/daterangepicker/daterangepicker.css') }} ">
    <!-- summernote -->
    <link rel="stylesheet" href=" {{ asset('plugins/summernote/summernote-bs4.min.css') }} ">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.7/css/rowReorder.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css"
        rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    <style>
        @import url('https://fonts.googleapis.com/css2?family=Prompt:wght@100;200;300;400&display=swap');

        body {
            font-family: 'Prompt', sans-serif;
        }

        table>thead>tr>th {
            white-space: nowrap;
            min-width: 100px;
        }

        hr {
            border-top: 1px solid #4f5962
        }

    </style>
    @yield('styles')
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed">
    <div class="wrapper">

        <!-- Preloader -->
        {{-- <div class="preloader flex-column justify-content-center align-items-center">
            <img class="animation__shake" src="{{ asset('dist/img/AdminLTELogo.png') }}" alt="AdminLTELogo"
                height="60" width="60">
        </div> --}}

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>
            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                        <i class="fas fa-expand-arrows-alt"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                        <i class="fas fa-th-large"></i>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">

            <!-- Sidebar -->
            <div class="sidebar mt-1">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel ">
                    <div class="info">
                        <b class="d-block text-light">{{ auth()->user()->name }}</b>
                        @switch(auth()->user()->status)
                            @case('admin')
                                <span class="d-block text-light">ผู้ดูแลระบบ</span>
                            @break
                            @case('farm')
                                <span class="d-block text-light">เจ้าของฟาร์ม</span>
                            @break
                            @case('slaugh')
                                <span class="d-block text-light">ผู้ดูแลโรงเชือด</span>
                            @break
                            @default
                        @endswitch
                        @if (auth()->user()->etps->count() > 0)
                            <span class="d-block text-light">ผู้ดูแลวิสาหกิจ</span>
                        @endif
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">

                        <li class="nav-item">
                            <a href="{{ route('admin.dashboard.index') }}" class="nav-link @if (Route::current()->getName() === 'admin.dashboard.index') active @endif">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    {{ __('Dashboard') }}
                                </p>
                            </a>
                            <hr>
                        </li>
                        @can('isAdmin')
                            <li class="nav-item">
                                <a href="{{ route('admin.details.index') }}" class="nav-link @if (Route::current()->getName() === 'admin.details.index') active @endif">
                                    <i class="nav-icon fas fa-users"></i>
                                    <p>
                                        {{ __('ข้อมูลผู้ใช้') }}
                                    </p>
                                </a>
                            </li>
                        @endcan

                        @can('isEtp')
                            <li class="nav-item">
                                <a href="{{ route('admin.enterpriseser.index') }}" class="nav-link @if (Route::current()->getName() === 'admin.enterpriseser.index') active @endif">
                                    <i class="nav-icon fas fa-user-tie"></i>
                                    <p>
                                        {{ __('ข้อมูลกลุ่มวิสาหกิจ') }}
                                    </p>
                                </a>
                            </li>
                        @endcan

                        @can('isFarm')
                            <li class="nav-item">
                                <a href="{{ route('admin.farms.index') }}" class="nav-link @if (Route::current()->getName() === 'admin.farms.index') active @endif">
                                    <i class="nav-icon fas fa-hat-cowboy"></i>
                                    <p>
                                        {{ __('ข้อมูลฟาร์ม') }}
                                    </p>
                                </a>
                            </li>
                        @endcan

                        @can('isAdmin')
                            <li class="nav-item">
                                <a href="{{ route('admin.rfid.index') }}" class="nav-link @if (Route::current()->getName() === 'admin.rfid.index') active @endif">
                                    <i class="nav-icon fas fa-tag"></i>
                                    <p>
                                        {{ __('ข้อมูล RFID-TAG') }}
                                    </p>
                                </a>
                            </li>
                        @endcan

                        @can('isFarm')
                            <li class="nav-item">
                                <hr>
                                <a href="{{ route('admin.cows.index') }}" class="nav-link @if (Route::current()->getName() === 'admin.cows.index') active @endif">
                                    <i class="nav-icon fas fa-paw"></i>
                                    <p>
                                        {{ __('ข้อมูลวัว') }}
                                    </p>
                                </a>
                            </li>
                        @endcan

                        @can('isAdmin')
                            <li class="nav-item">
                                <a href="{{ route('admin.species.index') }}" class="nav-link @if (Route::current()->getName() === 'admin.species.index') active @endif">
                                    <i class="nav-icon fas fa-code-branch"></i>
                                    <p>
                                        {{ __('สายพันธุ์วัว') }}
                                    </p>
                                </a>
                            </li>
                        @endcan

                        @can('isFarm')
                            <li class="nav-item">
                                <a href="{{ route('admin.breeding.index') }}" class="nav-link @if (Route::current()->getName() === 'admin.breeding.index') active @endif">
                                    <i class="nav-icon fas fa-wind"></i>
                                    <p>
                                        {{ __('ข้อมูลผสมเทียม') }}
                                    </p>
                                </a>
                            </li>
                        @endcan

                        @can('isFarm')
                            <li class="nav-item">
                                <a href="{{ route('admin.sperms.index') }}" class="nav-link @if (Route::current()->getName() === 'admin.sperms.index') active @endif">
                                    <i class="nav-icon fas  fa-tint"></i>
                                    <p>
                                        {{ __('น้ำเชื้อพ่อพันธุ์') }}
                                    </p>
                                </a>
                                <hr>
                            </li>
                        @endcan

                        @can('isSlaughterhouse')
                            <li class="nav-item">
                                <a href="{{ route('admin.slaugh.index') }}" class="nav-link @if (Route::current()->getName() === 'admin.slaugh.index') active @endif">
                                    <i class="nav-icon fas fa-skull"></i>
                                    <p>
                                        {{ __('ข้อมูลโรงเชือด') }}
                                    </p>
                                </a>
                            </li>
                        @endcan

                        @if (Gate::check('isSlaughterhouse'))
                            <li class="nav-item">
                                <a href="{{ route('admin.cows.waiting') }}" class="nav-link @if (Route::current()->getName() === 'admin.cows.waiting') active @endif">
                                    <i class="nav-icon fas fa-skull-crossbones"></i>
                                    <p>
                                        {{ __('วัวที่รอเชือด') }}
                                    </p>
                                </a>
                            </li>
                        @endif

                        @if (Gate::check('isEtp') || Gate::check('isSlaughterhouse'))
                            <li class="nav-item">
                                <a href="{{ route('admin.histories.index') }}" class="nav-link @if (Route::current()->getName() === 'admin.histories.index') active @endif">
                                    <i class="nav-icon fas fa-history"></i>
                                    <p>
                                        {{ __('HistoryCow') }}
                                    </p>
                                </a>
                                <hr>
                            </li>
                        @endif

                        <li class="nav-item">
                            <a href="{{ route('change.password') }}" class="nav-link @if (Route::current()->getName() === 'change.password') active @endif">
                                <i class="nav-icon fas fa-lock"></i>
                                <p>
                                    {{ __('NewPassword') }}
                                </p>
                            </a>
                        </li>
                        <form method="POST" action="{{ route('logout') }}" class="nav-item">
                            @csrf

                            <a :href="route('logout')" class=" nav-link" style="cursor: pointer" onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                <i class="nav-icon fas fa-sign-out-alt"></i>
                                <p> {{ __('Logout') }}</p>
                            </a>
                            <hr>
                        </form>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            {{-- <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Dashboard v1</li>
                            </ol>
                        </div><!-- /.row -->
                    </div><!-- /.container-fluid -->
                </div> --}}
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid py-4">
                    <main>
                        {{ $slot }}
                    </main>
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }} "></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }} "></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- Bootstrap 4 -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }} "></script>
    <!-- ChartJS -->
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }} "></script>
    <!-- Sparkline -->
    <script src="{{ asset('plugins/sparklines/sparkline.js') }} "></script>
    <!-- JQVMap -->
    <script src="{{ asset('plugins/jqvmap/jquery.vmap.min.js') }} "></script>
    <script src="{{ asset('plugins/jqvmap/maps/jquery.vmap.usa.js') }} "></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }}"></script>
    <!-- daterangepicker -->
    <script src="{{ asset('plugins/moment/moment.min.js') }} "></script>
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <!-- Summernote -->
    <script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }} "></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }} "></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.js') }} "></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }} "></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{ asset('dist/js/pages/dashboard.js') }} "></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
    <script script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"> </script>
    {{-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script> --}}
    <script type="text/javascript"
        src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/JQL.min.js"></script>
    <script type="text/javascript"
        src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/typeahead.bundle.js">
    </script>

    <link rel="stylesheet"
        href="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dist/jquery.Thailand.min.css">
    <script type="text/javascript"
        src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script>
        $('#multi_user').select2();
        $.Thailand({
            $district: $('#district'), // input ของตำบล
            $amphoe: $('#amphoe'), // input ของอำเภอ
            $province: $('#province'), // input ของจังหวัด
            $zipcode: $('#zipcode'), // input ของรหัสไปรษณีย์
        });
        $(document).ready(function() {
            const $valueSpan = $('.valueSpan');
            const $value = $('#slider11');

            const f5blood_dad = $('#f5blood_dad')
            const f5blood_mom = $('#f5blood_mom')
            const f4blood_dad = $('#f4blood_dad')
            const f4blood_mom = $('#f4blood_mom')
            const f3blood_dad = $('#f3blood_dad')
            const f3blood_mom = $('#f3blood_mom')
            const f2blood_dad = $('#f2blood_dad')
            const f2blood_mom = $('#f2blood_mom')
            const f1blood_dad = $('#f1blood_dad')
            const f1blood_mom = $('#f1blood_mom')

            const f5blood_dad_val = $('.f5blood_dad_val')
            const f5blood_mom_val = $('.f5blood_mom_val')
            const f4blood_dad_val = $('.f4blood_dad_val')
            const f4blood_mom_val = $('.f4blood_mom_val')
            const f3blood_dad_val = $('.f3blood_dad_val')
            const f3blood_mom_val = $('.f3blood_mom_val')
            const f2blood_dad_val = $('.f2blood_dad_val')
            const f2blood_mom_val = $('.f2blood_mom_val')
            const f1blood_dad_val = $('.f1blood_dad_val')
            const f1blood_mom_val = $('.f1blood_mom_val')

            f5blood_dad_val.html(f5blood_dad.val());
            f5blood_dad.on('input change', () => {
                f5blood_dad_val.html(f5blood_dad.val());
            });

            f4blood_dad_val.html(f4blood_dad.val());
            f4blood_dad.on('input change', () => {
                f4blood_dad_val.html(f4blood_dad.val());
            });

            f3blood_dad_val.html(f3blood_dad.val());
            f3blood_dad.on('input change', () => {
                f3blood_dad_val.html(f3blood_dad.val());
            });

            f2blood_dad_val.html(f2blood_dad.val());
            f2blood_dad.on('input change', () => {
                f2blood_dad_val.html(f2blood_dad.val());
            });

            f1blood_dad_val.html(f1blood_dad.val());
            f1blood_dad.on('input change', () => {
                f1blood_dad_val.html(f1blood_dad.val());
            });

            f5blood_mom_val.html(f5blood_mom.val());
            f5blood_mom.on('input change', () => {
                f5blood_mom_val.html(f5blood_mom.val());
            });

            f4blood_mom_val.html(f4blood_mom.val());
            f4blood_mom.on('input change', () => {
                f4blood_mom_val.html(f4blood_mom.val());
            });

            f3blood_mom_val.html(f3blood_mom.val());
            f3blood_mom.on('input change', () => {
                f3blood_mom_val.html(f3blood_mom.val());
            });

            f2blood_mom_val.html(f2blood_mom.val());
            f2blood_mom.on('input change', () => {
                f2blood_mom_val.html(f2blood_mom.val());
            });

            f1blood_mom_val.html(f1blood_mom.val());
            f1blood_mom.on('input change', () => {
                f1blood_mom_val.html(f1blood_mom.val());
            });

            function readURL(input) {
                if (input.files && input.files[0] && $(input).attr('id') == 'imgInp') {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#blah').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                } else if (input.files && input.files[0] && $(input).attr('id') == 'imgInprfid') {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#blahrfid').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#imgInp").change(function() {
                readURL(this);
            });

            $("#imgInprfid").change(function() {
                readURL(this);
            });

            $('.table_id').DataTable({
                responsive: false,
                scrollX: true,
            });
            @if (Session::has('message'))
                toastr.options =
                {
                "closeButton" : true,
                "progressBar" : true
                }
                toastr.success("{{ session('message') }}");
            @endif

            @if (Session::has('error'))
                toastr.options =
                {
                "closeButton" : true,
                "progressBar" : true
                }
                toastr.error("{{ session('error') }}");
            @endif

            @if (Session::has('info'))
                toastr.options =
                {
                "closeButton" : true,
                "progressBar" : true
                }
                toastr.info("{{ session('info') }}");
            @endif

            @if (Session::has('warning'))
                toastr.options =
                {
                "closeButton" : true,
                "progressBar" : true
                }
                toastr.warning("{{ session('warning') }}");
            @endif
        });

    </script>

    @yield('scripts')
</body>

</html>
