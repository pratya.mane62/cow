<x-adminlte-layout>
    <x-slot name="title">
        {{ __('Sperm') }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">
            <div class="float-left">
                <h4>{{ __('Sperm') }}</h4>
            </div>
            @can('isFarm')
                <a href=" {{ route('admin.sperms.create') }} " class=" btn btn-info float-right">
                    {{ __('AddData') }}
                </a>
            @endcan
        </div>
        <div class="card-body">
            <div class="card-content">
                <table class=" table_id " style="width:100%;" >
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>พ่อพันธุ์</th>
                            <th>สายพันธุ์</th>
                            <th>แหล่งน้ำเชื้อ</th>
                            <th>action</th>
                        </tr>
                    </thead>
                    @isset($sperms)
                        <tbody>
                            @foreach ($sperms as $i => $sperm)
                                <tr>
                                    <td>
                                        {{ ++$i }}
                                    </td>
                                    <td>{{ $sperm->bull }} </td>
                                    <td>{{ $sperm->specie->title }} </td>
                                    <td>{{ $sperm->resource }} </td>
                                    <td width="100">
                                        <a href="{{ route('admin.sperms.edit', $sperm) }} "
                                            class=" btn btn-warning btn-sm mb-1" style="width: 80px">{{ __('Edit') }}</a>
                                        <div class="mx-1"></div>
                                        <form action="{{ route('admin.sperms.destroy', $sperm) }} " method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class=" btn btn-danger btn-sm" style="width: 80px"
                                                onclick="return confirm('Are you sure?')">{{ __('Delete') }}</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    @endisset
                </table>
            </div>
        </div>
    </div>
</x-adminlte-layout>
