<x-adminlte-layout>
    <x-slot name="title">
        {{ __('Sperm') }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">

            <h4> {{ __('Sperm') }}</h4>
        </div>
        <div class="card-body">
            <x-auth-validation-errors class="mb-4" :errors="$errors" />
            <div class="card-content row justify-content-center">
                <form action=" {{ route('admin.sperms.store') }} " method="post" id="myForm"
                    enctype="multipart/form-data" class=" col-12 col-xl-12 row">
                    @csrf

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="bull">พ่อพันธุ์</label>
                        <input type="text" name="bull" value="{{ old('bull', '') }} " id="bull" class="form-control">
                        @error('bull')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="specie_id">สายพันธุ์</label>
                        <select class="form-control" name="specie_id" id="specie_id">
                            @if ($species->count() > 0)
                                @foreach ($species as $specie)
                                    <option value="{{ $specie->id }} " @if (old('specie_id') == $specie->id) selected="selected" @endif>
                                        {{ $specie->title }}</option>
                                @endforeach
                            @endif
                        </select>
                        @error('specie_id')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="resource">แหล่งน้ำเชื้อ</label>
                        <textarea name="resource" id="resource" cols="30" rows="3"
                            class=" form-control">{{ old('resource', '') }}</textarea>
                        @error('resource')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group mt-4 col-12">
                        <button class="btn btn-success float-right" id="addCowBtn"
                            type="submit">{{ __('AddData') }}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</x-adminlte-layout>
