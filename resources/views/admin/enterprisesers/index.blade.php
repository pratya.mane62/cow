<x-adminlte-layout>
    <x-slot name="title">
        {{ __('กลุ่มวิสาหกิจ') }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">
            <div class="float-left">
                <h4>{{ __('กลุ่มวิสาหกิจ') }}</h4>
            </div>
            @can('isAdmin')
                <a href=" {{ route('admin.enterpriseser.create') }} " class=" btn btn-info float-right">
                    {{ __('AddData') }}
                </a>
            @endcan
        </div>
        <div class="card-body">
            <div class="card-content">
                <table class=" table_id " style="width:100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>ชื่อ</th>
                            <th>ผู้ดูแลทั้งหมด</th>
                            <th>ฟาร์มทั้งหมด</th>
                            <th>action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($etps as $i => $enterpriseser)
                            <tr>
                                <td>
                                    {{ ++$i }}
                                </td>
                                <td>{{ $enterpriseser->name }} </td>
                                <td>{{ $enterpriseser->users->count() }} </td>
                                <td>{{ $enterpriseser->farms->count() }} </td>
                                <td width="100">
                                    <a href="{{ route('admin.enterpriseser.edit', $enterpriseser) }} "
                                        class=" btn btn-warning btn-sm mb-1" style="width: 80px">{{ __('Edit') }}</a>
                                    <div class="mx-1"></div>
                                    @can('isAdmin') <form
                                            action="{{ route('admin.enterpriseser.destroy', $enterpriseser) }} "
                                            method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class=" btn btn-danger btn-sm" style="width: 80px"
                                                onclick="return confirm('Are you sure?')">{{ __('Delete') }}</button>
                                        </form>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-adminlte-layout>
