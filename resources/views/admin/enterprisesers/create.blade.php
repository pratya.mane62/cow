<x-adminlte-layout>
    <x-slot name="title">
        {{ __('กลุ่มวิสาหกิจ') }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">
            <h4> {{ __('กลุ่มวิสาหกิจ') }}</h4>
        </div>
        <div class="card-body">
            <x-auth-validation-errors class="mb-4" :errors="$errors" />
            <div class="card-content row justify-content-center">
                <form action=" {{ route('admin.enterpriseser.store') }} " method="post" id="myForm"
                    class=" col-12 col-xl-12 row">
                    @csrf

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="name">ชื่อกลุ่มวิสาหกิจ</label>
                        <input type="text" name="name" value="{{ old('name', '') }} " id="name" class="form-control">
                        @error('name')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="location">ที่ตั้ง</label>
                        <input type="text" name="location" value="{{ old('location', '') }} " id="location"
                            class="form-control">
                        @error('location')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="coordinates">พิกัด</label>
                        <input type="text" name="coordinates" value="{{ old('coordinates', '') }} " id="coordinates"
                            class="form-control">
                        @error('coordinates')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="user_id">ผู้ดูแล</label>
                        <select multiple="multiple" class="js-example-basic-multiple form-control" id="multi_user"
                            name="user_id[]">
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        </select>
                        @error('user_id')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-12">
                        <button class="btn btn-primary" type="submit">เพิ่ม</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</x-adminlte-layout>
