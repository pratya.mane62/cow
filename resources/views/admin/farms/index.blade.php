<x-adminlte-layout>
    <x-slot name="title">
        {{ __('Farm') }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">
            <div class="float-left">
                <h4>{{ __('Farm') }}</h4>
            </div>
            @can('isFarm')
                <a href=" {{ route('admin.farms.create') }} " class=" btn btn-info float-right">
                    {{ __('AddData') }}
                </a>
            @endcan
        </div>
        <div class="card-body">
            <div class="card-content">
                <table class=" table_id " style="width:100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>ชื่อฟาร์ม</th>
                            @can('isAdmin') <th>เจ้าของฟาร์ม</th> @endcan
                            <th>กลุ่มวิสาหกิจที่สังกัด</th>
                            <th>
                                <center>วัวทั้งหมด</center>
                            </th>
                            <th>ประเถทของฟาร์ม</th>
                            <th>action</th>
                        </tr>
                    </thead>
                    @if ($farms->count() > 0)
                        <tbody>
                            @foreach ($farms as $i => $farm)
                                <tr>
                                    <td align="center">
                                        @isset($farm->photo)
                                            <img src="{{ asset('storage/uploads/' . $farm->photo) }}" style=" width:100px"
                                                alt="{{ $farm->name }}" class=" img-fluid img-thumbnail">
                                        @endisset
                                    </td>
                                    <td>{{ $farm->title }} </td>
                                    @can('isAdmin') <td>{{ $farm->user->name ?? null }}</td> @endcan
                                    <td>
                                        <span>{{ $farm->etp->name ?? null }}</span>
                                    </td>
                                    <td align="center">
                                        <a href="{{ route('admin.farm.cows', $farm) }}">
                                            {{ $farm->cows->count() }}
                                        </a>
                                    </td>
                                    <td>
                                        <span> {{ $farm->headwaters == 1 ? 'ต้นน้ำ  ' : '' }}
                                            {{ $farm->middle == 1 ? 'กลางน้ำ  ' : '' }}
                                            {{ $farm->downstream == 1 ? 'ปลายน้ำ ' : '' }}</span>
                                    </td>
                                    <td width="100">
                                        <a href="{{ route('admin.farms.edit', ['farm' => $farm]) }} "
                                            class=" btn btn-warning btn-sm mb-1"
                                            style="width: 80px">{{ __('Edit') }}</a>
                                        <div class="mx-1"></div>
                                        <form action="{{ route('admin.farms.destroy', ['farm' => $farm]) }} "
                                            method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class=" btn btn-danger btn-sm mb-1"
                                                style="width: 80px"
                                                onclick="return confirm('Are you sure?')">{{ __('Delete') }}</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</x-adminlte-layout>
