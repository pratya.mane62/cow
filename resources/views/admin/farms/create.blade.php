<x-adminlte-layout>
    <x-slot name="title">
        {{ __('Farm') }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">

            <h4> {{ __('Farm') }}</h4>
        </div>
        <div class="card-body">
            <div class="card-content">
                <x-auth-validation-errors class="mb-4" :errors="$errors" />
                <form action="{{ route('admin.farms.store') }} " method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-xl-6">
                            <label for="photo"> รูป </label>
                            <img id="blah" src="#" alt="รูปภาพของคุณ" class=" img-thumbnail img-fluid w-100" />
                        </div>

                        <div class="col-12 col-xl-6">
                            <label for="photo"> รูป </label>
                            <input type="file" name="photo" value="{{ old('photo', '') }} " class="form-control"
                                id="imgInp">
                            @error('photo')
                                <small class=" text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <input type="hidden" name="type" id="type" value="farm">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="farmtype">ประเภทของฟาร์ม ( เลือกได้มากกว่า 1 ข้อ )</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox"  id="defaultCheck1" name="headwaters">
                                    <label class="form-check-label" for="defaultCheck1">
                                        ต้นน้ำ
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox"  id="defaultCheck2" name="middle">
                                    <label class="form-check-label" for="defaultCheck2">
                                        กลางน้ำ
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox"  id="defaultCheck3" name="downstream">
                                    <label class="form-check-label" for="defaultCheck3">
                                        ปลายน้ำ
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="standard">มาตรฐานฟาร์ม</label>
                                <textarea name="standard" id="standard" cols="30" rows="2"
                                    class="form-control">{{ old('standard', '') }}</textarea>
                                @error('standard')
                                    <small class=" text-danger">{{ $message }} </small>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="traceback">ข้อมูลสืบย้อนกลับ</label>
                                <textarea name="traceback" id="traceback" cols="30" rows="2"
                                    class="form-control">{{ old('traceback', '') }}</textarea>
                                @error('traceback')
                                    <small class=" text-danger">{{ $message }} </small>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="latlong">พิกัดฟาร์ม</label>
                                <input type="number" step="0.000000001" name="latlong" id=""
                                    value="{{ old('latlong', '') }}" class="form-control" placeholder="" />
                                @error('latlong')
                                    <small class=" text-danger">{{ $message }} </small>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="">ชื่อฟาร์ม</label>
                                <input type="text" name="title" id="farm_title" class="form-control" placeholder=""
                                    value="{{ old('title', '') }}" />
                                @error('title')
                                    <small class=" text-danger">{{ $message }} </small>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="">สถานที่ตั้งฟาร์ม</label>
                                <textarea class="form-control" name="address" id="address"
                                    rows="3">{{ old('address', '') }}</textarea>
                                @error('address')
                                    <small class=" text-danger">{{ $message }} </small>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="district">ตำบล</label>
                                <input type="text" id="district" class=" form-control" name="district">
                            </div>
                            <div class="form-group">
                                <label for="amphoe">อำเภอ</label>
                                <input type="text" id="amphoe" class=" form-control" name="amphoe">
                            </div>
                            <div class="form-group">
                                <label for="province">จังหวัด</label>
                                <input type="text" id="province" class=" form-control" name="province">
                            </div>
                            <div class="form-group">
                                <label for="zipcode">รหัสไปรษณี</label>
                                <input type="text" id="zipcode" class=" form-control" name="zipcode">
                            </div>
                        </div>
                        @can('isAdmin')
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="user_id">เลือกเจ้าของฟาร์ม</label>
                                    <select class="form-control" name="user_id" id="user_id">
                                        @if ($users->count() > 0)
                                            @foreach ($users as $user)
                                                <option value="{{ $user->id }} ">{{ $user->name }} </option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @error('user_id')
                                        <small class=" text-danger">{{ $message }} </small>
                                    @enderror
                                </div>
                            </div>
                        @endcan
                        @can('isUser')
                            <input type="hidden" name="user_id" value="{{ auth()->user()->id }} ">
                        @endcan
                        <div class="col-12">
                            <div class="form-group">
                                <label for="enterpriseser_id">กลุ่มวิสาหกิจที่สังกัด </label>
                                <select class="form-control" name="enterpriseser_id" id="enterpriseser_id">
                                    @foreach ($etps as $etp)
                                        <option value="{{ $etp->id }} ">{{ $etp->name }} </option>
                                    @endforeach
                                </select>
                                @error('enterpriseser_id')
                                    <small class=" text-danger">{{ $message }} </small>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="">การใช้ประโยชน์ที่ดิน </label>
                                <input type="text" class="form-control" name="" value="{{ old('land_use', '') }}"
                                    id="land_use" placeholder="" />
                                @error('land_use')
                                    <small class=" text-danger">{{ $message }} </small>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="">การถือครอง </label>
                                <select class="form-control" name="canalization" id="canalization">
                                    <option>ภบท.</option>
                                    <option>สปก.</option>
                                    <option>โฉนด</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="">จำนวน </label>
                                <select class="form-control" name="quantity" id="quantity">
                                    <option>น้อยกว่า 5 ไร่</option>
                                    <option>6-10 ไร่</option>
                                    <option>11-15 ไร่</option>
                                    <option>16-20 ไร่</option>
                                    <option>มากกว่า 20 ไร่</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-12">
                            <input type="hidden" name="count" id="count" value="2">
                            <div class="form-group">
                                <label for="">รูปแบบการผลิต
                                </label>
                                <select class="form-control" name="model" id="model">
                                    <option>เลี้ยงแม่โคเพื่อผลิตลูกโค
                                    </option>
                                    <option>ขุนโคเพื่อจำหน่าย
                                    </option>
                                    <option>เลี้ยงแม่โค/ขุนโค
                                    </option>
                                    <option>ซื้อมาขายไป
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="">รูปแบบการเลี้ยง
                                </label>
                                <select class="form-control" name="parenting" id="parenting">
                                    <option>ปล่อยทุ่งหญ้าสาธารณะ
                                    </option>
                                    <option>ปล่อยทุ่งหญ้าตนเอง
                                    </option>
                                    <option>ขังในคอกรวม
                                    </option>
                                    <option>ขังในคอกเดี่ยว
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="">แม่พันธุ์ใช้
                                </label>
                                <select class="form-control" name="mom" id="mom">
                                    <option>พื้นเมือง
                                    </option>
                                    <option>บราห์มัน
                                    </option>
                                    <option>ลูกผสมบราห์มัน
                                    </option>
                                    <option>ลูกผสมชาโรเลส์
                                    </option>
                                    <option>ลูกผสมแองกัส
                                    </option>
                                    <option>แบรงกัส
                                    </option>
                                    <option>อื่นๆ
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="">วิธีการผสมพันธุ์

                                </label>
                                <select class="form-control" name="breed" id="breed">
                                    <option>พ่อพันธุ์คุมฝูง

                                    </option>
                                    <option>พ่อพันธุ์รับจ้าง

                                    </option>
                                    <option>ผสมเทียม
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="">แหล่งที่มาของน้ำเชื้อผสมเทียม

                                </label>
                                <select class="form-control" name="semen_source" id="semen_source">
                                    <option>กรมปศุสัตว์


                                    </option>
                                    <option>เอกชน

                                    </option>
                                    <option>ต่างประเทศ

                                    </option>
                                    <option>กลุ่ม/สหกรณ์

                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="">อายุแม่พันธุ์ที่ผสมครั้งแรก

                                </label>
                                <select class="form-control" name="first_mixed_mom_old" id="first_mixed_mom_old">
                                    <option>12-15 เดือน


                                    </option>
                                    <option>16-18 เดือน

                                    </option>
                                    <option>19-21 เดือน

                                    </option>
                                    <option>22-24 เดือน

                                    </option>
                                    <option>25-27 เดือน

                                    </option>
                                    <option>28-30 เดือน

                                    </option>
                                    <option>31-39 เดือน

                                    </option>
                                    <option>มากกว่า 36 เดือน

                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="">ระยะเวลาจากคลอดถึงผสมพันธุ์

                                </label>
                                <select class="form-control" name="time_from_birth_to_mating"
                                    id="time_from_birth_to_mating">
                                    <option>น้อยกว่า 2 เดือน


                                    </option>
                                    <option>3-4 เดือน

                                    </option>
                                    <option>5-6 เดือน

                                    </option>
                                    <option>7-8 เดือน

                                    </option>
                                    <option>มากกว่า 8 เดือน

                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="">โคขุนที่เลี้ยง

                                </label>
                                <select class="form-control" name="cattle" id="cattle">
                                    <option>ลูกผสมบราห์มัน


                                    </option>
                                    <option>ลูกผสมชาโรเลส์

                                    </option>
                                    <option>ลูกผสมแองกัส

                                    </option>
                                    <option>แบรงกัส

                                    </option>
                                    <option>ลูกผสมวากิว

                                    </option>
                                    <option>ลูกผสมบีฟมาสเตอร์

                                    </option>
                                    <option>อื่นๆ

                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="">อายุเริ่มขุน

                                </label>
                                <select class="form-control" name="old_start_cattle" id="old_start_cattle">
                                    <option>4-6 เดือน


                                    </option>
                                    <option>7-9 เดือน
                                    </option>
                                    <option>9-12 เดือน

                                    </option>
                                    <option>13-15 เดือน

                                    </option>
                                    <option>16-18 เดือน

                                    </option>
                                    <option>19-21 เดือน

                                    </option>
                                    <option>22-24 เดือน

                                    </option>
                                    <option>มากกว่า 24 เดือน

                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for=""> อายุสิ้นสุดการขุน


                                </label>
                                <select class="form-control" name="old_end_cattle" id="old_end_cattle">
                                    <option>น้อยกว่า 28 เดือน


                                    </option>
                                    <option>29-32 เดือน
                                    </option>
                                    <option>33-36 เดือน

                                    </option>
                                    <option>37-40 เดือน

                                    </option>
                                    <option>41-44 เดือน

                                    </option>
                                    <option>45-48 เดือน

                                    </option>
                                    <option>มากกว่า 48 เดือน

                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for=""> สถานที่ขายโคขุน


                                </label>
                                <select class="form-control" name="sale_location" id="sale_location">
                                    <option>พ่อค้าทั่วไป


                                    </option>
                                    <option>กลุ่มวิสาหกิจ
                                    </option>
                                    <option>สหกรณ์เครือข่ายโคเนื้อ จำกัด

                                    </option>
                                    <option>สหกรณ์การเกษตรหนองสูง จำกัด

                                    </option>
                                    <option>บริษัทนครพนมบีฟ จำกัด

                                    </option>
                                    <option>ขายผ่านสมาชิก

                                    </option>
                                    <option>อื่นๆ

                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for=""> สถานที่ขายโคขุน


                                </label>
                                <select class="form-control" name="sale_model" id="sale_model">
                                    <option>เหมาตัว

                                    </option>
                                    <option>ชั่งน้ำหนัก/ราคาตายตัว
                                    </option>
                                    <option>ชั่งน้ำหนัก/ราคาตามไขมันแทรก
                                    </option>
                                    <option>อื่นๆ

                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-end">
                        <div class="col-12 col-md-12">
                            <div class="form-group">
                                <label for="">แม่พันธุ์
                                </label>
                                <select class="form-control" name="food_type1" id="food_type1">
                                    <option>กินหญ้าอย่างเดียว
                                    </option>
                                    <option>กินหญ้า/เสริมอาหาร
                                    </option>
                                    <option>กินหญ้า/ฟาง/เสริมอาหารข้น
                                    </option>
                                    <option>อาหาร ที เอ็ม อาร์
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-11 col-md-12 ">
                            <div class="form-group">
                                <label for="">อาหารข้น
                                </label>
                                <select class="form-control" name="thick_food1" id="thick_food1">
                                    <option>ผสมเอง
                                    </option>
                                    <option>ซื้อของกลุ่ม/สหกรณ์
                                    </option>
                                    <option>ซื้อของบริษัท
                                    </option>
                                    <option>ซื้อร้านค้าทั่วไป
                                    </option>
                                    <option>อื่นๆ
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-10 col-md-5">
                            <div class="form-group">
                                <label for="">โปรตีน
                                </label>
                                <select class="form-control" name="protein1" id="protein1">
                                    <option>12%
                                    </option>
                                    <option>14%
                                    </option>
                                    <option>16%
                                    </option>
                                    <option>18%
                                    </option>
                                    <option>มากกว่า 18%
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-10 col-md-5">
                            <div class="form-group">
                                <label for="">ปริมาณ/วัน
                                </label>
                                <select class="form-control" name="volume1" id="volume1">
                                    <option>ไม่ได้ให้
                                    </option>
                                    <option>น้อยกว่า 1 ก.ก.
                                    </option>
                                    <option>1-2 ก.ก.
                                    </option>
                                    <option>3-4 ก.ก.
                                    </option>
                                    <option>มากกว่า 4 ก.ก.
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-11 col-md-12">
                            <div class="form-group">
                                <label for="">ที เอ็ม อาร์
                                </label>
                                <select class="form-control" name="thick_food2" id="thick_food2">
                                    <option>ผสมเอง
                                    </option>
                                    <option>ซื้อของกลุ่ม/สหกรณ์
                                    </option>
                                    <option>ซื้อของบริษัท
                                    </option>
                                    <option>ซื้อร้านค้าทั่วไป
                                    </option>
                                    <option>อื่นๆ
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-10 col-md-5">
                            <div class="form-group">
                                <label for="">โปรตีน
                                </label>
                                <select class="form-control" name="protein2" id="protein2">
                                    <option>12%
                                    </option>
                                    <option>14%
                                    </option>
                                    <option>16%
                                    </option>
                                    <option>18%
                                    </option>
                                    <option>มากกว่า 18%
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-10 col-md-5">
                            <div class="form-group">
                                <label for="">ปริมาณ/วัน
                                </label>
                                <select class="form-control" name="volume2" id="volume2">
                                    <option>ไม่ได้ให้
                                    </option>
                                    <option>น้อยกว่า 1 ก.ก.
                                    </option>
                                    <option>1-2 ก.ก.
                                    </option>
                                    <option>3-4 ก.ก.
                                    </option>
                                    <option>มากกว่า 4 ก.ก.
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="row justify-content-end">
                        <div class="col-12 col-md-12">
                            <div class="form-group">
                                <label for="">โคขุน
                                </label>
                                <select class="form-control" name="food_type2" id="food_type2">
                                    <option>กินหญ้าอย่างเดียว
                                    </option>
                                    <option>กินหญ้า/เสริมอาหาร
                                    </option>
                                    <option>กินหญ้า/ฟาง/เสริมอาหารข้น
                                    </option>
                                    <option>อาหาร ที เอ็ม อาร์
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-11 col-md-12 ">
                            <div class="form-group">
                                <label for="">อาหารข้น
                                </label>
                                <select class="form-control" name="thick_food3" id="thick_food3">
                                    <option>ผสมเอง
                                    </option>
                                    <option>ซื้อของกลุ่ม/สหกรณ์
                                    </option>
                                    <option>ซื้อของบริษัท
                                    </option>
                                    <option>ซื้อร้านค้าทั่วไป
                                    </option>
                                    <option>อื่นๆ
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-10 col-md-5">
                            <div class="form-group">
                                <label for="">โปรตีน
                                </label>
                                <select class="form-control" name="protein3" id="protein3">
                                    <option>12%
                                    </option>
                                    <option>14%
                                    </option>
                                    <option>16%
                                    </option>
                                    <option>18%
                                    </option>
                                    <option>มากกว่า 18%
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-10 col-md-5">
                            <div class="form-group">
                                <label for="">ปริมาณ/วัน
                                </label>
                                <select class="form-control" name="volume3" id="volume3">
                                    <option>ไม่ได้ให้
                                    </option>
                                    <option>น้อยกว่า 1 ก.ก.
                                    </option>
                                    <option>1-2 ก.ก.
                                    </option>
                                    <option>3-4 ก.ก.
                                    </option>
                                    <option>มากกว่า 4 ก.ก.
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-11 col-md-12">
                            <div class="form-group">
                                <label for="">ที เอ็ม อาร์
                                </label>
                                <select class="form-control" name="thick_food4" id="thick_food4">
                                    <option>ผสมเอง
                                    </option>
                                    <option>ซื้อของกลุ่ม/สหกรณ์
                                    </option>
                                    <option>ซื้อของบริษัท
                                    </option>
                                    <option>ซื้อร้านค้าทั่วไป
                                    </option>
                                    <option>อื่นๆ
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-10 col-md-5">
                            <div class="form-group">
                                <label for="">โปรตีน
                                </label>
                                <select class="form-control" name="protein4" id="protein4">
                                    <option>12%
                                    </option>
                                    <option>14%
                                    </option>
                                    <option>16%
                                    </option>
                                    <option>18%
                                    </option>
                                    <option>มากกว่า 18%
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-10 col-md-5">
                            <div class="form-group">
                                <label for="">ปริมาณ/วัน
                                </label>
                                <select class="form-control" name="volume4" id="volume4">
                                    <option>ไม่ได้ให้
                                    </option>
                                    <option>น้อยกว่า 1 ก.ก.
                                    </option>
                                    <option>1-2 ก.ก.
                                    </option>
                                    <option>3-4 ก.ก.
                                    </option>
                                    <option>มากกว่า 4 ก.ก.
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="row justify-content-end">
                        <div class="col-12 col-md-12">
                            <div class="form-group">
                                <label for="">พืชอาหารสัตว์
                                </label>
                                <select class="form-control" name="source" id="source">
                                    <option>ปลูกเอง
                                    </option>
                                    <option>ซื้อจากกลุ่ม/สหกรณ์
                                    </option>
                                    <option>ซื้อทั่วไป
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-10 col-md-5">
                            <div class="form-group">
                                <label for="">พื้นที่แปลงพืชอาหารสัตว์
                                </label>
                                <select class="form-control" name="area" id="area">
                                    <option>น้อยกว่า 5 ไร่
                                    </option>
                                    <option>6-10 ไร่
                                    </option>
                                    <option>11-15 ไร่
                                    </option>
                                    <option>16-20 ไร่
                                    </option>
                                    <option>มากกว่า 20 ไร่
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-10 col-md-5">
                            <div class="form-group">
                                <label for="">พันธุ์พืชอาหารสัตว์ที่ใช้
                                </label>
                                <select class="form-control" name="seed" id="seed">
                                    <option>เนเปียร์ปากช่อง 1
                                    </option>
                                    <option>รูซี่
                                    </option>
                                    <option>กินนี
                                    </option>
                                    <option>อื่นๆ
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-10 col-md-5">
                            <div class="form-group">
                                <label for="">การเก็บเกี่ยว
                                </label>
                                <select class="form-control" name="keep" id="keep">
                                    <option>มีด/เคียว
                                    </option>
                                    <option>เครื่องร่อน
                                    </option>
                                    <option>เครื่องตัดหญ้า

                                    </option>
                                    <option>อื่นๆ
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-10 col-md-5">
                            <div class="form-group">
                                <label for="">การถนมอาหาร
                                </label>
                                <select class="form-control" name="cheris">
                                    <option>ไม่ได้ทำ
                                    </option>
                                    <option>ตากแห้ง
                                    </option>
                                    <option>หมัก
                                    </option>
                                    <option>อื่นๆ
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-4">
                        <button type="submit" class="btn btn-info">บันทึก</button>
                        <!-- <a href="to.php" class="btn btn-info">ถัดไป</a> -->
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</x-adminlte-layout>
