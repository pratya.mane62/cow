<x-adminlte-layout>
    <x-slot name="title">
        {{ __('RFIDTAG') }}
    </x-slot>
    <div class="card">
        <div class="card-body">
            <h4><b>จัดการ RFID Tag</b></h4>
            <div class="row card-text">
                <form method="post" action="{{ route('admin.tag.create') }}" class="col-12 col-md-10">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-1 text-right">Tag ID: </div>
                        <div class="col-12 col-md-9">
                            <input type="text" class="form-control" id="tag_id" name="tag_id" value=""
                                placeholder="Tag ID" require autofocus />
                        </div>
                        <div class="col-12 col-md-2">
                            <button type="submit" class="btn btn-success form-control">Add</button>
                        </div>
                    </div>
                </form>
                <form action="{{ route('admin.tag.clear') }}" method="post" class="col-12 col-md-2">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-warning btn-block ">Clear</button>
                </form>
                <form method="post" action="{{ route('admin.rfid.store') }}" class="col-12 col-md-10">
                    @csrf
                    <div class="row my-3">
                        <div class="col-12 col-md-1 text-right">Farm: </div>
                        <div class="col-12 col-md-9">
                            <select class="form-control" id="farm_id" name="farm_id">
                                @foreach ($farms as $farm)
                                    <option value="{{ $farm->id }}">{{ $farm->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12 col-md-2">
                            <button type="submit" class="btn btn-primary form-control">Save</button>
                        </div>
                    </div>
                </form>
                <div class="col-12">
                    <table class=" table_id " style="width:100%;">
                        <thead>
                            <tr align="center">
                                <th style="width: 80px;">Number</th>
                                <th>Tag ID</th>
                                <th align="center" style="width: 50px;">
                                    <i class="fas fa-trash-alt"></i>
                                </th>
                            </tr>
                        </thead>
                        {{-- {{ dd($tags) }} --}}
                        @isset($tags)
                            <tbody>
                                @foreach ($tags as $i => $t)
                                    <tr align="center">
                                        <td align="center">{{ $i += 1 }}</td>
                                        <td>{{ $t }} </td>
                                        <td align="center">
                                            <form action="{{ route('admin.tag.remove', ['tag' => $t]) }}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button class=" btn btn-danger btn-sm"> <i
                                                        class="fas fa-trash-alt"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        @endisset
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-adminlte-layout>
