<x-adminlte-layout>
    <x-slot name="title">
        {{ __('Specie') }}
    </x-slot>
    <div class="row">
        <div class="col-12 col-md-4">
            <div class="card shadow">
                <div class="card-bady">
                    <div class="card-header">
                        <h4>{{ __('AddData') }}{{ __('Specie') }}</h4>
                    </div>
                    <form action="{{ route('admin.species.store') }}" method="post" class=" card-body">
                        @csrf
                        <p class="card-text">
                        <div class="form-group">
                            <label for="title">{{ __('Specie') }}</label>
                            <input type="text" name="title" value="{{ old('') }}" id="title" class="form-control">
                        </div>
                        </p>
                        <button class=" btn btn-success" type="submit">{{ __('AddData') }} </button>
                    </form>
                </div>
            </div>
        </div>
        <div class="cal-12 col-md-8">
            <div class="card shadow">
                <div class="card-header">
                    <h4>{{ __('Specie') }}</h4>
                </div>
                <div class="card-body">
                    <table class=" table_id " style="width:100%;" >
                        <thead>
                            <tr>
                                <th  >#</th>
                                <th  >{{ __('Specie') }}</th>
                                <th  >Actions</th>
                            </tr>
                        </thead>
                        @if ($species->count() > 0)

                            <tbody>
                                @foreach ($species as $i => $specie)
                                    <tr>
                                        <td align="center">{{ $i += 1 }}</td>
                                        <td>{{ $specie->title }} </td>
                                        <td   width="100">
                                            <form
                                                action="{{ route('admin.species.destroy', ['species' => $specie]) }} "
                                                method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class=" btn btn-danger btn-sm"
                                                    onclick="return confirm('Are you sure?')">{{ __('Delete') }}</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-adminlte-layout>
