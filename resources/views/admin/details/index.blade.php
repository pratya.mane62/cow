<x-adminlte-layout>
    <x-slot name="title">
        {{ __('AdminFarm') }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">
            <div class="float-left">
                <h4>{{ __('AdminFarm') }}</h4>
            </div>
            <a href=" {{ route('admin.details.create') }} " class=" btn btn-info float-right">
                {{ __('AddData') }}
            </a>
        </div>
        <div class="card-body">
            <div class="card-content ">
                <table class=" table_id " style="width:100%;">
                    <thead>
                        <tr>
                            <th  >#</th>
                            <th  >ชื่อ</th>
                            <th  >เพศ</th>
                            <th  >อายุ</th>
                            <th  >การศึกษา</th>
                            <th  >อาชีพ</th>
                            <th  >รายได้หลัก / เดือน</th>
                            <th  >ที่อยู่</th>
                            <th  >action</th>
                        </tr>
                    </thead>
                    @if ($admins->count() > 0)

                        <tbody>
                            @foreach ($admins as $i => $admin)
                                <tr>
                                    <td  >{{ $i += 1 }}
                                    </td>
                                    <td  >{{ $admin->name }} </td>
                                    <td  >{{ $admin->sex == 'male' ? 'ชาย' : 'หญิง' }} </td>
                                    <td  >{{ $admin->old }} </td>
                                    <td  >{{ $admin->study }} </td>
                                    <td  >{{ $admin->profession }} </td>
                                    <td  >{{ $admin->earning }} </td>
                                    <td  >{{ $admin->address }} </td>
                                    <td   width="100" >
                                        <a href="{{ route('admin.details.edit', ['detail' => $admin]) }} "
                                            class=" btn btn-warning btn-sm mb-1" style="width: 80px">{{ __('Edit') }}</a>
                                        <div class="mx-1"></div>
                                        <form
                                            action="{{ route('admin.details.destroy', ['detail' => $admin]) }} "
                                            method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class=" btn btn-danger btn-sm" style="width: 80px"
                                                onclick="return confirm('Are you sure?')">{{ __('Delete') }}</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</x-adminlte-layout>
