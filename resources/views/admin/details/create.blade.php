<x-adminlte-layout>
    <x-slot name="title">
        {{ __('AdminFarm') }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">

            <h4> {{ __('AdminFarm') }}</h4>
        </div>
        <div class="card-body">
            <div class="card-content">
                <x-auth-validation-errors class="mb-4" :errors="$errors" />

                <form action="{{ route('admin.details.store') }} " method="post" id="myForm" class=" row">
                    @csrf
                    <div class="form-group col-12 ">
                        <div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="status" id="status1" value="farm"
                                    checked>
                                <label class="form-check-label" for="status1">
                                    เจ้าของฟาร์ม
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="status" id="status2" value="slaugh">
                                <label class="form-check-label" for="status2">
                                    เจ้าของโรงเชือด
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="etp" id="isEtp">
                                <label class="form-check-label" for="etp">
                                    ผู้ดูแลกลุ่มวิสาหกิจ
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6 col-lg-4 col-xl-3 " id="showEtp">
                        <label for="etp_id">กลุ่มวิสาหกิจ</label>
                        <select multiple="multiple" class="js-example-basic-multiple form-control" id="multi_user"
                            name="etp_id[]">
                            @foreach ($etps as $etp)
                                <option value="{{ $etp->id }}">{{ $etp->name }}</option>
                            @endforeach
                        </select>
                        @error('etp_id')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group col-12 col-md-6 col-lg-4 col-xl-3">
                        <label for="">ชื่อ</label>
                        <input type="text" value="{{ old('name', '') }}" name="name" id="name" class="form-control"
                            placeholder="" />
                        @error('name')
                            <small class="text-danger">{{ $message }} </small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-md-6 col-lg-4 col-xl-3">
                        <label for="">เลขประจำตัวประชาชน</label>
                        <input type="text" value="{{ old('card_id', '') }}" name="card_id" id="id_cards"
                            class="form-control" placeholder="" />
                        @error('card_id')
                            <small class="text-danger">{{ $message }} </small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-md-6 col-lg-4 col-xl-3">
                        <label for="old">อายุ</label>
                        <select class="form-control" name="old" id="old">
                            <option>ต่ำกว่า 20 ปี</option>
                            <option>21 - 31 ปี</option>
                            <option>31 - 40 ปี</option>
                            <option>41 - 50 ปี</option>
                            <option>51 - 60 ปี</option>
                            <option>มากกว่า 60 ปี</option>
                        </select>
                        @error('old')
                            <small class="text-danger">{{ $message }} </small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-md-6 col-lg-4 col-xl-3">
                        <label for="study">การศึกษา</label>
                        <select class="form-control" name="study" id="study">
                            <option>ประถมศึกษา หรือต่ำกว่า</option>
                            <option>มัธยมศึกษา</option>
                            <option>ปวช.</option>
                            <option>ปวส.</option>
                            <option>ปริญญาตรี หรือสูงกว่า</option>
                        </select>
                        @error('study')
                            <small class="text-danger">{{ $message }} </small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-md-6 col-lg-4 col-xl-3">
                        <label for="profession">อาชีหลัก</label>
                        <select class="form-control" name="profession" id="profession">
                            <option>รับราชการ</option>
                            <option>รัฐวิสาหกิจ</option>
                            <option>บริษัท</option>
                            <option>เลี้ยวสัตว์</option>
                            <option>ทำนา หรือทำไร่</option>
                            <option>ธุรกิจส่วนตัว</option>
                            <option>รับจ้าง</option>
                            <option>อื่น ๆ</option>
                        </select>
                        @error('profession')
                            <small class="text-danger">{{ $message }} </small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-md-6 col-lg-4 col-xl-3">
                        <label for="earning">รายได้หลัก / เดือน</label>
                        <select class="form-control" name="earning" id="earning">
                            <option>น้อยกว่า 10,000 บาท</option>
                            <option>10,001 - 15,000</option>
                            <option>15,001 - 20,000</option>
                            <option>20,001 - 25,000</option>
                            <option>25,001 - 30,000</option>
                            <option>มากกว่า 30,000</option>
                        </select>
                        @error('earning')
                            <small class="text-danger">{{ $message }} </small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-md-6 col-lg-4 col-xl-3">
                        <label for="sex">เพศ</label>
                        <div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="sex" id="sex1" value="male" checked>
                                <label class="form-check-label" for="sex1">
                                    ชาย
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="sex" id="sex2" value="female">
                                <label class="form-check-label" for="sex2">
                                    หญิง
                                </label>
                            </div>
                        </div>
                        @error('sex')
                            <small class="text-danger">{{ $message }} </small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-md-6 col-lg-4 col-xl-3">
                        <label for="phone">หมายเลขโทรศัพท์</label>
                        <input type="text" name="phone" id="phone" class="form-control">
                        @error('phone')
                            <small class="text-danger">{{ $message }} </small>
                        @enderror
                    </div>

                    <div class="form-group col-12 ">
                        <label for="address">ที่อยู่</label>
                        <textarea class="form-control" name="address" id="address" rows="3"></textarea>
                        @error('address')
                            <small class="text-danger">{{ $message }} </small>
                        @enderror
                    </div>

                    <div class="form-group col-12 ">
                        <button type="submit" class="btn btn-success"> {{ __('AddData') }} </button>
                    </div>

                </form>
            </div>
        </div>
    </div>

    @section('scripts')
        <script>
            if (!$('#isEtp').is(":checked")) {
                $('#showEtp').addClass('d-none')
            } else {
                $('#showEtp').removeClass('d-none')
            }
            $('#isEtp').change(() => {
                if (!$('#isEtp').is(":checked")) {
                    $('#showEtp').addClass('d-none')
                } else {
                    $('#showEtp').removeClass('d-none')
                }
            })

        </script>
    @endsection
</x-adminlte-layout>
