<x-adminlte-layout>
    @section('styles')
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
            integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    @endsection
    <x-slot name="title">
        {{ __('ประวัติสุขภาพ') }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">
            ประวัติสุขภาพของ {{ $cow->name }}
        </div>
        <div class="card-body">
            <div class="card-content">
                <x-auth-validation-errors class="mb-4" :errors="$errors" />
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link @if ($health=='dewormings' ) active @endif" href="{{ route('admin.cow.health', [$cow, 'health' => 'dewormings']) }}"
                            role="tab">การถ่ายพยาธิ</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link @if ($health=='vaccinations' ) active @endif"
                            href="{{ route('admin.cow.health', [$cow, 'health' => 'vaccinations']) }}"
                            role="tab">การฉีดวัคซีน</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link @if ($health=='examinations' ) active @endif"
                            href="{{ route('admin.cow.health', [$cow, 'health' => 'examinations']) }}"
                            role="tab">ตรวจโรค</a>
                    </li>
                </ul>
                @if ($health == 'dewormings')
                    {{-- ถ่ายพยาธิ --}}
                    <div class="row">
                        <div class="col-12">
                            <form action="{{ route('admin.cow.health.store', $cow) }}" method="post">
                                @csrf
                                <div class="input-group mb-3">
                                    <input type="date" class="form-control" name="deworming" placeholder="วันที่">
                                    <div class="input-group-append">
                                        <button class=" btn btn-success" type="submit">เพิ่ม</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <div class="col-12">
                            <ul>
                                @foreach ($dewormings as $deworming)
                                    <li><span class=" d-flex justify-content-between"><b>ถ่ายพยาธิวันที่</b>
                                            {{ $deworming->deworming }}
                                            <b>ครั้งต่อไปวันที่</b>
                                            {{ \Carbon\Carbon::parse($deworming->deworming)->addMonths(4)->format('Y-m-d') }}
                                            <form action="{{ route('admin.deworming.destroy', $deworming) }} "
                                                method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-sm btn-danger mb-1"
                                                    onclick="return confirm('Are you sure?')">{{ __('Delete') }}</button>
                                            </form>
                                        </span> </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
                @if ($health == 'vaccinations')
                    {{-- วัคซีน --}}
                    <div class="row">
                        <div class="col-12">
                            <form action="{{ route('admin.cow.health.store', $cow) }}" method="post">
                                @csrf
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" name="vaccination" placeholder="ชื่อวัคซีน">
                                    <input type="date" class="form-control" name="vaccination_date"
                                        placeholder="วันที่ฉีด">
                                    <div class="input-group-append">
                                        <button class=" btn btn-success" type="submit">เพิ่ม</button>
                                    </div>
                                </div>


                            </form>
                        </div>
                        <div class="col-12">
                            <ul>
                                @foreach ($vaccinations as $vaccination)
                                    <li><span
                                            class=" d-flex justify-content-between"><b>วันที่</b>{{ $vaccination->vaccination_date }}
                                            <b>ฉีดวัคซีน</b>
                                            {{ $vaccination->vaccination }}
                                            <form action="{{ route('admin.vaccination.destroy', $vaccination) }} "
                                                method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-sm btn-danger mb-1"
                                                    onclick="return confirm('Are you sure?')">{{ __('Delete') }}</button>
                                            </form>
                                        </span> </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
                @if ($health == 'examinations')
                    {{-- ตรวจโรค --}}
                    <div class="row">
                        <div class="col-12">
                            <form action="{{ route('admin.cow.health.store', $cow) }}" method="post">
                                @csrf
                                <div class="input-group mb-3">
                                    <input type="date" class="form-control" name="examination_date"
                                        placeholder="วันที่ฉีด">
                                    <input type="text" class="form-control" name="examination" placeholder="ผลการตรวจ">
                                    <div class="input-group-append">
                                        <button class=" btn btn-success" type="submit">เพิ่ม</button>
                                    </div>
                                </div>


                            </form>
                        </div>
                        <div class="col-12">
                            <ul>
                                @foreach ($examinations as $examination)
                                    <li><span class=" d-flex justify-content-between"><span><b>วันที่</b>
                                                {{ $examination->examination_date }}</span>
                                            <form action="{{ route('admin.examination.destroy', $examination) }} "
                                                method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-sm btn-danger mb-1"
                                                    onclick="return confirm('Are you sure?')">{{ __('Delete') }}</button>
                                            </form>
                                    </li>
                                    <li class=" d-flex flex-wrap">
                                        {{ $examination->examination }}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    @section('scripts')
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
            integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js"
            integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous">
        </script>
    @endsection
</x-adminlte-layout>
