<x-adminlte-layout>
    <x-slot name="title">
        {{ __('Cow') }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">
            <h4> {{ __('Cow') }}</h4>
        </div>
        <div class="card-body">
            @php
                // dd($cow->farm->id);
                $c = App\Models\TransectionCow::where('cow_id', $cow->id)
                    ->where('farm_id', $cow->farm->id)
                    ->orderBy('created_at', 'desc')
                    ->first();
            @endphp
            <x-auth-validation-errors class="mb-4" :errors="$errors" />
            <div class="card-content row">
                <div class="col-12 col-xl-6 mb-2">
                    <img id="blah" src="{{ asset('storage/uploads/' . $c->photo) }}" alt="{{ $cow->name }}"
                        class=" img-thumbnail img-fluid w-100 " />
                </div>
                <div class="col-12 col-xl-6 mb-2">
                    <img id="blahrfid" src="{{ asset('storage/uploads/' . $c->photorfid) }}" alt="{{ $cow->name }}"
                        class=" img-thumbnail img-fluid w-100 " />
                </div>
                <form action=" {{ route('admin.cows.update', $cow) }}" method="post" id="myForm"
                    enctype="multipart/form-data" class=" col-12 col-xl-12 row justify-content-end">
                    @csrf
                    @method('put')

                    <input type="hidden" name="farm_id" id="farm_id" value="{{ $cow->farm_id ?? 0 }}">
                    <div class="form-groupcol-12 col-md-6 col-xl-4 ">
                        <label for="photo"> รูป </label>
                        <input type="file" name="photo" value="{{ old('photo', '') }}" class="form-control"
                            id="imgInp">
                        @error('photo')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-groupcol-12 col-md-6 col-xl-4 ">
                        <label for="photorfid"> รูปถ่ายกับ RFIDTAG </label>
                        <input type="file" name="photorfid" value="{{ old('photorfid', '') }}" class="form-control"
                            id="imgInprfid">
                        @error('photorfid')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group col-12 col-md-6 col-xl-4 ">
                        <label for="rfid">RFID</label>
                        <input type="text" name="rfid" value="{{ old('rfid', $cow->rfid) }}" id="rfid"
                            class="form-control">
                        @error('rfid')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-md-6 col-xl-4 ">
                        <label for="name">ชื่อ</label>
                        <input type="text" name="name" value="{{ old('name', $cow->name) }}" id="name"
                            class="form-control">
                        @error('name')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-md-6 col-xl-4 ">
                        <label for="weight">น้ำหนัก Kg.</label>
                        <input type="number" step="0.01" name="weight" value="{{ old('weight', $c->weight) }}"
                            class="form-control">
                        @error('weight')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>


                    <div class="form-group col-12 col-md-6 col-xl-4 ">
                        <label for="type">ประเภท</label>
                        <select class="form-control" name="type" id="type">
                            <option value="mom" @if ($cow->type == 'mom' || old('type') == 'mom') selected="selected" @endif>แม่พันธ์ุ</option>
                            <option value="dad" @if ($cow->type == 'dad' || old('type') == 'dad') selected="selected" @endif>พ่อพันธ์ุ</option>
                            <option value="cattle" @if ($cow->type == 'cattle' || old('type') == 'cattle') selected="selected" @endif>โคขุน</option>
                        </select>
                    </div>

                    <div class="form-group col-12 col-md-6 col-xl-4 ">
                        <label for="birth">วันเกิด(ถ้ามี)</label>
                        <input type="date" name="birth" value="{{ old('birth', $cow->birth) }}" id="birth"
                            class="form-control">
                        @error('birth')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    {{-- @can('isAdmin')
                        <div class="form-groupcol-12 col-md-6 col-xl-4 ">
                            <label for="farm_id">เลือกฟาร์ม</label>
                            <select class="form-control" name="farm_id" id="farm_id">
                                @if ($farms->count() > 0)
                                    @foreach ($farms as $farm)
                                        <option value="{{ $farm->id }}" @if (old('farm_id') == $farm->id || $cow->farm_id == $farm->id) selected="selected" @endif>
                                            {{ $farm->title }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    @endcan --}}

                    <div class="form-group col-12 col-md-6 col-xl-4 ">
                        <label for="sex">เพศ</label>
                        <div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="sex" value="male" @if ($cow->sex == 'male') checked="checked" @endif>
                                <label class="form-check-label">ผู้</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="sex" value="female" @if ($cow->sex == 'female') checked="checked" @endif>
                                <label class="form-check-label">เมีย</label>
                            </div>
                        </div>
                    </div>
                    <div class=" col-12 col-md-6 col-xl-4"></div>
{{--
                    <div class="col-xl-12">
                        <hr>
                        <button class="btn btn-sm btn-warning mx-1 mb-1" onclick="addDeworming()"
                            type="button">เพิ่ม</button>
                        <button class="btn btn-sm btn-danger mx-1 mb-1" type="button"
                            onclick="removeDeworming()">ลบ</button>
                    </div>
                    <div class=" col-xl-12 row" id="dewormings">

                    </div>

                    <div class="col-xl-12">
                        <hr>
                        <button class="btn btn-sm btn-warning mx-1 mb-1" onclick="addVaccination()"
                            type="button">เพิ่ม</button>
                        <button class="btn btn-sm btn-danger mx-1 mb-1" type="button"
                            onclick="removeVaccination()">ลบ</button>
                    </div>

                    <div class="col-xl-12 row" id="vaccinations">


                    </div>

                    <div class="col-xl-12">
                        <hr>
                        <button class="btn btn-sm btn-warning mx-1 mb-1" onclick="addExamination()"
                            type="button">เพิ่ม</button>
                        <button class="btn btn-sm btn-danger mx-1 mb-1" type="button"
                            onclick="removeExamination()">ลบ</button>
                    </div>
                    <div class="col-xl-12 row" id="examinations">

                    </div>

                    <div class="col-xl-12">
                        <hr>
                    </div> --}}

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 "></div>
                    <div class="form-group col-12  col-md-12 ">
                        <label for="specie">สายพันธุ์</label>
                        <select class="form-control" name="specie_id" id="specie_id">
                            @if ($secies->count() > 0)
                                @foreach ($secies as $sepie)
                                    <option value="{{ $sepie->id }} " @if ($cow->specie_id == $sepie->id || old('specie_id') == $sepie->id) selected="selected" @endif>
                                        {{ $sepie->title }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group col-12 col-md-12">
                        <select name="pedigree_level" id="pedigree_level">
                            <option value="0" @if($cow->pedigree_level == 0 ) selected @endif>ระดับสายเลือด</option>
                            <option value="1" @if($cow->pedigree_level == 1 ) selected @endif>F1</option>
                            <option value="2" @if($cow->pedigree_level == 2 ) selected @endif>F2</option>
                            <option value="3" @if($cow->pedigree_level == 3 ) selected @endif>F3</option>
                            <option value="4" @if($cow->pedigree_level == 4 ) selected @endif>F4</option>
                            <option value="5" @if($cow->pedigree_level == 5 ) selected @endif>F5</option>
                            <option value="6" @if($cow->pedigree_level == 6 ) selected @endif>F6</option>
                            <option value="7" @if($cow->pedigree_level == 7 ) selected @endif>F7</option>
                            <option value="8" @if($cow->pedigree_level == 8 ) selected @endif>F8</option>
                            <option value="9" @if($cow->pedigree_level == 9 ) selected @endif>F9</option>
                            <option value="10" @if($cow->pedigree_level == 10 ) selected @endif>F10</option>
                        </select>
                    </div>
                    <div class="col-12 row p-0 m-0">

                        <div class="form-group col-12 col-md-6 ">
                            <label for="F5name_dad">ชื่อพ่อพันธุ์</label>
                            <input type="text" value="{{ old('F5name_dad', $cow->F5name_dad) }}" name="F5name_dad"
                                id="F5name_dad" class=" form-control">
                            <label for="F5specie">สายพันธุ์</label>
                            <select class="form-control" name="F5specie_dad" id="F5specie_dad">
                                <option value="">เลือก</option>
                                @if ($secies->count() > 0)
                                    @foreach ($secies as $sepie)
                                        <option value="{{ $sepie->id }} " @if (old('F5specie_dad') == $sepie->id || $cow->F5specie_dad == $sepie->id) selected="selected" @endif>
                                            {{ $sepie->title }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="form-group pt-3">
                                <label for="f5blood_dad">ปริมาณเลือด ( <span
                                        class="f5blood_dad_val font-weight-bold "></span> %)</label>
                                <div class="d-flex w-100">
                                    <input id="f5blood_dad" class="form-control" type="number" step="any" min="0" max="100"
                                        value="{{ $cow->f5blood_dad ?? '' }}" name="f5blood_dad"
                                        style="width: 100%" />

                                </div>
                            </div>
                        </div>

                        <div class="form-group col-12 col-md-6 ">
                            <label for="F5name_mom">ชื่อแม่พันธุ์</label>
                            <input type="text" value="{{ old('F5name_mom', $cow->F5name_mom) }}" name="F5name_mom"
                                id="F5name_mom" class=" form-control">
                            <label for="specie">สายพันธุ์</label>
                            <select class="form-control" name="F5specie_mom" id="F5specie_mom">
                                <option value="">เลือก</option>
                                @if ($secies->count() > 0)
                                    @foreach ($secies as $sepie)
                                        <option value="{{ $sepie->id }} " @if (old('F5specie_mom') == $sepie->id || $cow->F5specie_mom == $sepie->id) selected="selected" @endif>
                                            {{ $sepie->title }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="form-group pt-3">
                                <label for="f5blood_mom">ปริมาณเลือด ( <span
                                        class="f5blood_mom_val font-weight-bold "></span> %)</label>
                                <div class="d-flex w-100">
                                    <input id="f5blood_mom" class="form-control" type="number" step="any" min="0" max="100"
                                        value="{{ $cow->f5blood_mom ?? '' }}" name="f5blood_mom"
                                        style="width: 100%" />
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="form-group mt-4 col-12">
                        <button class="btn btn-success float-right" id="addCowBtn"
                            type="submit">{{ __('EditData') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @section('scripts')
        <script>
            let dewormingsCount = 0
            let vaccinationsCount = 0
            let examinationsCount = 0
            @if ($dewormings->count() > 0)
                @foreach ($dewormings as $deworming)
                    dewormingsCount++
                    $('#dewormings').append(`
                    <div class="form-group  col-12 col-md-6 col-xl-4 " id="dewormings${dewormingsCount}">
                        <b>ประวัติการถ่ายพยาธิ</b>
                        <input type="date" name="dewormings[]" class="form-control" value="{{$deworming->deworming}}">
                    </div>`)
                @endforeach
            @else
                $('#dewormings').append(`
                <div class="form-group  col-12 col-md-6 col-xl-4 ">
                    <b>ประวัติการถ่ายพยาธิ ( วันที่ )</b>
                    <input type="date" name="dewormings[]" class="form-control">
                </div>`)
            @endif

            @if ($examinations->count() > 0)
                @foreach ($examinations as $examination)
                    examinationsCount++
                    $('#examinations').append(`
                    <div class="form-group col-12 col-md-6  " id="examinations_date${examinationsCount}">
                        <label for="examination_date[]">ประวัติการตรวจโรค ( วันที่ )</label>
                        <input type="date" name="examination_date[]" class="form-control" value="{{$examination->examination_date}}">
                        @error('examination_date[]')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-md-6 " id="examinations${examinationsCount}">
                        <label for="examination[]">ผลการตรวจโรค</label>
                        <input type="text" name="examination[]" class="form-control" value="{{$examination->examination}}">
                        @error('examination[]')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>`)
                @endforeach
            @else
                $('#examinations').append(`
                <div class="form-group col-12 col-md-6  ">
                    <label for="examination_date[]">ประวัติการตรวจโรค ( วันที่ )</label>
                    <input type="date" name="examination_date[]" class="form-control">
                    @error('examination_date[]')
                        <small class=" text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group col-12 col-md-6 ">
                    <label for="examination[]">ผลการตรวจโรค</label>
                    <input type="text" name="examination[]" class="form-control">
                    @error('examination[]')
                        <small class=" text-danger">{{ $message }}</small>
                    @enderror
                </div>`)
            @endif

            @if ($vaccinations->count() > 0)
                @foreach ($vaccinations as $vaccination)
                    vaccinationsCount++
                    $('#vaccinations').append(`
                    <div class="form-group  col-12 col-md-6 " id="vaccinations_date${vaccinationsCount}">
                        <label for="vaccination_date[]">ประวัติการฉีดวัคซีน ( วันที่ )</label>
                        <input type="date" name="vaccination_date[]" class="form-control" value="{{$vaccination->vaccination_date}}">
                        @error('vaccination_date[]')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group  col-12 col-md-6 " id="vaccinations${vaccinationsCount}">
                        <label for="vaccination[]">ชื่อวัคซีน</label>
                        <input type="text" name="vaccination[]" class="form-control" value="{{$vaccination->vaccination}}">
                        @error('vaccination[]')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>`)
                @endforeach
            @else
                $('#vaccinations').append(`
                <div class="form-group  col-12 col-md-6 ">
                    <label for="vaccination_date[]">ประวัติการฉีดวัคซีน ( วันที่ )</label>
                    <input type="date" name="vaccination_date[]" class="form-control">
                    @error('vaccination_date[]')
                        <small class=" text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group  col-12 col-md-6 ">
                    <label for="vaccination[]">ชื่อวัคซีน</label>
                    <input type="text" name="vaccination[]" class="form-control">
                    @error('vaccination[]')
                        <small class=" text-danger">{{ $message }}</small>
                    @enderror
                </div>`)
            @endif


            function addDeworming() {
                dewormingsCount++
                $('#dewormings').append(`
                                                                                                <div class="form-group  col-12 col-md-6 col-xl-4 " id="dewormings${dewormingsCount}" >
                                                                                                    <b>ประวัติการถ่ายพยาธิ</b>
                                                                                                    <input type="date" name="dewormings[]"class="form-control">
                                                                                                </div>`)
            }

            function removeDeworming() {
                $(`#dewormings${dewormingsCount}`).remove()
                dewormingsCount--
            }


            function addVaccination() {
                vaccinationsCount++
                $('#vaccinations').append(`
                                                                                                        <div class="form-group  col-12 col-md-6 " id="vaccinations_date${vaccinationsCount}">
                                                                                                            <label for="vaccination_date[]">ประวัติการฉีดวัคซีน ( วันที่ )</label>
                                                                                                            <input type="date" name="vaccination_date[]" class="form-control">
                                                                                                            @error('vaccination_date[]')
                                                                                                                <small class=" text-danger">{{ $message }}</small>
                                                                                                            @enderror
                                                                                                        </div>

                                                                                                        <div class="form-group  col-12 col-md-6 " id="vaccinations${vaccinationsCount}">
                                                                                                            <label for="vaccination[]">ชื่อวัคซีน</label>
                                                                                                            <input type="text" name="vaccination[]" class="form-control">
                                                                                                            @error('vaccination[]')
                                                                                                                <small class=" text-danger">{{ $message }}</small>
                                                                                                            @enderror
                                                                                                        </div>
                                                                                                   `)
            }

            function removeVaccination() {
                $(`#vaccinations_date${vaccinationsCount}`).remove()
                $(`#vaccinations${vaccinationsCount}`).remove()
                vaccinationsCount--
            }



            function addExamination() {
                examinationsCount++
                $('#examinations').append(`
                                                                                <div class="form-group col-12 col-md-6  " id="examinations_date${examinationsCount}">
                                                                                    <label for="examination_date[]">ประวัติการตรวจโรค ( วันที่ )</label>
                                                                                    <input type="date" name="examination_date[]" class="form-control">
                                                                                    @error('examination_date[]')
                                                                                        <small class=" text-danger">{{ $message }}</small>
                                                                                    @enderror
                                                                                </div>

                                                                                <div class="form-group col-12 col-md-6 " id="examinations${examinationsCount}">
                                                                                    <label for="examination[]">ผลการตรวจโรค</label>
                                                                                    <input type="text" name="examination[]" class="form-control">
                                                                                    @error('examination[]')
                                                                                        <small class=" text-danger">{{ $message }}</small>
                                                                                    @enderror
                                                                                </div>
                                                                                                   `)
            }

            function removeExamination() {
                $(`#examinations_date${examinationsCount}`).remove()
                $(`#examinations${examinationsCount}`).remove()
                examinationsCount--
            }

        </script>
    @endsection
</x-adminlte-layout>
