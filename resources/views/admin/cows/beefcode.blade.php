<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>QR Code</title>
</head>

<body>
    <center>

    </center>
    <table align="center" border="1" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://allnewgen.co.th/cowkit/scanqrcode.php?id={{ $cow->id }}&bf=1&choe=UTF-8"
                    title="QR Code" />
                <h3>เนื้อสันคอ<br />Chuck</h3>
            </td>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://allnewgen.co.th/cowkit/scanqrcode.php?id={{ $cow->id }}&bf=2&choe=UTF-8"
                    title="QR Code" />
                <h3>เสือร้องไห้<br />Brisket</h3>
            </td>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://allnewgen.co.th/cowkit/scanqrcode.php?id={{ $cow->id }}&bf=3&choe=UTF-8"
                    title="QR Code" />
                <h3>ฝาริบอาย<br />Cab Rib</h3>
            </td>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://allnewgen.co.th/cowkit/scanqrcode.php?id={{ $cow->id }}&bf=4&choe=UTF-8"
                    title="QR Code" />
                <h3>ริบอาย<br />Rib eye</h3>
            </td>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://allnewgen.co.th/cowkit/scanqrcode.php?id={{ $cow->id }}&bf=5&choe=UTF-8"
                    title="QR Code" />
                <h3>สันนอก<br />Strip Loin</h3>
            </td>
        </tr>
        <tr>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://allnewgen.co.th/cowkit/scanqrcode.php?id={{ $cow->id }}&bf=6&choe=UTF-8"
                    title="QR Code" />
                <h3>สันสะโพก<br />Sir Loin</h3>
            </td>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://allnewgen.co.th/cowkit/scanqrcode.php?id={{ $cow->id }}&bf=7&choe=UTF-8"
                    title="QR Code" />
                <h3>เนื้อใบบัว<br />Jumeau</h3>
            </td>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://allnewgen.co.th/cowkit/scanqrcode.php?id={{ $cow->id }}&bf=8&choe=UTF-8"
                    title="QR Code" />
                <h3>สะโพกใน<br />Top Round</h3>
            </td>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://allnewgen.co.th/cowkit/scanqrcode.php?id={{ $cow->id }}&bf=9&choe=UTF-8"
                    title="QR Code" />
                <h3>สะโพกนอก<br />Bottom Round</h3>
            </td>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://allnewgen.co.th/cowkit/scanqrcode.php?id={{ $cow->id }}&bf=10&choe=UTF-8"
                    title="QR Code" />
                <h3>ลูกมะพร้าว<br />Sirloin Tip</h3>
            </td>
        </tr>
        <tr>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://allnewgen.co.th/cowkit/scanqrcode.php?id={{ $cow->id }}&bf=11&choe=UTF-8"
                    title="QR Code" />
                <h3>หางตะเข้<br />Aiguillette baronne</h3>
            </td>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://allnewgen.co.th/cowkit/scanqrcode.php?id={{ $cow->id }}&bf=12&choe=UTF-8"
                    title="QR Code" />
                <h3>ใบพาย<br />Parerlon</h3>
            </td>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://allnewgen.co.th/cowkit/scanqrcode.php?id={{ $cow->id }}&bf=13&choe=UTF-8"
                    title="QR Code" />
                <h3>เนื้อน่อง<br />Shank</h3>
            </td>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://allnewgen.co.th/cowkit/scanqrcode.php?id={{ $cow->id }}&bf=14&choe=UTF-8"
                    title="QR Code" />
                <h3>เนื้อชายท้อง<br />Bavette</h3>
            </td>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://allnewgen.co.th/cowkit/scanqrcode.php?id={{ $cow->id }}&bf=15&choe=UTF-8"
                    title="QR Code" />
                <h3>ร่องซี่โครง<br />Short Rib</h3>
            </td>
        </tr>
        <tr>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://allnewgen.co.th/cowkit/scanqrcode.php?id={{ $cow->id }}&bf=16&choe=UTF-8"
                    title="QR Code" />
                <h3>น่องแก้ว<br />Silver Shank</h3>
            </td>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://allnewgen.co.th/cowkit/scanqrcode.php?id={{ $cow->id }}&bf=17&choe=UTF-8"
                    title="QR Code" />
                <h3>ตะพาบ<br />Macreuse</h3>
            </td>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://allnewgen.co.th/cowkit/scanqrcode.php?id={{ $cow->id }}&bf=18&choe=UTF-8"
                    title="QR Code" />
                <h3>เนื้อแดง<br />G Meat</h3>
            </td>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://allnewgen.co.th/cowkit/scanqrcode.php?id={{ $cow->id }}&bf=19&choe=UTF-8"
                    title="QR Code" />
                <h3>ซี่โครงเอ<br />Shot rip A</h3>
            </td>
            <td></td>
        </tr>
    </table>
</body>

</html>
