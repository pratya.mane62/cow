<x-adminlte-layout>
    <x-slot name="title">
        {{ __('Cow') }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">

            <h4> {{ __('Cow') }}</h4>
        </div>
        <div class="card-body">
            <x-auth-validation-errors class="mb-4" :errors="$errors" />
            <div class="card-content row justify-content-center">
                <div class="col-12 col-xl-6 my-4">
                    <img id="blah" src="#" alt="รูปภาพ" class=" img-thumbnail img-fluid w-100" />
                </div>
                <div class="col-12 col-xl-6 my-4">
                    <img id="blahrfid" src="#" alt="รูปภาพ" class=" img-thumbnail img-fluid w-100" />
                </div>
                <form action=" {{ route('admin.cows.store') }} " method="post" id="myForm"
                    enctype="multipart/form-data" class=" col-12 col-xl-12 row justify-content-end">
                    @csrf
                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="photo"> รูป </label>
                        <input type="file" name="photo" value="{{ old('photo', '') }}" class="form-control"
                            id="imgInp">
                        @error('photo')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="photorfid"> รูปถ่ายกับ RFIDTAG </label>
                        <input type="file" name="photorfid" value="{{ old('photorfid', '') }}" class="form-control"
                            id="imgInprfid">
                        @error('photorfid')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="rfid">RFID</label>
                        <input type="text" name="rfid" value="{{ old('rfid', '') }}" id="rfid" class="form-control">
                        @error('rfid')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="name">ชื่อ</label>
                        <input type="text" name="name" value="{{ old('name', '') }}" id="name" class="form-control">
                        @error('name')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="weight">น้ำหนัก Kg.</label>
                        <input type="number" step="0.01" name="weight" value="{{ old('weight', '') }}" id="weight"
                            class="form-control">
                        @error('weight')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="type">ประเภท</label>
                        <select class="form-control" name="type" id="type">
                            <option value="mom">แม่พันธ์ุ</option>
                            <option value="dad">พ่อพันธ์ุ</option>
                            <option value="cattle">โคขุน</option>
                        </select>
                    </div>

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="birth">วันเกิด(ถ้ามี)</label>
                        <input type="date" name="birth" value="{{ old('birth', '') }}" id="birth"
                            class="form-control">
                        @error('birth')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="farm_id">เลือกฟาร์ม</label>
                        <select class="form-control" name="farm_id" value="" id="farm_id">
                            @if ($farms->count() > 0)
                                @foreach ($farms as $farm)
                                    <option value="{{ $farm->id }} ">{{ $farm->title }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="sex">เพศ</label>
                        <div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="sex" value="male" checked>
                                <label class="form-check-label">ผู้</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="sex" value="female">
                                <label class="form-check-label">เมีย</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-12  col-md-12 ">
                        <label for="specie">สายพันธุ์</label>
                        <select class="form-control" name="specie_id" id="specie_id">
                            @if ($secies->count() > 0)
                                @foreach ($secies as $sepie)
                                    <option value="{{ $sepie->id }} ">{{ $sepie->title }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group col-12 col-md-12">
                        <select name="pedigree_level" id="pedigree_level">
                            <option value="0" selected>ระดับสายเลือด</option>
                            <option value="1">F1</option>
                            <option value="2">F2</option>
                            <option value="3">F3</option>
                            <option value="4">F4</option>
                            <option value="5">F5</option>
                            <option value="6">F6</option>
                            <option value="7">F7</option>
                            <option value="8">F8</option>
                            <option value="9">F9</option>
                            <option value="10">F10</option>
                        </select>
                    </div>
                    <div class="col-12 row p-0 m-0">

                        <div class="form-group col-12 col-md-6 ">
                            <label for="F5name_dad">ชื่อพ่อพันธุ์</label>
                            <input type="text" value="{{ old('F5name_dad', '') }}" name="F5name_dad" id="F5name_dad"
                                class=" form-control">
                            <label for="F5specie">สายพันธุ์</label>
                            <select class="form-control" name="F5specie_dad" id="F5specie_dad">
                                <option value="">เลือก</option>
                                @if ($secies->count() > 0)
                                    @foreach ($secies as $sepie)
                                        <option value="{{ $sepie->id }} " @if (old('F5specie_dad') == $sepie->id) selected="selected" @endif>
                                            {{ $sepie->title }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="form-group pt-3">
                                <label for="f5blood_dad">ปริมาณเลือด ( <span
                                        class="f5blood_dad_val font-weight-bold "></span> %)</label>
                                <div class="d-flex w-100">
                                    <input id="f5blood_dad" class="form-control" type="number" min="0" max="100" step="any"
                                        name="f5blood_dad" style="width: 100%" />

                                </div>
                            </div>
                        </div>

                        <div class="form-group col-12 col-md-6 ">
                            <label for="F5name_mom">ชื่อแม่พันธุ์</label>
                            <input type="text" value="{{ old('F5name_mom', '') }}" name="F5name_mom" id="F5name_mom"
                                class=" form-control">
                            <label for="specie">สายพันธุ์</label>
                            <select class="form-control" name="F5specie_mom" id="F5specie_mom">
                                <option value="">เลือก</option>
                                @if ($secies->count() > 0)
                                    @foreach ($secies as $sepie)
                                        <option value="{{ $sepie->id }} " @if (old('F5specie_mom') == $sepie->id) selected="selected" @endif>
                                            {{ $sepie->title }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="form-group pt-3">
                                <label for="f5blood_mom">ปริมาณเลือด ( <span
                                        class="f5blood_mom_val font-weight-bold "></span> %)</label>
                                <div class="d-flex w-100">
                                    <input id="f5blood_mom" class="form-control" type="number" min="0" max="100" step="any"
                                        name="f5blood_mom" style="width: 100%" />

                                </div>
                            </div>
                        </div>

                    </div>

                    {{-- <div class="col-11 row p-0 m-0">

                        <div class="form-group col-12 col-md-6 ">
                            <h5>F4</h5>
                            <label for="f4name_dad">ชื่อพ่อพันธุ์</label>
                            <input type="text" value="{{ old('f4name_dad', '') }}" name="f4name_dad" id="f4name_dad"
                                class=" form-control">
                            <label for="specie">สายพันธุ์</label>
                            <select class="form-control" name="f4specie_dad" id="f4specie_dad">
                                <option value="">เลือก</option>
                                @if ($secies->count() > 0)
                                    @foreach ($secies as $sepie)
                                        <option value="{{ $sepie->id }} " @if (old('f4specie_dad') == $sepie->id) selected="selected" @endif>
                                            {{ $sepie->title }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="form-group pt-3">
                                <label for="f4blood_dad">ปริมาณเลือด ( <span
                                        class="f4blood_dad_val font-weight-bold "></span> %)</label>
                                <div class="d-flex w-100">
                                    <input id="f4blood_dad" class="border-0" type="range" min="0" max="100" value="0"
                                        name="f4blood_dad" style="width: 100%" />

                                </div>
                            </div>
                        </div>

                        <div class="form-group col-12 col-md-6 ">
                            <h5 class=" text-transparent text-white "> F4</h5>
                            <label for="f4name_mom">ชื่อแม่พันธุ์</label>
                            <input type="text" value="{{ old('f4name_mom', '') }}" name="f4name_mom" id="f4name_mom"
                                class=" form-control">
                            <label for="specie">สายพันธุ์</label>
                            <select class="form-control" name="f4specie_mom" id="f4specie_mom">
                                <option value="">เลือก</option>
                                @if ($secies->count() > 0)
                                    @foreach ($secies as $sepie)
                                        <option value="{{ $sepie->id }} " @if (old('f4specie_mom') == $sepie->id) selected="selected" @endif>
                                            {{ $sepie->title }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="form-group pt-3">
                                <label for="f4blood_mom">ปริมาณเลือด ( <span
                                        class="f4blood_mom_val font-weight-bold "></span> %)</label>
                                <div class="d-flex w-100">
                                    <input id="f4blood_mom" class="border-0" type="range" min="0" max="100" value="0"
                                        name="f4blood_mom" style="width: 100%" />

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-10 row p-0 m-0">

                        <div class="form-group col-12 col-md-6 ">
                            <h5>F3</h5>
                            <label for="name_dad">ชื่อพ่อพันธุ์</label>
                            <input type="text" value="{{ old('name_dad', '') }}" name="name_dad" id="name_dad"
                                class=" form-control">
                            <label for="specie">สายพันธุ์</label>
                            <select class="form-control" name="specie_dad" id="specie_dad">
                                <option value="">เลือก</option>
                                @if ($secies->count() > 0)
                                    @foreach ($secies as $sepie)
                                        <option value="{{ $sepie->id }} " @if (old('specie_dad') == $sepie->id) selected="selected" @endif>
                                            {{ $sepie->title }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="form-group pt-3">
                                <label for="f3blood_dad">ปริมาณเลือด ( <span
                                        class="f3blood_dad_val font-weight-bold "></span> %)</label>
                                <div class="d-flex w-100">
                                    <input id="f3blood_dad" class="border-0" type="range" min="0" max="100" value="0"
                                        name="f3blood_dad" style="width: 100%" />

                                </div>
                            </div>
                        </div>

                        <div class="form-group col-12 col-md-6 ">
                            <h5 class=" text-transparent text-white "> F3</h5>
                            <label for="name_mom">ชื่อแม่พันธุ์</label>
                            <input type="text" value="{{ old('name_mom', '') }}" name="name_mom" id="name_mom"
                                class=" form-control">
                            <label for="specie">สายพันธุ์</label>
                            <select class="form-control" name="specie_mom" id="specie_mom">
                                <option value="">เลือก</option>
                                @if ($secies->count() > 0)
                                    @foreach ($secies as $sepie)
                                        <option value="{{ $sepie->id }} " @if (old('specie_mom') == $sepie->id) selected="selected" @endif>
                                            {{ $sepie->title }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="form-group pt-3">
                                <label for="f3blood_mom">ปริมาณเลือด ( <span
                                        class="f3blood_mom_val font-weight-bold "></span> %)</label>
                                <div class="d-flex w-100">
                                    <input id="f3blood_mom" class="border-0" type="range" min="0" max="100" value="0"
                                        name="f3blood_mom" style="width: 100%" />

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-9 row p-0 m-0">

                        <div class="form-group col-12 col-md-6 ">
                            <h5>F2</h5>
                            <label for="name_dad_dad">ชื่อพ่อพันธุ์</label>
                            <input type="text" value="{{ old('name_dad_dad', '') }}" name="name_dad_dad"
                                id="name_dad_dad" class=" form-control">
                            <label for="specie">สายพันธุ์</label>
                            <select class="form-control" name="specie_dad_dad" id="specie_dad_dad">
                                <option value="">เลือก</option>
                                @if ($secies->count() > 0)
                                    @foreach ($secies as $sepie)
                                        <option value="{{ $sepie->id }} " @if (old('specie_dad_dad') == $sepie->id) selected="selected" @endif>
                                            {{ $sepie->title }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="form-group pt-3">
                                <label for="f2blood_dad">ปริมาณเลือด ( <span
                                        class="f2blood_dad_val font-weight-bold "></span> %)</label>
                                <div class="d-flex w-100">
                                    <input id="f2blood_dad" class="border-0" type="range" min="0" max="100" value="0"
                                        name="f2blood_dad" style="width: 100%" />

                                </div>
                            </div>
                        </div>

                        <div class="form-group col-12 col-md-6 ">
                            <h5 class=" text-transparent text-white "> F5</h5>
                            <label for="name_mom_dad">ชื่อแม่พันธุ์</label>
                            <input type="text" value="{{ old('name_mom_dad', '') }}" name="name_mom_dad"
                                id="name_mom_dad" class=" form-control">
                            <label for="specie">สายพันธุ์</label>
                            <select class="form-control" name="specie_mom_dad" id="specie_mom_dad">
                                <option value="">เลือก</option>
                                @if ($secies->count() > 0)
                                    @foreach ($secies as $sepie)
                                        <option value="{{ $sepie->id }} " @if (old('specie_mom_dad') == $sepie->id) selected="selected" @endif>
                                            {{ $sepie->title }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="form-group pt-3">
                                <label for="f2blood_mom">ปริมาณเลือด ( <span
                                        class="f2blood_mom_val font-weight-bold "></span> %)</label>
                                <div class="d-flex w-100">
                                    <input id="f2blood_mom" class="border-0" type="range" min="0" max="100" value="0"
                                        name="f2blood_mom" style="width: 100%" />

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-8 row p-0 m-0">

                        <div class="form-group col-12 col-md-6 ">
                            <h5>F1</h5>
                            <label for="name_dad_mom">ชื่อพ่อพันธุ์</label>
                            <input type="text" value="{{ old('name_dad_mom', '') }}" name="name_dad_mom"
                                id="name_dad_mom" class=" form-control">
                            <label for="specie">สายพันธุ์</label>
                            <select class="form-control" name="specie_dad_mom" id="specie_dad_mom">
                                <option value="">เลือก</option>
                                @if ($secies->count() > 0)
                                    @foreach ($secies as $sepie)
                                        <option value="{{ $sepie->id }} " @if (old('specie_dad_mom') == $sepie->id) selected="selected" @endif>
                                            {{ $sepie->title }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="form-group pt-3">
                                <label for="f1blood_dad">ปริมาณเลือด ( <span
                                        class="f1blood_dad_val font-weight-bold "></span> %)</label>
                                <div class="d-flex w-100">
                                    <input id="f1blood_dad" class="border-0" type="range" min="0" max="100" value="0"
                                        name="f1blood_dad" style="width: 100%" />

                                </div>
                            </div>
                        </div>

                        <div class="form-group col-12 col-md-6 ">
                            <h5 class=" text-transparent text-white "> F5</h5>
                            <label for="name_mom_mom">ชื่อแม่พันธุ์</label>
                            <input type="text" value="{{ old('name_mom_mom', '') }}" name="name_mom_mom"
                                id="name_mom_mom" class=" form-control">
                            <label for="specie">สายพันธุ์</label>
                            <select class="form-control" name="specie_mom_mom" id="specie_mom_mom">
                                <option value="">เลือก</option>
                                @if ($secies->count() > 0)
                                    @foreach ($secies as $sepie)
                                        <option value="{{ $sepie->id }} " @if (old('specie_mom_mom') == $sepie->id) selected="selected" @endif>
                                            {{ $sepie->title }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="form-group pt-3">
                                <label for="f1blood_mom">ปริมาณเลือด ( <span
                                        class="f1blood_mom_val font-weight-bold "></span> %)</label>
                                <div class="d-flex w-100">
                                    <input id="f1blood_mom" class="border-0" type="range" min="0" max="100" value="0"
                                        name="f1blood_mom" style="width: 100%" />

                                </div>
                            </div>
                        </div>

                    </div> --}}

                    <div class="form-group mt-4 col-12">
                        <button class="btn btn-success float-right" id="addCowBtn"
                            type="submit">{{ __('AddData') }}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</x-adminlte-layout>
