<x-adminlte-layout>
    <x-slot name="title">
        {{ __('ของ') }} {{ $cow->name }}
    </x-slot>

    <div class="card shadow">
        <h4 class=" card-body text-secondary">{{ __('ของ') }} {{ $cow->name }}</h4>
        <div class="card-body px-5">
            @foreach ($transections as $i => $timeline)
                @php
                    $dewormings = App\Models\Deworming::where('cow_id', $cow->id)
                        ->where('farm_id', $timeline->farm_id)
                        ->orderBy('deworming', 'asc')
                        ->get();
                    $vaccinations = App\Models\Vaccination::where('cow_id', $cow->id)
                        ->where('farm_id', $timeline->farm_id)
                        ->orderBy('vaccination_date', 'asc')
                        ->get();
                    $examinations = App\Models\Examination::where('cow_id', $cow->id)
                        ->where('farm_id', $timeline->farm_id)
                        ->orderBy('examination_date', 'asc')
                        ->get();
                @endphp
                @if ($i % 2 != 0)
                    <!-- timeline item 2 -->
                    <div class="row no-gutters">
                        <div class="col-sm">
                            <!--spacer-->
                        </div>
                        <div class="col-sm-1 text-center flex-column d-none d-sm-flex">
                            <div class="row h-50">
                                <div class="col border-right">&nbsp;</div>
                                <div class="col">&nbsp;</div>
                            </div>
                            <h5 class="m-2">
                                <span class="badge badge-pill @if ($last->farm_id ==
                                $timeline->farm_id) bg-success @else bg-light border @endif
                                    ">&nbsp;</span>
                            </h5>
                            <div class="row h-50">
                                <div class="col border-right">&nbsp;</div>
                                <div class="col">&nbsp;</div>
                            </div>
                        </div>
                        <div class="col-sm py-2">
                            <div class="card  @if ($last->farm_id == $timeline->farm_id) border-success shadow @endif ">
                                <div class="card-body">
                                    <div class="float-right  @if ($last->farm_id ==
                                    $timeline->farm_id) text-success @else text-muted @endif">{{ $timeline->updated_at }}</div>
                                    <h4 class="card-title  @if ($last->farm_id ==
                                    $timeline->farm_id) text-success @else text-muted @endif"
                                        >{{ $timeline->farms()->first()->title }}</h4>
                                    <p class=" card-text">
                                    <div class="row">
                                        <div class="col-12 col-lg-6 col-xl-4">
                                            <img src="{{ asset('storage/uploads/' . $timeline->photo) }}"
                                                alt="{{ $cow->name }}" class=" img-fluid img-thumbnail">
                                        </div>
                                        <div class="col-12 col-lg-6 col-xl-8 row">
                                            <div class="col-12 ">
                                                <b>ข้อมูลสืบย้อนกลับ</b>
                                                <p> {{ $timeline->farms()->first()->traceback }}</p>
                                            </div>
                                            <div class="col-12 ">
                                                <b>อายุ</b>
                                                {{ \Carbon\Carbon::parse($cow->birth)->diff($timeline->created_at)->format('%y ปี, %m เดือน และอีก   %d วัน') }}
                                            </div>
                                            <div class="col-12 ">
                                                <b>น้ำหนัก</b>
                                                {{ $timeline->weight }}
                                            </div>
                                            <div class="col-12 mt-2">
                                                <b>ประวัติสุขภาพ</b>
                                            </div>
                                            <div class="col-12">
                                                <ul>
                                                    <li>
                                                        <b>การถ่ายพยาธิ</b><br>
                                                        @if ($dewormings->count() == 0)
                                                            -
                                                        @endif
                                                        @foreach ($dewormings as $i => $item)
                                                            <h6>ครั้งที่ {{ ++$i }} :
                                                                {{ \Carbon\Carbon::parse($item->deworming)->format('d/m/Y') ?? '-' }}
                                                            </h6>
                                                        @endforeach
                                                    </li>
                                                    <li>
                                                        <b>การฉีดวัคซีน</b><br>
                                                        @if ($vaccinations->count() == 0)
                                                            -
                                                        @endif
                                                        @foreach ($vaccinations as $i => $item)
                                                            <span>ครั้งที่ {{ ++$i }} :
                                                                {{ \Carbon\Carbon::parse($item->vaccination_date)->format('d/m/Y') ?? '-' }}</span>
                                                            <p>{{ $item->vaccination ?? '-' }}</p>
                                                        @endforeach
                                                    </li>
                                                    <li>
                                                        <b>การตรวจโรค</b><br>
                                                        @if ($examinations->count() == 0)
                                                            -
                                                        @endif
                                                        @foreach ($examinations as $i => $item)
                                                            <span>ครั้งที่ {{ ++$i }} :
                                                                {{ \Carbon\Carbon::parse($item->examination_date)->format('d/m/Y') ?? '-' }}</span>
                                                            <p>{{ $item->examination ?? '-' }}</p>
                                                        @endforeach
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->
                @else
                    <!-- timeline item 2 -->
                    <div class="row no-gutters">
                        <div class="col-sm py-2">
                            <div class="card  @if ($last->farm_id == $timeline->farm_id) border-success shadow @endif ">
                                <div class="card-body">
                                    <div class="float-right  @if ($last->farm_id ==
                                    $timeline->farm_id) text-success @else text-muted @endif">{{ $timeline->updated_at }}</div>
                                    <h4 class="card-title  @if ($last->farm_id ==
                                    $timeline->farm_id) text-success @else text-muted @endif"
                                        >{{ $timeline->farms()->first()->title }}</h4>
                                    <p class=" card-text">
                                    <div class="row">
                                        <div class="col-12 col-lg-6 col-xl-4">
                                            @isset($timeline->photo)
                                                <img src="{{ asset('storage/uploads/' . $timeline->photo) }}"
                                                    alt="{{ $cow->name }}" class=" img-fluid img-thumbnail">
                                            @endisset
                                            <div class="mt-1"></div>
                                            @isset($timeline->photorfid)
                                                <img src="{{ asset('storage/uploads/' . $timeline->photorfid) }}"
                                                    alt="{{ $cow->name }}" class=" img-fluid img-thumbnail">
                                            @endisset
                                        </div>
                                        <div class="col-12 col-lg-6 col-xl-8 row">
                                            <div class="col-12 ">
                                                <b>ข้อมูลสืบย้อนกลับ</b>
                                                <p> {{ $timeline->farms()->first()->traceback }}</p>
                                            </div>
                                            <div class="col-12 ">
                                                <b>อายุ</b>
                                                {{ \Carbon\Carbon::parse($cow->birth)->diff($timeline->created_at)->format('%y ปี, %m เดือน และอีก   %d วัน') }}
                                            </div>
                                            <div class="col-12 ">
                                                <b>น้ำหนัก</b>
                                                {{ $timeline->weight }}
                                            </div>
                                            <div class="col-12 mt-2">
                                                <b>ประวัติสุขภาพ</b>
                                            </div>
                                            <div class="col-12">
                                                <ul>
                                                    <li>
                                                        <b>การถ่ายพยาธิ</b><br>
                                                        @if ($dewormings->count() == 0)
                                                            -
                                                        @endif
                                                        @foreach ($dewormings as $i => $item)
                                                            <h6>ครั้งที่ {{ ++$i }} :
                                                                {{ \Carbon\Carbon::parse($item->deworming)->format('d/m/Y') ?? '-' }}
                                                            </h6>
                                                        @endforeach
                                                    </li>
                                                    <li>
                                                        <b>การฉีดวัคซีน</b><br>
                                                        @if ($vaccinations->count() == 0)
                                                            -
                                                        @endif
                                                        @foreach ($vaccinations as $i => $item)
                                                            <span>ครั้งที่ {{ ++$i }} :
                                                                {{ \Carbon\Carbon::parse($item->vaccination_date)->format('d/m/Y') ?? '-' }}</span>
                                                            <p>{{ $item->vaccination ?? '-' }}</p>
                                                        @endforeach
                                                    </li>
                                                    <li>
                                                        <b>การตรวจโรค</b><br>
                                                        @if ($examinations->count() == 0)
                                                            -
                                                        @endif
                                                        @foreach ($examinations as $i => $item)
                                                            <span>ครั้งที่ {{ ++$i }} :
                                                                {{ \Carbon\Carbon::parse($item->examination_date)->format('d/m/Y') ?? '-' }}</span>
                                                            <p>{{ $item->examination ?? '-' }}</p>
                                                        @endforeach
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1 text-center flex-column d-none d-sm-flex">
                            <div class="row h-50">
                                <div class="col border-right">&nbsp;</div>
                                <div class="col">&nbsp;</div>
                            </div>
                            <h5 class="m-2">
                                <span class="badge badge-pill @if ($last->farm_id ==
                                $timeline->farm_id) bg-success @else bg-light border @endif
                                    ">&nbsp;</span>
                            </h5>
                            <div class="row h-50">
                                <div class="col border-right">&nbsp;</div>
                                <div class="col">&nbsp;</div>
                            </div>
                        </div>
                        <div class="col-sm">
                            <!--spacer-->
                        </div>
                    </div>
                    <!--/row-->
                @endif
            @endforeach
        </div>
    </div>
</x-adminlte-layout>
