<x-adminlte-layout>
    <x-slot name="title">
        {{ __('Dashboard') }}
    </x-slot>

    <div class="card">
        <div class="card-header">
            <h4><b> ออก QR Code </b></h4>
            <!-- <h6><a href="specie_form.php">เพิ่มสายพันธุ์</a></b></h6> -->
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="small-box bg-primary">
                        <div class="inner">
                            <h5>QR Code</h5>
                            <div class="text-center">
                                <div class="mx-5"></div>
                                <h3> สำหรับซากวัว </h3>
                            </div>
                        </div>
                        <div class="icon">
                            <!-- <i class="ion ion-stats-bars"></i> -->
                            <i class="fas fa-qrcode"></i>
                        </div>
                        <div class="d-flex small-box-footer justify-content-end px-3">
                            <a href="{{ asset('qrcode/carrion.php?cow_id=') }}{{$cow->id}}" class="text-light" target="_blank">
                                <small>ออก QR Code</small>
                                <i class="fas fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <h2>QR Code สำรับชิ้นเนื้อ</h2>
            @csrf

            <div class="row">
                <div class="col-md-2">
                    <div class="card mb-3">
                        <img src="{{ asset('breef/chuck.png') }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">เนื้อสันคอ<br />Chuck</h5>
                            <p class="card-text">
                            <form action="{{ asset('qrcode/singlebeef.php') }}" method="post" target="_blank">
                                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                                <input type="hidden" name="bf_id" value="1" />
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        QR Code <i class="fas fa-qrcode"></i>
                                    </button>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="card mb-3">
                        <img src="{{ asset('breef/Brisket.png') }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">เสือร้องไห้<br />Brisket</h5>
                            <p class="card-text">
                            <form action="{{ asset('qrcode/singlebeef.php') }}" method="post" target="_blank">
                                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                                <input type="hidden" name="bf_id" value="2" />
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        QR Code <i class="fas fa-qrcode"></i>
                                    </button>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="card mb-3">
                        <img src="{{ asset('breef/Cab_Rib.png') }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">ฝาริบอาย<br />Cab Rib</h5>
                            <p class="card-text">
                            <form action="{{ asset('qrcode/singlebeef.php') }}" method="post" target="_blank">
                                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                                <input type="hidden" name="bf_id" value="3" />
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        QR Code <i class="fas fa-qrcode"></i>
                                    </button>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="card mb-3">
                        <img src="{{ asset('breef/Rib_eye.png') }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">ริบอาย<br />Rib eye</h5>
                            <p class="card-text">
                            <form action="{{ asset('qrcode/singlebeef.php') }}" method="post" target="_blank">
                                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                                <input type="hidden" name="bf_id" value="4" />
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        QR Code <i class="fas fa-qrcode"></i>
                                    </button>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="card mb-3">
                        <img src="{{ asset('breef/Strip_Loin.png') }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">สันนอก<br />Strip Loin</h5>
                            <p class="card-text">
                            <form action="{{ asset('qrcode/singlebeef.php') }}" method="post" target="_blank">
                                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                                <input type="hidden" name="bf_id" value="5" />
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        QR Code <i class="fas fa-qrcode"></i>
                                    </button>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="card mb-3">
                        <img src="{{ asset('breef/Sir_Loin.png') }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">สันสะโพก<br />Sir Loin</h5>
                            <p class="card-text">
                            <form action="{{ asset('qrcode/singlebeef.php') }}" method="post" target="_blank">
                                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                                <input type="hidden" name="bf_id" value="6" />
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        QR Code <i class="fas fa-qrcode"></i>
                                    </button>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-md-2">
                    <div class="card mb-3">
                        <img src="{{ asset('breef/Jumeau.png') }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">เนื้อใบบัว<br />Jumeau</h5>
                            <p class="card-text">
                            <form action="{{ asset('qrcode/singlebeef.php') }}" method="post" target="_blank">
                                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                                <input type="hidden" name="bf_id" value="7" />
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        QR Code <i class="fas fa-qrcode"></i>
                                    </button>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="card mb-3">
                        <img src="{{ asset('breef/Top_Round.png') }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">สะโพกใน<br />Top Round</h5>
                            <p class="card-text">
                            <form action="{{ asset('qrcode/singlebeef.php') }}" method="post" target="_blank">
                                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                                <input type="hidden" name="bf_id" value="8" />
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        QR Code <i class="fas fa-qrcode"></i>
                                    </button>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="card mb-3">
                        <img src="{{ asset('breef/Bottom_Round.png') }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">สะโพกนอก<br />Bottom Round</h5>
                            <p class="card-text">
                            <form action="{{ asset('qrcode/singlebeef.php') }}" method="post" target="_blank">
                                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                                <input type="hidden" name="bf_id" value="9" />
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        QR Code <i class="fas fa-qrcode"></i>
                                    </button>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="card mb-3">
                        <img src="{{ asset('breef/Sirloin_Tip.png') }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">ลูกมะพร้าว<br />Sirloin Tip</h5>
                            <p class="card-text">
                            <form action="{{ asset('qrcode/singlebeef.php') }}" method="post" target="_blank">
                                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                                <input type="hidden" name="bf_id" value="10" />
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        QR Code <i class="fas fa-qrcode"></i>
                                    </button>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="card mb-3">
                        <img src="{{ asset('breef/Aigullette_Barnonne.png') }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">หางตะเข้<br />Aiguillette baronne</h5>
                            <p class="card-text">
                            <form action="{{ asset('qrcode/singlebeef.php') }}" method="post" target="_blank">
                                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                                <input type="hidden" name="bf_id" value="11" />
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        QR Code <i class="fas fa-qrcode"></i>
                                    </button>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="card mb-3">
                        <img src="{{ asset('breef/Parerlon.png') }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">ใบพาย<br />Parerlon</h5>
                            <p class="card-text">
                            <form action="{{ asset('qrcode/singlebeef.php') }}" method="post" target="_blank">
                                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                                <input type="hidden" name="bf_id" value="12" />
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        QR Code <i class="fas fa-qrcode"></i>
                                    </button>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-md-2">
                    <div class="card mb-3">
                        <img src="{{ asset('breef/Shank.png') }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">เนื้อน่อง<br />Shank</h5>
                            <p class="card-text">
                            <form action="{{ asset('qrcode/singlebeef.php') }}" method="post" target="_blank">
                                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                                <input type="hidden" name="bf_id" value="13" />
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        QR Code <i class="fas fa-qrcode"></i>
                                    </button>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="card mb-3">
                        <img src="{{ asset('breef/Bavette.png') }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">เนื้อชายท้อง<br />Bavette</h5>
                            <p class="card-text">
                            <form action="{{ asset('qrcode/singlebeef.php') }}" method="post" target="_blank">
                                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                                <input type="hidden" name="bf_id" value="14" />
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        QR Code <i class="fas fa-qrcode"></i>
                                    </button>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="card mb-3">
                        <img src="{{ asset('breef/Short_Rib.png') }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">ร่องซี่โครง<br />Short Rib</h5>
                            <p class="card-text">
                            <form action="{{ asset('qrcode/singlebeef.php') }}" method="post" target="_blank">
                                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                                <input type="hidden" name="bf_id" value="15" />
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        QR Code <i class="fas fa-qrcode"></i>
                                    </button>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="card mb-3">
                        <img src="{{ asset('breef/Silver_Shank.png') }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">น่องแก้ว<br />Silver Shank</h5>
                            <p class="card-text">
                            <form action="{{ asset('qrcode/singlebeef.php') }}" method="post" target="_blank">
                                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                                <input type="hidden" name="bf_id" value="16" />
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        QR Code <i class="fas fa-qrcode"></i>
                                    </button>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="card mb-3">
                        <img src="{{ asset('breef/Macreuse.png') }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">ตะพาบ<br />Macreuse</h5>
                            <p class="card-text">
                            <form action="{{ asset('qrcode/singlebeef.php') }}" method="post" target="_blank">
                                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                                <input type="hidden" name="bf_id" value="17" />
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        QR Code <i class="fas fa-qrcode"></i>
                                    </button>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="card mb-3">
                        <img src="{{ asset('breef/G_Meat.png') }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">เนื้อแดง<br />G Meat</h5>
                            <p class="card-text">
                            <form action="{{ asset('qrcode/singlebeef.php') }}" method="post" target="_blank">
                                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                                <input type="hidden" name="bf_id" value="18" />
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        QR Code <i class="fas fa-qrcode"></i>
                                    </button>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-md-2">
                    <div class="card mb-3">
                        <img src="{{ asset('breef/Shot_rip_A.png') }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">ซี่โครงเอ<br />Shot rip A</h5>
                            <p class="card-text">
                            <form action="{{ asset('qrcode/singlebeef.php') }}" method="post" target="_blank">
                                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                                <input type="hidden" name="bf_id" value="19" />
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        QR Code <i class="fas fa-qrcode"></i>
                                    </button>
                                </div>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>

            </div>

            <form action="{{ asset('qrcode/allbeef.php') }}" method="post" target="_blank">
                <input type="hidden" name="cow_id" value="{{ $cow->id }}" />
                <div class="row btn-link">
                    <div class="col">
                        <div class="d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">
                                ออก QR Code ทั้งหมด
                                <i class="fas fa-angle-right"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>


        </div>
    </div>
</x-adminlte-layout>