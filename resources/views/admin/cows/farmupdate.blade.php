<x-adminlte-layout>
    <x-slot name="title">
        {{ __('Cow') }}
    </x-slot>
    <div class="row justify-content-center">
        <div class="col-12 col-md-8 col-lg-6">

            <div class="card shadow">
                <div class="card-header">
                    <h4> {{ __('Cow') }}</h4>
                </div>
                <div class="card-body">
                    @php
                        $c = App\Models\TransectionCow::where('cow_id', $cow->id)
                            ->orderBy('created_at', 'desc')
                            ->first();
                    @endphp
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <div class="card-content row justify-content-center">

                        <form action=" {{ route('admin.cow.update.farm', $cow) }}" method="post" id="myForm"
                            enctype="multipart/form-data" class=" col-12 col-xl-12 row justify-content-end">
                            @csrf
                            @method('put')

                            <div class="form-group col-12 ">
                                <label for="farm_id">เลือกฟาร์ม</label>
                                <select class="form-control" name="farm_id" id="farm_id">
                                    @foreach ($farms as $farm)
                                        @if ($cow->farm_id != $farm->id)
                                            <option value="{{ $farm->id }}">
                                                {{ $farm->title }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group mt-4 col-12">
                                <button class="btn btn-success float-right" id="addCowBtn"
                                    type="submit">{{ __('EditData') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-adminlte-layout>
