<x-adminlte-layout>
    <x-slot name="title">
        {{ __('Cow') }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">
            <div class="float-left">
                <h4>{{ __('Cow') }}</h4>
            </div>
            @can('isFarm')
                <a href=" {{ route('admin.cows.create') }} " class=" btn btn-info float-right">
                    {{ __('AddData') }}
                </a>
            @endcan
        </div>
        <div class="card-body">
            <div class="card-content">
                <table class=" table_id " style="width:100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>RFID</th>
                            <th>ชื่อ</th>
                            <th>สายพันธุ์</th>
                            <th>น้ำหนัก</th>
                            <th>อายุ</th>
                            <th>ประเภท</th>
                            @can('isAdmin') <th>โรงเชือด</th> @endcan
                            <th>action</th>
                        </tr>
                    </thead>
                    @isset($cows)
                        <tbody>
                            @foreach ($cows as $i => $c)
                                <tr>
                                    @php
                                        $cow = App\Models\TransectionCow::where('cow_id', $c->id)
                                            ->orderBy('created_at', 'desc')
                                            ->first();
                                    @endphp
                                    <td>
                                        <img src="{{ isset($cow->photo) ? asset('storage/uploads/' . $cow->photo) : 'https://images.vexels.com/media/users/3/145600/isolated/preview/290e1a100e95214228126e4bda8e3851-cow-avatar-by-vexels.png' }}"
                                            style=" width:100px" alt="{{ $cow->name }}" class=" img-fluid img-thumbnail">
                                    </td>
                                    <td>{{ $c->rfid }} </td>
                                    <td>{{ $c->name }} </td>
                                    <td>{{ $c->specie->title }} </td>
                                    <td>{{ $cow->weight }} </td>
                                    <td>{{ $c->age() }} </td>
                                    <td>
                                        @switch($c->type)
                                            @case('mom')
                                                แม่พันธุ์
                                            @break
                                            @case('dad')
                                                พ่อพันธุ์
                                            @break
                                            @default
                                                โคขุน
                                        @endswitch
                                    </td>
                                    @can('isAdmin') <td>{{ $c->farm()->first()->title }}
                                    </td> @endcan
                                    <td width="100">
                                        <a href="{{ route('admin.cow.timeline', ['cow' => $c]) }} "
                                            class=" btn btn-success btn-sm mb-1"
                                            style="width:80px">{{ __('ไทม์ไลน์') }}</a>
                                        <div class="mx-1"></div>
                                        @can('isFarm')
                                            <a href="{{ route('admin.cow.health', ['cow' => $c]) }} "
                                                class=" btn btn-primary btn-sm mb-1"
                                                style="width:80px">{{ __('ประวัติสุขภาพ') }}</a>
                                            <div class="mx-1"></div>
                                        @endcan
                                        <a href="{{ route('admin.cows.edit', ['cow' => $c]) }} "
                                            class=" btn btn-warning btn-sm mb-1" style="width:80px">{{ __('Edit') }}</a>
                                        <div class="mx-1"></div>

                                        @if ($c->farm()->first()->type == 'slaughterhouse')
                                            <a href="{{ route('admin.cow.qrcode', ['cow' => $c]) }} "
                                                class=" btn btn-info btn-sm mb-1 "
                                                style="width: 80px">{{ __('QR Code') }}</a>

                                            <a href="{{ route('admin.cow.update.status', ['cow' => $c]) }} "
                                                class=" btn btn-primary btn-sm mb-1 " style="width: 80px"
                                                onclick="return confirm('ยืนยันคำสั่งหรือไม่!?')">
                                                เชือดแล้ว </a>
                                        @endif

                                        <form action="{{ route('admin.cows.destroy', ['cow' => $c]) }} " method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class=" btn btn-danger btn-sm mb-1" style="width:80px"
                                                onclick="return confirm('Are you sure?')">{{ __('Delete') }}</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    @endisset
                </table>
            </div>
        </div>
    </div>
</x-adminlte-layout>
