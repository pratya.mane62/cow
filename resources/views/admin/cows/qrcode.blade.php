<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>QR Code</title>
</head>

<body>
    <table align="center">
        <tr>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=https://allnewgen.co.th/cowkit/cow.php?id={{ $cow->id }}&choe=UTF-8"
                    title="QR Code" />
                <h2>ซ้าย</h2>
            </td>
            <td align="center">
                <img src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=https://allnewgen.co.th/cowkit/cow.php?id={{ $cow->id }}&choe=UTF-8"
                    title="QR Code" />
                <h2>ขวา</h2>
            </td>
        </tr>
    </table>
</body>

</html>
