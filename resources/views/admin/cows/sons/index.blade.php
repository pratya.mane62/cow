<x-adminlte-layout>
    <x-slot name="title">
        {{ __('ลูกวัวที่ที่เกิดจาก') }} {{ $cow->name }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">
            <div class="float-left">
                <h4>{{ __('ลูกวัวที่ที่เกิดจาก') }} {{ $cow->name }} </h4>
            </div>
        </div>
        <div class="card-body">
            <div class="card-content">
                <table class=" table_id " style="width:100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>น้ำหนัก</th>
                            <th>วันเกิด</th>
                            <th>อายุ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($sons as $i => $c)
                            <tr>
                                <td>
                                    <img src="{{ isset($c->photo) ? asset('storage/uploads/' . $c->photo) : 'https://images.vexels.com/media/users/3/145600/isolated/preview/290e1a100e95214228126e4bda8e3851-cow-avatar-by-vexels.png' }}"
                                        style=" width:100px" alt="{{ $c->id }}" class=" img-fluid img-thumbnail">
                                </td>
                                <td>{{ $c->weight }} </td>
                                <td>{{ $c->birth }} </td>
                                <td>{{ $c->age() }} </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-adminlte-layout>
