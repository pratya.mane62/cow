<x-adminlte-layout>
    <x-slot name="title">
        {{ __('เพิ่มลูกวัวของ ') . $cow->name }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">

            <h4> {{ __('เพิ่มลูกวัวของ ') . $cow->name }}</h4>
        </div>
        <div class="card-body">
            <div class="col-12 col-xl-6 my-4">
                <img id="blah" src="#" alt="รูปภาพ" class=" img-thumbnail img-fluid w-100" />
            </div>
            <x-auth-validation-errors class="mb-4" :errors="$errors" />
            <div class="card-content row justify-content-center">
                <form action=" {{ route('admin.sons.store', $cow) }} " method="post" id="myForm"
                    enctype="multipart/form-data" class=" col-12 col-xl-12 row">
                    @csrf

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="photo"> รูป </label>
                        <input type="file" name="photo" value="{{ old('photo', '') }}" class="form-control"
                            id="imgInp">
                        @error('photo')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="weight">น้ำหนัก Kg.</label>
                        <input type="number" step="0.01" name="weight" value="{{ old('weight', '') }}" id="weight"
                            class="form-control">
                        @error('weight')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="birth">วันเกิด(ถ้ามี)</label>
                        <input type="date" name="birth" value="{{ old('birth', '') }}" id="birth"
                            class="form-control">
                        @error('birth')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="sex">เพศ</label>
                        <div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" value="male" checked>
                                <label class="form-check-label">ผู้</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" value="female">
                                <label class="form-check-label">เมีย</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group mt-4 col-12">
                        <button class="btn btn-success float-right" id="addCowBtn"
                            type="submit">{{ __('AddData') }}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</x-adminlte-layout>
