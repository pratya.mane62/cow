<x-adminlte-layout>
    <x-slot name="title">
        {{ __('Breeding') }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">

            <h4> {{ __('Breeding') }}</h4>
        </div>
        <div class="card-body">
            <x-auth-validation-errors class="mb-4" :errors="$errors" />
            <div class="card-content row justify-content-center">
                <form action=" {{ route('admin.breeding.store') }} " method="post" id="myForm"
                    enctype="multipart/form-data" class=" col-12 col-xl-12 row">
                    @csrf

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="cow_id">วัวที่ผสม</label>
                        @isset($cow)
                            <input type="text" name="cow" id="cow" disabled class=" form-control"
                                value="{{ $cow->name }}">
                            <input type="hidden" name="cow_id" id="cow_id" value="{{ old('cow_id', $cow->id) }}">
                        @endisset
                        @error('cow_id')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="sperm_id">น้ำเชื้อที่ใช้ผสม</label>
                        <select class="form-control" name="sperm_id" id="sperm_id">
                            @isset($sperms)
                                @foreach ($sperms as $sperm)
                                    <option value="{{ $sperm->id }} " @if (old('sperm_id') == $sperm->id) selected="selected" @endif>
                                        {{ $sperm->bull . '  --  สายพันธุ์: ' . $sperm->specie->title . '  --  แหล่งน้ำเชื้อ: ' . $sperm->resource }}
                                    </option>
                                @endforeach
                            @endisset
                        </select>
                        @error('sperm_id')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="date">วันที่</label>
                        <input type="date" name="date" class=" form-control" value="{{ old('date', '') }}">
                        @error('date')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group col-12 col-sm-12 col-md-6  col-xl-4 ">
                        <label for="time">เวลา</label>
                        <input type="time" name="time" class=" form-control" value="{{ old('time', '') }}">
                        @error('time')
                            <small class=" text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group mt-4 col-12">
                        <button class="btn btn-success float-right" id="addCowBtn"
                            type="submit">{{ __('AddData') }}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</x-adminlte-layout>
