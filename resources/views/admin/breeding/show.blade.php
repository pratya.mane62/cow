<x-adminlte-layout>
    <x-slot name="title">
        {{ __('Breeding') }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">
            <div class="float-left">
                <h4>ประวัติ{{ __('Breeding') }}</h4>
            </div>
        </div>
        <div class="card-body">
            <div class="card-content">
                <table class=" table_id " style="width:100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>วัวที่ผสม</th>
                            <th>ฟาร์ม</th>
                            <th>น้ำเชื้อ</th>
                            <th>วันที่</th>
                            <th>เวลา</th>
                            <th>กำหนดการคลอด</th>
                            <th>ผลการผสม</th>
                            <th>action</th>
                        </tr>
                    </thead>
                    @isset($breedings)
                        <tbody>
                            @foreach ($breedings as $i => $breeding)
                                @php
                                    $c = App\Models\TransectionCow::where('cow_id', $breeding->cow->id)
                                        ->orderBy('created_at', 'desc')
                                        ->first();
                                @endphp
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $breeding->cow->name }}</td>
                                    <td>{{ $c->farms()->orderBy('created_at', 'desc')->first()->title }}
                                    </td>
                                    <td>
                                        {{ $breeding->sperm->bull . '  --  สายพันธุ์: ' . $breeding->sperm->specie->title . '  --  แหล่งน้ำเชื้อ: ' . $breeding->sperm->resource }}
                                    </td>
                                    <td>{{ $breeding->date }}</td>
                                    <td>{{ $breeding->time }}</td>
                                    <td>{{ date('Y-m-d', strtotime('+200day', strtotime($breeding->date))) }}</td>
                                    <td>
                                        @switch($breeding->status)
                                            @case('pending')
                                                <span class="badge bg-info">กำลังรอผล</span>
                                            @break
                                            @case('success')
                                                <span class="badge bg-success">ติด</span>
                                            @break
                                            @default
                                                <span class="badge bg-danger">ไม่ติด</span>
                                        @endswitch
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.breeding.edit', $breeding) }}"
                                            class=" btn btn-sm btn-warning" style="width: 100px">ผลการผสม</a>
                                        <div class="mt-1"></div>
                                        <a href="{{ route('admin.sons.create', ['cow' => $breeding->cow->id]) }}"
                                            class=" btn btn-sm btn-primary" style="width: 100px">คลอดแล้ว</a>
                                        <div class="mt-1"></div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    @endisset
                </table>
            </div>
        </div>
    </div>
</x-adminlte-layout>
