<x-adminlte-layout>
    <x-slot name="title">
        {{ __('Breeding') }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">
            <div class="float-left">
                <h4>{{ __('Breeding') }}</h4>
            </div>
        </div>
        <div class="card-body">
            <div class="card-content">
                <table class=" table_id " style="width:100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>RFID</th>
                            <th>ชื่อ</th>
                            <th>สายพันธุ์</th>
                            <th>ฟาร์ม</th>
                            <th width="100">action</th>
                        </tr>
                    </thead>
                    @isset($cows)
                        <tbody>
                            @foreach ($cows as $i => $c)
                                <tr>
                                    @php
                                        $cow = App\Models\TransectionCow::where('cow_id', $c->id)
                                            ->orderBy('created_at', 'desc')
                                            ->first();
                                    @endphp
                                    <td>
                                        <img src="{{ isset($cow->photo) ? asset('storage/uploads/' . $cow->photo) : 'https://images.vexels.com/media/users/3/145600/isolated/preview/290e1a100e95214228126e4bda8e3851-cow-avatar-by-vexels.png' }}"
                                            style=" width:100px" alt="{{ $cow->name }}" class=" img-fluid img-thumbnail">
                                    </td>
                                    <td>{{ $c->rfid }} </td>
                                    <td>{{ $c->name }} </td>
                                    <td>{{ $c->specie->title }} </td>
                                    <td>{{ $cow->farms()->first()->title }} </td>
                                    <td width="100">
                                        @if ($c->breedings()->get()->first())
                                            <a href="{{ route('admin.breeding.show', [
                                                'breeding' => $c->breedings()->get()->first()->id,
                                            ]) }}"
                                                class=" btn btn-warning btn-sm" style="width:120px">ประวัติการผสม</a>
                                        @endif
                                        <div class="mb-1"></div>
                                        <a href="{{ route('admin.breeding.create', ['cow' => $c]) }} "
                                            class=" btn btn-info btn-sm" style="width:120px">เพิ่มการผสม</a>
                                        <div class="mb-1"></div>
                                        @if ($c->sons->count() > 0)
                                            <a href="{{ route('admin.sons', ['cow' => $c]) }}"
                                                class=" btn btn-sm btn-success" style="width:120px">ลูกทั้งหมด</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    @endisset
                </table>
            </div>
        </div>
    </div>
</x-adminlte-layout>
