<x-adminlte-layout>
    <x-slot name="title">
        {{ __('Breeding') }}
    </x-slot>
    <div class="row justify-content-center">
        <div class="col-12 col-md-8 col-lg-6">
            <div class="card shadow">
                <div class="card-body">
                    <div class="card-title">
                        <h4>ผล{{ __('Breeding') }} </h4>
                    </div>
                    <div class="card-text row justify-content-center">
                        <table class=" table border-0">
                            <tbody>
                                <tr>
                                    <th>วัวที่ผสม</th>
                                    <td>{{ $breeding->cow->name }}</td>
                                </tr>
                                <tr>
                                    <th>น้ำเชื้อที่ใช้</th>
                                    <td> {{ $breeding->sperm->bull . '  --  สายพันธุ์: ' . $breeding->sperm->specie->title . '  --  แหล่งน้ำเชื้อ: ' . $breeding->sperm->resource }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>เมื่อวันที่</th>
                                    <td>{{ $breeding->date }}</td>
                                </tr>
                                <tr>
                                    <th>เวลา</th>
                                    <td>{{ $breeding->time }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="d-flex">
                            <form action="{{ route('admin.breeding.update', $breeding) }}" method="post">
                                @csrf
                                @method('PUT')

                                <button class="btn btn-success mr-2" name="success" value="1"
                                    style="width: 100px">ติด</button>
                                <button class="btn btn-danger" name="success" value="0"
                                    style="width: 100px">ไม่ติด</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-adminlte-layout>
