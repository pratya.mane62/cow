<x-adminlte-layout>
    <x-slot name="title">
        {{ __('จัดการโรงเชือด') }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">

            <h4> {{ __('จัดการโรงเชือด') }}</h4>
        </div>
        <div class="card-body">
            <div class="card-content">
                <x-auth-validation-errors class="mb-4" :errors="$errors" />
                <form action="{{ route('admin.slaugh.store') }} " method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-xl-6">
                            <label for="photo"> รูป </label>
                            <img id="blah" src="#" alt="รูปภาพของคุณ" class=" img-thumbnail img-fluid w-100" />
                        </div>

                        <div class="col-12 col-xl-6">
                            <label for="photo"> รูป </label>
                            <input type="file" name="photo" value="{{ old('photo', '') }} " class="form-control"
                                id="imgInp">
                            @error('photo')
                                <small class=" text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <input type="hidden" name="type" id="type" value="slaughterhouse">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="standard">มาตรฐานโรงเชือด</label>
                                <textarea name="standard" id="standard" cols="30" rows="2"
                                    class="form-control">{{ old('standard', '') }}</textarea>
                                @error('standard')
                                    <small class=" text-danger">{{ $message }} </small>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="traceback">ข้อมูลสืบย้อนกลับ</label>
                                <textarea name="traceback" id="traceback" cols="30" rows="2"
                                    class="form-control">{{ old('traceback', '') }}</textarea>
                                @error('traceback')
                                    <small class=" text-danger">{{ $message }} </small>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="latlong">พิกัดโรงเชือด</label>
                                <input type="number" step="0.000000001" name="latlong" id=""
                                    value="{{ old('latlong', '') }}" class="form-control" placeholder="" />
                                @error('latlong')
                                    <small class=" text-danger">{{ $message }} </small>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="">ชื่อโรงเชือด</label>
                                <input type="text" name="title" id="farm_title" class="form-control" placeholder=""
                                    value="{{ old('title', '') }}" />
                                @error('title')
                                    <small class=" text-danger">{{ $message }} </small>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="">สถานที่ตั้งโรงเชือด</label>
                                <textarea class="form-control" name="address" id="address"
                                    rows="3">{{ old('address', '') }}</textarea>
                                @error('address')
                                    <small class=" text-danger">{{ $message }} </small>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="district">ตำบล</label>
                                <input type="text" id="district" class=" form-control" name="district">
                            </div>
                            <div class="form-group">
                                <label for="amphoe">อำเภอ</label>
                                <input type="text" id="amphoe" class=" form-control" name="amphoe">
                            </div>
                            <div class="form-group">
                                <label for="province">จังหวัด</label>
                                <input type="text" id="province" class=" form-control" name="province">
                            </div>
                            <div class="form-group">
                                <label for="zipcode">รหัสไปรษณี</label>
                                <input type="text" id="zipcode" class=" form-control" name="zipcode">
                            </div>
                        </div>
                        @can('isAdmin')
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="user_id">เลือกเจ้าของโรงเชือด</label>
                                    <select class="form-control" name="user_id" id="user_id">
                                        @if ($users->count() > 0)
                                            @foreach ($users as $user)
                                                <option value="{{ $user->id }} ">{{ $user->name }} </option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @error('user_id')
                                        <small class=" text-danger">{{ $message }} </small>
                                    @enderror
                                </div>
                            </div>
                        @endcan
                        @can('isUser')
                            <input type="hidden" name="user_id" value="{{ auth()->user()->id }} ">
                        @endcan
                    </div>
                    <div class="form-group mt-4">
                        <button type="submit" class="btn btn-info">เพิ่ม</button>
                        <!-- <a href="to.php" class="btn btn-info">ถัดไป</a> -->
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</x-adminlte-layout>
