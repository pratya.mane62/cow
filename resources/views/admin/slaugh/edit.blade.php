<x-adminlte-layout>
    <x-slot name="title">
        {{ __('จัดการโรงเชือด') }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">

            <h4> {{ __('จัดการโรงเชือด') }}</h4>
        </div>
        <div class="card-body">
            <div class="card-content">
                <x-auth-validation-errors class="mb-4" :errors="$errors" />
                <form action="{{ route('admin.slaugh.update', $slaugh) }} " method="post" enctype="multipart/form-data">
                    @csrf
                    @method("put")
                    <div class="row">
                        <div class="col-12 col-xl-6">
                            <label for="photo"> รูป </label>
                            <img id="blah" src="{{ asset('storage/uploads/' . $slaugh->photo) }}" alt="รูปภาพของคุณ"
                                class=" img-thumbnail img-fluid w-100" />
                        </div>

                        <div class="col-12 col-xl-6">
                            <label for="photo"> รูป </label>
                            <input type="file" name="photo" value="{{ old('photo', $slaugh->photo) }} "
                                class="form-control" id="imgInp">
                            @error('photo')
                                <small class=" text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        @can('isSlaughterhouse')
                            <input type="hidden" name="type" id="type" value="slaughterhouse">
                        @endcan

                        <div class="col-12">
                            <div class="form-group">
                                <label for="standard">มาตรฐานโรงเชือด</label>
                                <textarea name="standard" id="" cols="30" rows="2"
                                    class="form-control">{{ old('standard', $slaugh->standard) }}</textarea>
                                @error('standard')
                                    <small class=" text-danger">{{ $message }} </small>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="traceback">ข้อมูลสืบย้อนกลับ</label>
                                <textarea name="traceback" id="" cols="30" rows="2"
                                    class="form-control">{{ old('traceback', $slaugh->traceback) }}</textarea>
                                @error('traceback')
                                    <small class=" text-danger">{{ $message }} </small>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="latlong">พิกัดโรงเชือด</label>
                                <input type="number" step="0.000000001" name="latlong" id="latlong" class="form-control"
                                    placeholder="" value="{{ old('latlong', $slaugh->latlong) }}" />
                                @error('latlong')
                                    <small class=" text-danger">{{ $message }} </small>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="">ชื่อโรงเชือด</label>
                                <input type="text" name="title" id="farm_title" class="form-control" placeholder=""
                                    value="{{ old('title', $slaugh->title) }}" />
                                @error('title')
                                    <small class=" text-danger">{{ $message }} </small>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="">สถานที่ตั้งโรงเชือด</label>
                                <textarea class="form-control" name="address" id="address"
                                    rows="3">{{ old('address', $slaugh->address) }}</textarea>
                                @error('address')
                                    <small class=" text-danger">{{ $message }} </small>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="district">ตำบล</label>
                                <input type="text" id="district" class=" form-control" value="{{$slaugh->district}}" name="district">
                            </div>
                            <div class="form-group">
                                <label for="amphoe">อำเภอ</label>
                                <input type="text" id="amphoe" class=" form-control" value="{{$slaugh->amphoe}}" name="amphoe">
                            </div>
                            <div class="form-group">
                                <label for="province">จังหวัด</label>
                                <input type="text" id="province" class=" form-control" value="{{$slaugh->province}}" name="province">
                            </div>
                            <div class="form-group">
                                <label for="zipcode">รหัสไปรษณี</label>
                                <input type="text" id="zipcode" class=" form-control" value="{{$slaugh->zipcode}}" name="zipcode">
                            </div>
                        </div>

                        @can('isAdmin')
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="user_id">เลือกเจ้าของโรงเชือด</label>
                                    <select class="form-control" name="user_id" value="" id="user_id">
                                        @if ($users->count() > 0)
                                            @foreach ($users as $user)
                                                <option value="{{ $user->id }} " @if (old('user_id') == $user->id || $slaugh->user_id == $user->id) selected="selected" @endif>
                                                    {{ $user->name }} </option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @error('user_id')
                                        <small class=" text-danger">{{ $message }} </small>
                                    @enderror
                                </div>
                            </div>
                        @endcan
                        @can('isUser')
                            <input type="hidden" name="user_id" value="{{ auth()->user()->id }} ">
                        @endcan
                    </div>
                        <div class="form-group mt-4">
                            <button type="submit" class="btn btn-info">บันทึก</button>
                            <!-- <a href="to.php" class="btn btn-info">ถัดไป</a> -->
                        </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</x-adminlte-layout>
