<x-adminlte-layout>
    <x-slot name="title">
        {{ __('จัดการโรงเชือด') }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">
            <div class="float-left">
                <h4>{{ __('จัดการโรงเชือด') }}</h4>
            </div>
            @can('isAdmin')
                <a href=" {{ route('admin.slaugh.create') }} " class=" btn btn-info float-right">
                    {{ __('AddData') }}
                </a>
            @endcan
        </div>
        <div class="card-body">
            <div class="card-content">
                <table class=" table_id " style="width:100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>ชื่อโรงเชือด</th>
                            @can('isAdmin') <th>เจ้าของโรงเชือด</th> @endcan
                            <th>
                                <center>วัวที่รอเชือด</center>
                            </th>
                            <th>action</th>
                        </tr>
                    </thead>
                    @if ($slaughs->count() > 0)
                        <tbody>
                            @foreach ($slaughs as $i => $slaugh)
                                <tr>
                                    <td align="center">
                                        @isset($slaugh->photo)
                                            <img src="{{ asset('storage/uploads/' . $slaugh->photo) }}" style=" width:100px"
                                                alt="{{ $slaugh->name }}" class=" img-fluid img-thumbnail">
                                        @endisset
                                    </td>
                                    <td>{{ $slaugh->title }} </td>
                                    @can('isAdmin') <td>{{ $slaugh->user->name ?? null }}</td> @endcan
                                    <td align="center">
                                        <a href="{{ route('admin.farm.cows', $slaugh) }}">
                                            {{ $slaugh->cows->count() }}
                                        </a>
                                    </td>
                                    <td width="100">
                                        <a href="{{ route('admin.slaugh.edit', ['slaugh' => $slaugh]) }} "
                                            class=" btn btn-warning btn-sm mb-1"
                                            style="width: 80px">{{ __('Edit') }}</a>
                                        <div class="mx-1"></div>
                                        <form action="{{ route('admin.slaugh.destroy', ['slaugh' => $slaugh]) }} "
                                            method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class=" btn btn-danger btn-sm mb-1"
                                                style="width: 80px"
                                                onclick="return confirm('Are you sure?')">{{ __('Delete') }}</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</x-adminlte-layout>
