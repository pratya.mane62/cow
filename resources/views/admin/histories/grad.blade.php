<x-adminlte-layout>
    <x-slot name="title">
        {{ __('เพิ่มเกรด') }}
    </x-slot>
    <div class="row justify-content-center">
        <div class="col-10 col-md-6 col-md-4">
            <div class="card shadow p-4">
                <div class="card-body">
                    <form action="{{ route('admin.histories.update', $history) }}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="grade">เกรด</label>
                            <input type="text" class="form-control" name="grade" value="{{ $history->grade }}">
                            @error('grade')
                                <small class=" font-weight-bold text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-success">บันทึก</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-adminlte-layout>
