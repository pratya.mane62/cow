<x-adminlte-layout>
    <x-slot name="title">
        {{ __('HistoryCow') }}
    </x-slot>
    <div class="card shadow">
        <div class="card-header">
            <div class="float-left">
                <h4>{{ __('HistoryCow') }}</h4>
            </div>
        </div>
        <div class="card-body">
            <div class="card-content">
                <table class=" table_id " style="width:100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>RFID</th>
                            <th>ชื่อ</th>
                            <th>สายพันธุ์</th>
                            <th>น้ำหนัก</th>
                            <th>วันที่เชือด</th>
                            <th>เกรด</th>
                            <th>โรงเชือด</th>
                            @can('isSlaughterhouse') <th>จัดการ</th> @endcan
                        </tr>
                    </thead>
                    @isset($cows)
                        <tbody>
                            @foreach ($cows as $i => $c)
                                <tr>
                                    @php
                                        $cow = App\Models\TransectionCow::where('cow_id', $c->id)
                                            ->orderBy('created_at', 'desc')
                                            ->first();
                                    @endphp
                                    <td>
                                        <img src="{{ isset($cow->photo) ? asset('storage/uploads/' . $cow->photo) : 'https://images.vexels.com/media/users/3/145600/isolated/preview/290e1a100e95214228126e4bda8e3851-cow-avatar-by-vexels.png' }}"
                                            style=" width:100px" alt="{{ $cow->name }}" class=" img-fluid img-thumbnail">
                                    </td>
                                    <td>{{ $c->rfid }} </td>
                                    <td>{{ $c->name }} </td>
                                    <td>{{ $c->specie->title }} </td>
                                    <td>{{ $cow->weight }} </td>
                                    <td>{{ $c->updated_at }} </td>
                                    <td>{{ $c->grade }} </td>
                                    <td>{{ $cow->farms()->find($cow->farm_id)->title }} </td>
                                    @can('isSlaughterhouse')
                                        <td>
                                            <a href="{{ route('admin.cow.timeline', ['cow' => $c]) }} "
                                                class=" btn btn-success btn-sm mb-1"
                                                style="width:80px">{{ __('ประวัติ') }}</a>
                                            <div class="mx-1"></div>
                                            <a href="{{ route('admin.cow.qrcode', ['cow' => $c]) }} "
                                                class=" btn btn-info btn-sm mb-1 "
                                                style="width: 80px">{{ __('QR Code') }}</a>
                                            <div class="mx-1"></div>
                                            <a href="{{ route('admin.histories.edit', ['history' => $c]) }} "
                                                class=" btn btn-primary btn-sm mb-1" style="width:80px">{{ __('เกรด') }}</a>
                                    </td> @endcan
                                </tr>
                            @endforeach
                        </tbody>
                    @endisset
                </table>
            </div>
        </div>
    </div>
</x-adminlte-layout>
