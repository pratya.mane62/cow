<x-adminlte-layout>
    <x-slot name="title">
        {{ __('Dashboard') }}
    </x-slot>
    <div class="row">
        <div class="col-sm-12">
            <div class="card shadow">
                <div class="card-header">
                    <h5>วัวทั้งหมด</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.dashboard.etpcows', $enterpriseser) }}" method="get">
                        @csrf
                        <div class="card-title d-flex w-100">
                            <button class="btn btn-light rounded-none">
                                <i class="fas fa-sort-amount-down"></i>
                            </button>
                            <div class="mr-1"></div>

                            <select name="choice" id="choice" class=" custom-select" onchange="this.form.submit()">
                                <option value="">-- เลือก --</option>
                                <option value="province" @if (isset($choice) && $choice == 'province') selected @endif>จังหวัด
                                </option>
                                <option value="amphoe" @if (isset($choice) && $choice == 'amphoe') selected @endif>อำเภอ</option>
                                <option value="district" @if (isset($choice) && $choice == 'district') selected @endif>ตำบล</option>
                                <option value="cow_type" @if (isset($choice) && $choice == 'cow_type') selected @endif>ประเภทของวัว
                                </option>
                            </select>
                            <div class="mx-1"></div>
                            <select name="value" id="value" class=" custom-select" onchange="this.form.submit()">
                                <option value="">-- เลือก --</option>
                                @isset($choice)
                                    @switch($choice)
                                        @case('province')
                                            @foreach ($choices as $c)
                                                @if ($c->province != null)
                                                    <option value="{{ $c->province }}" @if (isset($value) && $c->province == $value) selected @endif>
                                                        {{ $c->province }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        @break
                                        @case('amphoe')
                                            @foreach ($choices as $c)
                                                @if ($c->amphoe != null)
                                                    <option value="{{ $c->amphoe }}" @if (isset($value) && $c->amphoe == $value) selected @endif>
                                                        {{ $c->amphoe }} </option>
                                                @endif
                                            @endforeach
                                        @break
                                        @case('district')
                                            @foreach ($choices as $c)
                                                @if ($c->district != null)
                                                    <option value="{{ $c->district }}" @if (isset($value) && $c->district == $value) selected @endif>
                                                        {{ $c->district }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        @break
                                        @case('cow_type')
                                            @foreach ($choices as $c)
                                                @if ($c->type != null)
                                                    <option value="{{ $c->type }}" @if (isset($value) && $c->type == $value) selected @endif>
                                                        @switch($c->type)
                                                            @case('cattle')
                                                                โคขุน
                                                            @break
                                                            @case('mom')
                                                                แม่พันธุ์
                                                            @break
                                                            @case('dad')
                                                                พ่อพันธุ์
                                                            @break
                                                            @default

                                                        @endswitch
                                                    </option>
                                                @endif
                                            @endforeach
                                        @break
                                        @default
                                    @endswitch
                                @endisset
                            </select>
                        </div>
                        <div class="card-body">
                            <div class="card-content">
                                <table class=" table_id " style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>RFID</th>
                                            <th>ชื่อ</th>
                                            <th>สายพันธุ์</th>
                                            <th>น้ำหนัก</th>
                                            <th>ประเภท</th>
                                            <th>ย้ายฟาร์ม</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    @isset($cows)
                                        <tbody>
                                            @foreach ($cows as $i => $c)
                                                <tr>
                                                    @php
                                                        $cow = App\Models\TransectionCow::where('cow_id', $c->id)
                                                            ->orderBy('created_at', 'desc')
                                                            ->first();
                                                    @endphp
                                                    <td>
                                                        <img src="{{ isset($cow->photo) ? asset('storage/uploads/' . $cow->photo) : 'https://images.vexels.com/media/users/3/145600/isolated/preview/290e1a100e95214228126e4bda8e3851-cow-avatar-by-vexels.png' }}"
                                                            style=" width:100px" alt="{{ $cow->name }}"
                                                            class=" img-fluid img-thumbnail">
                                                    </td>
                                                    <td>{{ $c->rfid }} </td>
                                                    <td>{{ $c->name }} </td>
                                                    <td>{{ $c->specie->title }} </td>
                                                    <td>{{ $cow->weight }} </td>
                                                    <td>
                                                        @switch($c->type)
                                                            @case('mom')
                                                                แม่พันธุ์
                                                            @break
                                                            @case('dad')
                                                                พ่อพันธุ์
                                                            @break
                                                            @default
                                                                โคขุน
                                                        @endswitch
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('admin.cows.show', ['cow' => $c]) }}"
                                                            type="button" data-toggle="tooltip" data-placement="bottom"
                                                            title="เปลี่ยนฟาร์ม">
                                                            {{ $c->farm()->first()->title }} <i class=" fas fa-edit"></i>
                                                        </a>
                                                    </td>
                                                    <td width="100">
                                                        <a href="{{ route('admin.cow.timeline', ['cow' => $c]) }} "
                                                            class=" btn btn-success btn-sm mb-1"
                                                            style="width:80px">{{ __('ประวัติ') }}</a>
                                                        <div class="mx-1"></div>
                                                        <a href="{{ route('admin.cows.edit', ['cow' => $c]) }} "
                                                            class=" btn btn-warning btn-sm mb-1"
                                                            style="width:80px">{{ __('Edit') }}</a>
                                                        <div class="mx-1"></div>
                                                        <form action="{{ route('admin.cows.destroy', ['cow' => $c]) }} "
                                                            method="post">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class=" btn btn-danger btn-sm mb-1"
                                                                style="width:80px"
                                                                onclick="return confirm('Are you sure?')">{{ __('Delete') }}</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    @endisset
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-adminlte-layout>
