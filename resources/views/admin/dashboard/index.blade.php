<x-adminlte-layout>
    <x-slot name="title">
        {{ __('Dashboard') }}
    </x-slot>
    <div class="row">
        @can('isFarm')
            <div class="col-12 col-lg">
                <div class="row">
                    <div class="col-6">
                        <div class="card shadow">
                            <div class="card-body">
                                <h6 class="card-subtitle mb-2 text-muted text-sm">สรุปข้อมูลฟาร์ม</h6>
                                <h4 class=" font-weight-bold">ฟาร์มทั้งหมด {{ $farmAlls->count() }} ฟาร์ม</h4>
                                <div class=" card-text row px-2">
                                    <div class="col-6">
                                        <b>แยกตามประเภท</b>
                                        <form action="{{ route('admin.dashboard.index') }}" method="get">
                                            @csrf
                                            <ul>
                                                <li>
                                                    <input class="form-check-input" type="checkbox" id="defaultCheck1"
                                                        name="headwaters" @if (isset($headwaters) && $headwaters == 1) checked @endif onchange="this.form.submit()">
                                                    <label class="form-check-label" for="defaultCheck1">
                                                        ต้นน้ำ
                                                    </label>
                                                </li>
                                                <li>
                                                    <input class="form-check-input" type="checkbox" id="defaultCheck2"
                                                        name="middle" @if (isset($middle) && $middle == 1) checked @endif onchange="this.form.submit()">
                                                    <label class="form-check-label" for="defaultCheck2">
                                                        กลางน้ำ
                                                    </label>
                                                </li>
                                                <li>
                                                    <input class="form-check-input" type="checkbox" id="defaultCheck3"
                                                        name="downstream" @if (isset($downstream) && $downstream == 1) checked @endif onchange="this.form.submit()">
                                                    <label class="form-check-label" for="defaultCheck3">
                                                        ปลายน้ำ
                                                    </label>
                                                </li>
                                            </ul>
                                        </form>
                                    </div>
                                    <div class="col-6 text-center">
                                        <h1>{{ $farms->count() }}</h1>
                                        <h6>ฟาร์ม</h6>
                                    </div>
                                </div>
                                <p class=" card-text text-right">
                                    <a href="{{ route('admin.dashboard.farms') }}">รายละเอียดเพิ่มเติม <i
                                            class="fas fa-long-arrow-alt-right"></i></a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <form action="{{ route('admin.dashboard.index') }}" method="get">
                            @csrf
                            <div class="card shadow">
                                <div class="card-body">
                                    <h6 class="card-subtitle mb-2 text-muted text-sm">สรุปข้อมูลวัว</h6>
                                    <h4 class=" font-weight-bold">วัวทั้งหมด {{ $cowAlls->count() }} ตัว</h4>
                                    <div class=" card-text row px-2">
                                        <div class="col-6">
                                            <b>แยกตามประเภท</b>
                                            <ul>
                                                <li>
                                                    <input class="form-check-input" type="radio" id="dad" name="type"
                                                        onclick="this.form.submit()" value="dad" @if (isset($cow_type) && $cow_type == 'dad') checked @endif>
                                                    <label class="form-check-label" for="dad">
                                                        พ่อพันธุ์
                                                    </label>
                                                </li>
                                                <li>
                                                    <input class="form-check-input" type="radio" id="mom" name="type"
                                                        onclick="this.form.submit()" value="mom" @if (isset($cow_type) && $cow_type == 'mom') checked @endif>
                                                    <label class="form-check-label" for="mom">
                                                        แม่พันธุ์
                                                    </label>
                                                </li>
                                                <li>
                                                    <input class="form-check-input" type="radio" id="cattle" name="type"
                                                        onclick="this.form.submit()" value="cattle" @if (isset($cow_type) && $cow_type == 'cattle') checked @endif>
                                                    <label class="form-check-label" for="cattle">
                                                        โคขุน
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-6 text-center">
                                            <h1>{{ $cows->count() }}</h1>
                                            <h6>ตัว</h6>
                                        </div>
                                    </div>
                                    <div class=" card-text d-flex justify-content-between">
                                        <select name="specie_id" id="specie_id" onchange="this.form.submit()">
                                            <option disabled selected>เลือกสายพันธุ์</option>
                                            @foreach ($species as $specie)
                                                <option value="{{ $specie->id }}" @if (isset($specie_id) && $specie_id == $specie->id) selected @endif>
                                                    {{ $specie->title }} </option>
                                            @endforeach
                                        </select>
                                        <a href="{{ route('admin.dashboard.cows') }}">รายละเอียดเพิ่มเติม <i
                                                class="fas fa-long-arrow-alt-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    @can('isAdmin')
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <div class="card shadow">
                                        <div class="card-body">
                                            <h6 class="card-subtitle mb-2 text-muted text-sm">สรุปข้อมูลผู้ใช้</h6>
                                            <h4 class=" font-weight-bold">ผู้ใช้ทั้งหมด {{ $users->sum('total') }} ผู้ใช้
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                @foreach ($users as $user)
                                    <div class="col-6 col-lg-4">
                                        <div class="shadow card card-primary text-light">
                                            <div class="card-body text-center">
                                                @switch($user->status)
                                                    @case('admin')
                                                        <h5>ผู้ดูแลระบบ</h5>
                                                    @break
                                                    @case('farm')
                                                        <h5>เจ้าของฟาร์ม</h5>
                                                    @break
                                                    @case('slaugh')
                                                        <h5>ผู้ดูแลโรงเชือด</h5>
                                                    @break
                                                    @default
                                                @endswitch
                                                <b>
                                                    {{ $user->total }}
                                                </b>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endcan
                    @can('isEtp')
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <div class="card shadow">
                                        <div class="card-body">
                                            <h6 class="card-subtitle mb-2 text-muted text-sm">สรุปข้อมูลวิสาหกิจ</h6>
                                            <h4 class=" font-weight-bold">วิสาหกิจทั้งหมด {{ $etps->count() }} วิสาหกิจ</h4>
                                        </div>
                                    </div>
                                </div>
                                @foreach ($etps as $etp)
                                    @php
                                        $etp_cows = App\Models\Cow::select('cows.*')
                                            ->join('farms', 'cows.farm_id', 'farms.id')
                                            ->join('enterprisesers', 'farms.enterpriseser_id', 'enterprisesers.id')
                                            ->where('enterprisesers.id', $etp->id)
                                            ->where('cows.status', 'alive')
                                            ->get();
                                        $etp_cow_alls = App\Models\Cow::select('cows.*')
                                            ->join('farms', 'cows.farm_id', 'farms.id')
                                            ->join('enterprisesers', 'farms.enterpriseser_id', 'enterprisesers.id')
                                            ->where('cows.specie_id', !empty($specie_id) ? '=' : '<>', !empty($specie_id) ? $specie_id : null)
                                            ->where('cows.type', $cow_type)
                                            ->where('enterprisesers.id', $etp->id)
                                            ->where('cows.status', 'alive')
                                            ->get();
                                        if (!$headwaters && !$middle && !$downstream) {
                                            $farms_etp = $etp
                                                ->farms()
                                                ->where('headwaters', $headwaters)
                                                ->where('middle', $middle)
                                                ->where('downstream', $downstream)
                                                ->get();
                                        } else {
                                            $farms_etp = $etp
                                                ->farms()
                                                ->where('headwaters', $headwaters ? '=' : '<>', $headwaters ? $headwaters : null)
                                                ->where('middle', $middle ? '=' : '<>', $middle ? $middle : null)
                                                ->where('downstream', $downstream ? '=' : '<>', $downstream ? $downstream : null);
                                        }
                                    @endphp
                                    <div class="col-12 col-lg-6">
                                        <div class="shadow card">
                                            <div class="card-body">
                                                <b class="text-lg text-truncate w-100 d-block mb-2">{{ $etp->name }}</b>
                                                <div class="row px-2 ">
                                                    <div class="col-lg">
                                                        <b>ฟาร์มทั้งหมด {{ $etp->farms()->count() }} ฟาร์ม</b>
                                                        @can('isUser')
                                                            <div class=" card-text row px-2">
                                                                <div class="col-6">
                                                                    <b>แยกตามประเภท</b>
                                                                    <form action="{{ route('admin.dashboard.index') }}"
                                                                        method="get">
                                                                        @csrf
                                                                        <ul>
                                                                            <li>
                                                                                <input class="form-check-input" type="checkbox"
                                                                                    id="defaultCheck1" name="headwaters" @if (isset($headwaters) && $headwaters == 1) checked @endif
                                                                                    onchange="this.form.submit()">
                                                                                <label class="form-check-label" for="defaultCheck1">
                                                                                    ต้นน้ำ
                                                                                </label>
                                                                            </li>
                                                                            <li>
                                                                                <input class="form-check-input" type="checkbox"
                                                                                    id="defaultCheck2" name="middle" @if (isset($middle) && $middle == 1) checked @endif
                                                                                    onchange="this.form.submit()">
                                                                                <label class="form-check-label" for="defaultCheck2">
                                                                                    กลางน้ำ
                                                                                </label>
                                                                            </li>
                                                                            <li>
                                                                                <input class="form-check-input" type="checkbox"
                                                                                    id="defaultCheck3" name="downstream" @if (isset($downstream) && $downstream == 1) checked @endif
                                                                                    onchange="this.form.submit()">
                                                                                <label class="form-check-label" for="defaultCheck3">
                                                                                    ปลายน้ำ
                                                                                </label>
                                                                            </li>
                                                                        </ul>
                                                                    </form>
                                                                </div>
                                                                <div class="col-6 text-center pt-3">
                                                                    <h1>
                                                                        {{ $farms_etp->count() }}
                                                                    </h1>
                                                                    <h6>ฟาร์ม</h6>
                                                                </div>
                                                            </div>
                                                            <p class=" card-text text-right">
                                                                <a
                                                                    href="{{ route('admin.dashboard.etpfarms', ['enterpriseser' => $etp]) }}">รายละเอียดเพิ่มเติม
                                                                    <i class="fas fa-long-arrow-alt-right"></i></a>
                                                            </p>
                                                        @endcan
                                                    </div>
                                                    <div class="col-lg-1">
                                                    </div>
                                                    <div class="col-lg">
                                                        <form action="{{ route('admin.dashboard.index') }}" method="get">
                                                            @csrf
                                                            <b>วัวทั้งหมด {{ $etp_cows->count() }} ตัว </b>
                                                            @can('isUser')
                                                                <div class=" card-text row px-2">
                                                                    <div class="col-6">
                                                                        <b>แยกตามประเภท</b>
                                                                        <ul>
                                                                            <li>
                                                                                <input class="form-check-input" type="radio"
                                                                                    id="dad" name="type"
                                                                                    onclick="this.form.submit()" value="dad" @if (isset($cow_type) && $cow_type == 'dad') checked @endif>
                                                                                <label class="form-check-label" for="dad">
                                                                                    พ่อพันธุ์
                                                                                </label>
                                                                            </li>
                                                                            <li>
                                                                                <input class="form-check-input" type="radio"
                                                                                    id="mom" name="type"
                                                                                    onclick="this.form.submit()" value="mom" @if (isset($cow_type) && $cow_type == 'mom') checked @endif>
                                                                                <label class="form-check-label" for="mom">
                                                                                    แม่พันธุ์
                                                                                </label>
                                                                            </li>
                                                                            <li>
                                                                                <input class="form-check-input" type="radio"
                                                                                    id="cattle" name="type"
                                                                                    onclick="this.form.submit()" value="cattle" @if (isset($cow_type) && $cow_type == 'cattle') checked @endif>
                                                                                <label class="form-check-label" for="cattle">
                                                                                    โคขุน
                                                                                </label>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-6 text-center">
                                                                        <h1>{{ $etp_cow_alls->count() }}</h1>
                                                                        <h6>ตัว</h6>
                                                                    </div>
                                                                </div>
                                                                <div class=" card-text d-flex justify-content-between">
                                                                    <select name="specie_id" id="specie_id"
                                                                        onchange="this.form.submit()">
                                                                        <option disabled selected>เลือกสายพันธุ์</option>
                                                                        @foreach ($species as $specie)
                                                                            <option value="{{ $specie->id }}" @if (isset($specie_id) && $specie_id == $specie->id) selected @endif>
                                                                                {{ $specie->title }} </option>
                                                                        @endforeach
                                                                    </select>
                                                                    <a
                                                                        href="{{ route('admin.dashboard.etpcows', ['enterpriseser' => $etp]) }}">รายละเอียดเพิ่มเติม
                                                                        <i class="fas fa-long-arrow-alt-right"></i></a>

                                                                </div>
                                                            @endcan
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endcan
                </div>
            </div>
        @endcan
        @can('isSlaughterhouse')
            <div class="col-12 col-lg-4">
                <div class="row">
                    <div class="col-12">
                        <div class="card shadow">
                            <div class="card-body">
                                <h6 class="card-subtitle mb-2 text-muted text-sm">สรุปข้อมูลโรงเชือด</h6>
                                <h4 class=" font-weight-bold">โรงเชือดทั้งหมด {{ $slaugs->count() }} โรงเชือด</h4>
                            </div>
                        </div>
                    </div>
                    @foreach ($slaugs as $slaug)
                        <div class="col-6">
                            <div class="card shadow">
                                <div class="card-body">
                                    <h3 class="card-title mb-2 font-weight-bold d-block text-truncate w-100">
                                        {{ $slaug->title }}</h3><br>
                                    <div class="row card-text">
                                        <div class="col-9 pt-3">
                                            <h6 class=" card-subtitle ml-2 text-muted text-sm">วัวที่รอเชือดทั้งหมด </h6>
                                        </div>
                                        <div class="col-2">
                                            <h3 class=" card-title text-center"><b>{{ $slaugs->count() }}</b> <br> ตัว
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endcan
    </div>
</x-adminlte-layout>
