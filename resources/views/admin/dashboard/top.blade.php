<x-adminlte-layout>
    <x-slot name="title">
        {{ __('Dashboard') }}
    </x-slot>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card shadow">
                        <div class="card-body">
                            <h6 class="card-subtitle mb-2 text-muted text-sm">สรุปข้อมูลฟาร์ม</h6>
                            <h3 class=" card-title font-weight-bold text-lg">ฟาร์มทั้งหมด 10 ฟาร์ม</h3>
                        </div>
                    </div>
                    <div class="card shadow">
                        <div class="card-body">
                            <p class=" card-text">
                                <b>แยกตามประเภท</b>
                                <canvas id="farmChart"></canvas>
                            </p>
                            {{-- <p class=" card-text text-right">
                    <a href="">รายละเอียดเพิ่มเติม <i class="fas fa-long-arrow-alt-right"></i></a>
                </p> --}}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card shadow">
                        <div class="card-body">
                            <h6 class="card-subtitle mb-2 text-muted text-sm">สรุปข้อมูลวัว</h6>
                            <h3 class=" card-title font-weight-bold text-lg">วัวทั้งหมด 10 ตัว</h3>
                        </div>
                    </div>
                    <div class="card shadow">
                        <div class="card-body">
                            <p class=" card-text">
                                <b>แยกตามประเภท</b>
                                <canvas id="cowChart"></canvas>
                            </p>
                            {{-- <p class=" card-text text-right">
                    <a href="">รายละเอียดเพิ่มเติม <i class="fas fa-long-arrow-alt-right"></i></a>
                </p> --}}
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="card shadow">
                        <div class="card-body">
                            <h6 class="card-subtitle mb-2 text-muted text-sm">สรุปข้อมูลโรงเชือด</h6>
                            <h3 class=" card-title font-weight-bold text-lg">โรงเชือดทั้งหมด 10 โรงเชือดท</h3>
                            <p class=" card-text">
                                <canvas id="killChart"></canvas>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">

            <div class="card shadow">
                <div class="card-header">
                    <h5>ฟาร์มทั้งหมด</h5>
                </div>
                <div class="card-body">
                    <div class="card-title d-flex w-100">
                        <button class="btn btn-light rounded-none">
                            <i class="fas fa-sort-amount-down"></i>
                        </button>
                        <div class="mr-1"></div>
                        <select name="filter01" id="filter01" class=" custom-select" onchange="location = this.value;">
                            <option value="">-- เลือก --</option>
                            @can('isAdmin')
                                <option value="{{ route('admin.select.choice', ['choice' => 'etp']) }}" @if (isset($choice) && $choice == 'etp') selected @endif>
                                    กลุ่มวิสาหกิจ</option>
                            @endcan
                            <option value="{{ route('admin.select.choice', ['choice' => 'province']) }}" @if (isset($choice) && $choice == 'province') selected @endif>จังหวัด
                            </option>
                            <option value="{{ route('admin.select.choice', ['choice' => 'amphoe']) }}" @if (isset($choice) && $choice == 'amphoe') selected @endif>อำเภอ</option>
                            <option value="{{ route('admin.select.choice', ['choice' => 'district']) }}" @if (isset($choice) && $choice == 'district') selected @endif>ตำบล</option>
                            <option value="{{ route('admin.select.choice', ['choice' => 'cow']) }}" @if (isset($choice) && $choice == 'cow') selected @endif>
                                จำนวนวัวที่มี
                            </option>
                            <option value="{{ route('admin.select.choice', ['choice' => 'cow_type']) }}" @if (isset($choice) && $choice == 'cow_type') selected @endif>ประเภทของวัว
                            </option>
                        </select>
                        <div class="mx-1"></div>
                        <select name="filter02" id="filter02" class=" custom-select" onchange="location = this.value;">
                            <option value="">-- เลือก --</option>
                            @isset($choice)
                                @switch($choice)
                                    @case('etp')
                                        @foreach ($choices as $etp)
                                            @if ($etp->enterpriseser_id)
                                                <option
                                                    value="{{ route('admin.select.filter', ['value' => $etp->enterpriseser_id, $choice]) }}"
                                                    @if (isset($value) && $etp->enterpriseser_id == $value) selected @endif>
                                                    {{ $etp->etp->name }}
                                                </option>
                                            @endif
                                        @endforeach
                                    @break
                                    @case('province')
                                        @foreach ($choices as $c)
                                            @if ($c->province != null)
                                                <option
                                                    value="{{ route('admin.select.filter', ['value' => $c->province, $choice]) }}"
                                                    @if (isset($value) && $c->province == $value) selected @endif>
                                                    {{ $c->province }}
                                                </option>
                                            @endif
                                        @endforeach
                                    @break
                                    @case('amphoe')
                                        @foreach ($choices as $c)
                                            @if ($c->amphoe != null)
                                                <option
                                                    value="{{ route('admin.select.filter', ['value' => $c->amphoe, $choice]) }}"
                                                    @if (isset($value) && $c->amphoe == $value) selected @endif>
                                                    {{ $c->amphoe }} << {{ $c->province }} </option>
                                            @endif
                                        @endforeach
                                    @break
                                    @case('district')
                                        @foreach ($choices as $c)
                                            @if ($c->district != null)
                                                <option
                                                    value="{{ route('admin.select.filter', ['value' => $c->district, $choice]) }}"
                                                    @if (isset($value) && $c->district == $value) selected @endif>
                                                    {{ $c->district }} << {{ $c->amphoe }} << {{ $c->province }}
                                                        </option>
                                            @endif
                                        @endforeach
                                    @break
                                    @case('cow')
                                        @foreach ($choices as $key => $cow)
                                            <option value="{{ route('admin.select.filter', ['value' => $key, $choice]) }}"
                                                @if (isset($value) && $key == $value) selected @endif>
                                                {{ $cow[0] }} - {{ $cow[1] }}
                                            </option>
                                        @endforeach
                                    @break
                                    @case('cow_type')
                                        @foreach ($choices as $c)
                                            @if ($c->type != null)
                                                <option
                                                    value="{{ route('admin.select.filter', ['value' => $c->type, $choice]) }}"
                                                    @if (isset($value) && $c->type == $value) selected @endif>
                                                    @switch($c->type)
                                                        @case('cattle')
                                                            โคขุน
                                                        @break
                                                        @case('mom')
                                                            แม่พันธุ์
                                                        @break
                                                        @case('dad')
                                                            พ่อพันธุ์
                                                        @break
                                                        @default

                                                    @endswitch
                                                </option>
                                            @endif
                                        @endforeach
                                    @break
                                    @default
                                @endswitch
                            @endisset
                        </select>
                    </div>
                    <div class="card-content">
                        <table class=" table_id " style="width:100%;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ชื่อฟาร์ม</th>
                                    @can('isAdmin') <th>เจ้าของฟาร์ม</th> @endcan
                                    <th>กลุ่มวิสาหกิจที่สังกัด</th>
                                    <th>
                                        <center>วัวทั้งหมด</center>
                                    </th>
                                    <th>ประเภท</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            @if ($farms->count() > 0)
                                <tbody>
                                    @foreach ($farms as $i => $farm)
                                        <tr>
                                            <td align="center">
                                                @isset($farm->photo)
                                                    <img src="{{ asset('storage/uploads/' . $farm->photo) }}"
                                                        style=" width:100px" alt="{{ $farm->name }}"
                                                        class=" img-fluid img-thumbnail">
                                                @endisset
                                            </td>
                                            <td>{{ $farm->title }} </td>
                                            @can('isAdmin') <td>{{ $farm->user->name ?? null }}</td> @endcan
                                            <td>
                                                <pre>{{ $farm->etp()->first()->name ?? null }}</pre>
                                            </td>
                                            <td align="center">
                                                <a href="{{ route('admin.farm.cows', $farm) }}">
                                                    {{ $farm->cows->count() }}
                                                </a>
                                            </td>
                                            <td>{{ $farm->type == 'farm' ? 'ฟาร์ม' : 'โรงเชือด' }}</td>
                                            <td width="100">
                                                <a href="{{ route('admin.farms.edit', ['farm' => $farm]) }} "
                                                    class=" btn btn-warning btn-sm mb-1"
                                                    style="width: 80px">{{ __('Edit') }}</a>
                                                <div class="mx-1"></div>
                                                <form action="{{ route('admin.farms.destroy', ['farm' => $farm]) }} "
                                                    method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class=" btn btn-danger btn-sm mb-1"
                                                        style="width: 80px"
                                                        onclick="return confirm('Are you sure?')">{{ __('Delete') }}</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
            <div class="card shadow">
                <div class="card-header">
                    <h5>วัวทั้งหมด</h5>
                </div>
                <div class="card-body">
                    <div class="card-title d-flex w-100">
                        <button class="btn btn-light rounded-none">
                            <i class="fas fa-sort-amount-down"></i>
                        </button>
                        <div class="mr-1"></div>

                        <select name="filter01" id="filter01" class=" custom-select" onchange="location = this.value;">
                            <option value="">-- เลือก --</option>
                            <option value="{{ route('admin.select.choice', ['choice' => 'province']) }}" @if (isset($choice) && $choice == 'province') selected @endif>จังหวัด
                            </option>
                            <option value="{{ route('admin.select.choice', ['choice' => 'amphoe']) }}" @if (isset($choice) && $choice == 'amphoe') selected @endif>อำเภอ</option>
                            <option value="{{ route('admin.select.choice', ['choice' => 'district']) }}" @if (isset($choice) && $choice == 'district') selected @endif>ตำบล</option>
                            <option value="{{ route('admin.select.choice', ['choice' => 'cow_type']) }}" @if (isset($choice) && $choice == 'cow_type') selected @endif>ประเภทของวัว
                            </option>
                        </select>
                        <div class="mx-1"></div>
                        <select name="filter02" id="filter02" class=" custom-select" onchange="location = this.value;">
                            <option value="">-- เลือก --</option>
                            @isset($choice)
                                @switch($choice)
                                    @case('province')
                                        @foreach ($choices as $c)
                                            @if ($c->province != null)
                                                <option
                                                    value="{{ route('admin.select.filter', ['value' => $c->province, $choice]) }}"
                                                    @if (isset($value) && $c->province == $value) selected @endif>
                                                    {{ $c->province }}
                                                </option>
                                            @endif
                                        @endforeach
                                    @break
                                    @case('amphoe')
                                        @foreach ($choices as $c)
                                            @if ($c->amphoe != null)
                                                <option
                                                    value="{{ route('admin.select.filter', ['value' => $c->amphoe, $choice]) }}"
                                                    @if (isset($value) && $c->amphoe == $value) selected @endif>
                                                    {{ $c->amphoe }} << {{ $c->province }} </option>
                                            @endif
                                        @endforeach
                                    @break
                                    @case('district')
                                        @foreach ($choices as $c)
                                            @if ($c->district != null)
                                                <option
                                                    value="{{ route('admin.select.filter', ['value' => $c->district, $choice]) }}"
                                                    @if (isset($value) && $c->district == $value) selected @endif>
                                                    {{ $c->district }} << {{ $c->amphoe }} << {{ $c->province }}
                                                        </option>
                                            @endif
                                        @endforeach
                                    @break
                                    @case('cow_type')
                                        @foreach ($choices as $c)
                                            @if ($c->type != null)
                                                <option
                                                    value="{{ route('admin.select.filter', ['value' => $c->type, $choice]) }}"
                                                    @if (isset($value) && $c->type == $value) selected @endif>
                                                    @switch($c->type)
                                                        @case('cattle')
                                                            โคขุน
                                                        @break
                                                        @case('mom')
                                                            แม่พันธุ์
                                                        @break
                                                        @case('dad')
                                                            พ่อพันธุ์
                                                        @break
                                                        @default

                                                    @endswitch
                                                </option>
                                            @endif
                                        @endforeach
                                    @break
                                    @default
                                @endswitch
                            @endisset
                        </select>
                    </div>
                    <div class="card-body">
                        <div class="card-content">
                            <table class=" table_id " style="width:100%;">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>RFID</th>
                                        <th>ชื่อ</th>
                                        <th>สายพันธุ์</th>
                                        <th>น้ำหนัก</th>
                                        <th>ประเภท</th>
                                        <th>ย้ายฟาร์ม</th>
                                        <th>action</th>
                                    </tr>
                                </thead>
                                @isset($cows)
                                    <tbody>
                                        @foreach ($cows as $i => $c)
                                            <tr>
                                                @php
                                                    $cow = App\Models\TransectionCow::where('cow_id', $c->id)
                                                        ->orderBy('created_at', 'desc')
                                                        ->first();
                                                @endphp
                                                <td>
                                                    <img src="{{ isset($cow->photo) ? asset('storage/uploads/' . $cow->photo) : 'https://images.vexels.com/media/users/3/145600/isolated/preview/290e1a100e95214228126e4bda8e3851-cow-avatar-by-vexels.png' }}"
                                                        style=" width:100px" alt="{{ $cow->name }}"
                                                        class=" img-fluid img-thumbnail">
                                                </td>
                                                <td>{{ $c->rfid }} </td>
                                                <td>{{ $c->name }} </td>
                                                <td>{{ $c->specie->title }} </td>
                                                <td>{{ $cow->weight }} </td>
                                                <td>
                                                    @switch($c->type)
                                                        @case('mom')
                                                            แม่พันธุ์
                                                        @break
                                                        @case('dad')
                                                            พ่อพันธุ์
                                                        @break
                                                        @default
                                                            โคขุน
                                                    @endswitch
                                                </td>
                                                <td>
                                                    <a href="{{ route('admin.cows.show', ['cow' => $c]) }}" type="button"
                                                        data-toggle="tooltip" data-placement="bottom" title="เปลี่ยนฟาร์ม">
                                                        {{ $c->farm()->first()->title }} <i class=" fas fa-edit"></i>
                                                    </a>
                                                </td>
                                                <td width="100">
                                                    <a href="{{ route('admin.cow.timeline', ['cow' => $c]) }} "
                                                        class=" btn btn-success btn-sm mb-1"
                                                        style="width:80px">{{ __('ประวัติ') }}</a>
                                                    <div class="mx-1"></div>
                                                    <a href="{{ route('admin.cows.edit', ['cow' => $c]) }} "
                                                        class=" btn btn-warning btn-sm mb-1"
                                                        style="width:80px">{{ __('Edit') }}</a>
                                                    <div class="mx-1"></div>
                                                    <form action="{{ route('admin.cows.destroy', ['cow' => $c]) }} "
                                                        method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class=" btn btn-danger btn-sm mb-1"
                                                            style="width:80px"
                                                            onclick="return confirm('Are you sure?')">{{ __('Delete') }}</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                @endisset
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @section('scripts')
        <script script script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        <script>
            const farm = document.getElementById('farmChart');
            const cow = document.getElementById('cowChart');
            const kill = document.getElementById('killChart');
            const dataFarm = {
                labels: [
                    'ต้นน้ำ',
                    'ต้นน้ำ กลางน้ำ',
                    'ต้นน้ำ ปลายน้ำ',
                    'ปลายน้ำ กลางน้ำ',
                    'ต้นน้ำ กลางน้ำ ปลายน้ำ'
                ],
                datasets: [{
                    label: 'My First Dataset',
                    data: [30, 50, 40,30,20],
                    backgroundColor: [
                        'rgb(255, 99, 132)',
                        'rgb(54, 162, 235)',
                        'rgb(255, 205, 86)',
                        'rgb(255, 25, 86)',
                        'rgb(255, 25, 860)'
                    ],
                    hoverOffset: 4
                }]
            };
            const farmChart = new Chart(farm, {
                type: 'doughnut',
                data: dataFarm,
                options: {}
            });

            const dataCow = {
                labels: [
                    'พ่อพันธุ์',
                    'แม่พันธ์',
                    'โคขุน'
                ],
                datasets: [{
                    label: 'My First Dataset',
                    data: [30, 50, 45],
                    backgroundColor: [
                        'rgb(255, 99, 132)',
                        'rgb(54, 162, 235)',
                        'rgb(255, 205, 86)'
                    ],
                    hoverOffset: 4
                }]
            };
            const cowChart = new Chart(cow, {
                type: 'polarArea',
                data: dataCow,
                options: {}
            });

            const dataKill = {
                labels: [
                    'โรงเชือด 1',
                    'โรงเชือด 2',
                    'โรงเชือด 3'
                ],
                datasets: [{
                    label: 'วัวที่รอเชือด',
                    data: [30, 50, 45],
                    backgroundColor: [
                        'rgb(255, 99, 132)',
                        'rgb(54, 162, 235)',
                        'rgb(255, 205, 86)'
                    ],
                    hoverOffset: 4
                }]
            };
            const killChart = new Chart(kill, {
                type: 'bar',
                data: dataKill,
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                },
            });

        </script>
    @endsection
</x-adminlte-layout>
