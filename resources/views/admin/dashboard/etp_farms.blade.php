<x-adminlte-layout>
    <x-slot name="title">
        {{ __('Dashboard') }}
    </x-slot>
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header">
                    <h5>ฟาร์มทั้งหมด</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.dashboard.etpfarms',$enterpriseser) }}" method="get">
                        @csrf
                        <div class="card-title d-flex w-100">
                            <button class="btn btn-light rounded-none">
                                <i class="fas fa-sort-amount-down"></i>
                            </button>
                            <div class="mr-1"></div>
                            <select name="choice" id="choice1" class=" custom-select" onchange="this.form.submit()">
                                <option disabled selected>-- เลือก --</option>
                                @can('isAdmin')
                                    <option value="etp" @if (isset($choice) && $choice == 'etp') selected @endif>
                                        กลุ่มวิสาหกิจ</option>
                                @endcan
                                <option value="province" @if (isset($choice) && $choice == 'province') selected @endif>จังหวัด
                                </option>
                                <option value="amphoe" @if (isset($choice) && $choice == 'amphoe') selected @endif>อำเภอ</option>
                                <option value="district" @if (isset($choice) && $choice == 'district') selected @endif>ตำบล</option>
                                <option value="cow" @if (isset($choice) && $choice == 'cow') selected @endif>
                                    จำนวนวัวที่มี
                                </option>
                                <option value="cow_type" @if (isset($choice) && $choice == 'cow_type') selected @endif>ประเภทของวัว
                                </option>
                            </select>
                            <div class="mx-1"></div>
                            <select name="value" id="value" class=" custom-select" onchange="this.form.submit()">
                                <option value="">-- เลือก --</option>
                                @isset($choice)
                                    @switch($choice)
                                        @case('etp')
                                            @foreach ($choices as $etp)
                                                @if ($etp->enterpriseser_id)
                                                    <option value="{{ $etp->enterpriseser_id }}" @if (isset($value) && $etp->enterpriseser_id == $value) selected @endif>
                                                        {{ $etp->etp->name }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        @break
                                        @case('province')
                                            @foreach ($choices as $c)
                                                @if ($c->province != null)
                                                    <option value="{{ $c->province }}" @if (isset($value) && $c->province == $value) selected @endif>
                                                        {{ $c->province }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        @break
                                        @case('amphoe')
                                            @foreach ($choices as $c)
                                                @if ($c->amphoe != null)
                                                    <option value="{{ $c->amphoe }}" @if (isset($value) && $c->amphoe == $value) selected @endif>
                                                        {{ $c->amphoe }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        @break
                                        @case('district')
                                            @foreach ($choices as $c)
                                                @if ($c->district != null)
                                                    <option value="{{ $c->district }}" @if (isset($value) && $c->district == $value) selected @endif>
                                                        {{ $c->district }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        @break
                                        @case('cow')
                                            @foreach ($choices as $key => $cow)
                                                <option value="{{ $key }}" @if (isset($value) && $key == $value) selected @endif>
                                                    {{ $cow[0] }} - {{ $cow[1] }}
                                                </option>
                                            @endforeach
                                        @break
                                        @case('cow_type')
                                            @foreach ($choices as $c)
                                                @if ($c->type != null)
                                                    <option value="{{ $c->type }}" @if (isset($value) && $c->type == $value) selected @endif>
                                                        @switch($c->type)
                                                            @case('cattle')
                                                                โคขุน
                                                            @break
                                                            @case('mom')
                                                                แม่พันธุ์
                                                            @break
                                                            @case('dad')
                                                                พ่อพันธุ์
                                                            @break
                                                            @default
                                                        @endswitch
                                                    </option>
                                                @endif
                                            @endforeach
                                        @break
                                        @default
                                    @endswitch
                                @endisset
                            </select>
                        </div>
                    </form>
                    <div class="card-content">
                        <table class=" table_id " style="width:100%;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ชื่อฟาร์ม</th>
                                    @can('isAdmin') <th>เจ้าของฟาร์ม</th> @endcan
                                    <th>กลุ่มวิสาหกิจที่สังกัด</th>
                                    <th>
                                        <center>วัวทั้งหมด</center>
                                    </th>
                                    <th>ประเภท</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            @if ($farms->count() > 0)
                                <tbody>
                                    @foreach ($farms as $i => $farm)
                                        <tr>
                                            <td align="center">
                                                @isset($farm->photo)
                                                    <img src="{{ asset('storage/uploads/' . $farm->photo) }}"
                                                        style=" width:100px" alt="{{ $farm->name }}"
                                                        class=" img-fluid img-thumbnail">
                                                @endisset
                                            </td>
                                            <td>{{ $farm->title }} </td>
                                            @can('isAdmin') <td>{{ $farm->user->name ?? null }}</td> @endcan
                                            <td>
                                                <pre>{{ $farm->etp()->first()->name ?? null }}</pre>
                                            </td>
                                            <td align="center">
                                                <a href="{{ route('admin.farm.cows', $farm) }}">
                                                    {{ $farm->cows->count() }}
                                                </a>
                                            </td>
                                            <td>{{ $farm->type == 'farm' ? 'ฟาร์ม' : 'โรงเชือด' }}</td>
                                            <td width="100">
                                                <a href="{{ route('admin.farms.edit', ['farm' => $farm]) }} "
                                                    class=" btn btn-warning btn-sm mb-1"
                                                    style="width: 80px">{{ __('Edit') }}</a>
                                                <div class="mx-1"></div>
                                                <form action="{{ route('admin.farms.destroy', ['farm' => $farm]) }} "
                                                    method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class=" btn btn-danger btn-sm mb-1"
                                                        style="width: 80px"
                                                        onclick="return confirm('Are you sure?')">{{ __('Delete') }}</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-adminlte-layout>
