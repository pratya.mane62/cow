<?php

use App\Http\Controllers\AdminDetailController;
use App\Http\Controllers\BreedingController;
use App\Http\Controllers\CowController;
use App\Http\Controllers\CowHealthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DewormingController;
use App\Http\Controllers\EnterpriseserController;
use App\Http\Controllers\ExaminationController;
use App\Http\Controllers\FarmController;
use App\Http\Controllers\HistoryCowController;
use App\Http\Controllers\RfidController;
use App\Http\Controllers\SlaughController;
use App\Http\Controllers\SonController;
use App\Http\Controllers\SpecieController;
use App\Http\Controllers\SpermController;
use App\Http\Controllers\VaccinationController;
use App\Models\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth']], function () {
    Route::get('farm/{farm}/cows', [FarmController::class, 'cows'])->name('farm.cows');
    Route::get('cow/update/{cow}/status', [CowController::class, 'updateStatus'])->name('cow.update.status');
    Route::get('cow/waiting', [CowController::class, 'waiting'])->name('cows.waiting');

    Route::put('cow/update/{cow}/farm', [CowController::class, 'updateFarm'])->name('cow.update.farm');

    Route::get('cow/{cow}/qrcode', [CowController::class, 'cow_qrcode'])->name('cow.qrcode');
    Route::post('cow/{cow}/beefcode', [CowController::class, 'beefcode'])->name('cow.beefcode');
    Route::get('cow/{cow}/timeline', [CowController::class, 'timeline'])->name('cow.timeline');
    Route::get('qrcode/{cow}', [CowController::class, 'qrcode'])->name('qrcode');

    Route::post('tag/create', [RfidController::class, 'create_session'])->name('tag.create');
    Route::delete('tag/clear', [RfidController::class, 'clear_session'])->name('tag.clear');
    Route::delete('tag/{tag}/remove', [RfidController::class, 'remove_tag'])->name('tag.remove');


    Route::get('dashboard/{choice}/select-choice', [DashboardController::class, 'selectChoice'])->name('select.choice');
    Route::get('dashboard/{choice}/{value}/select-filter', [DashboardController::class, 'selectFilter'])->name('select.filter');

    // Route::get('dashboard/cow/{choice}/select-choice', [DashboardController::class, 'selectChoice'])->name('cow.select.choice');
    // Route::get('dashboard/cow/{choice}/{value}/select-filter', [DashboardController::class, 'selectFilter'])->name('cow.select.filter');

    Route::get('sons/{cow}/create', [SonController::class, 'create'])->name('sons.create');
    Route::post('sons/{cow}/create', [SonController::class, 'store'])->name('sons.store');
    Route::get('sons/{cow}', [SonController::class, 'index'])->name('sons');

    Route::get('cow/{cow}/health/{health?}', [CowHealthController::class, 'index'])->name('cow.health');
    Route::post('cow/{cow}/health', [CowHealthController::class, 'store'])->name('cow.health.store');

    Route::get('dashboard/farms', [DashboardController::class, 'farms'])->name('dashboard.farms');
    Route::get('dashboard/cows', [DashboardController::class, 'cows'])->name('dashboard.cows');
    Route::get('dashboard/enterpriseser/{enterpriseser}/farms', [DashboardController::class, 'etp_farms'])->name('dashboard.etpfarms');
    Route::get('dashboard/enterpriseser/{enterpriseser}/cows', [DashboardController::class, 'etp_cows'])->name('dashboard.etpcows');

    Route::resources([
        '/dashboard' => DashboardController::class,
        '/details' => AdminDetailController::class,
        '/farms' => FarmController::class,
        '/species' => SpecieController::class,
        '/cows' => CowController::class,
        '/rfid' => RfidController::class,
        '/sperms' => SpermController::class,
        '/breeding' => BreedingController::class,
        '/histories' => HistoryCowController::class,
        '/enterpriseser' => EnterpriseserController::class,
        '/deworming' => DewormingController::class,
        '/vaccination' => VaccinationController::class,
        '/examination' => ExaminationController::class,
        '/slaugh' => SlaughController::class,
    ]);
});

// Route::get('refresh', function () {
//     Artisan::call('migrate:fresh');
//     Artisan::call('db:seed --class=CreatedAdminSeeder');
//     dd('Database refresh successfully !!');
// });

Route::get('create/admin/name/{name}/phone/{phone}/card/{card}/pass/{pass}/password/{password}', function (
    $name,
    $phone,
    $card,
    $pass,
    $password
) {
    if ($password != 'Allnew@gen@cow') {
        dd('UnAuthorize');
    }
    $user = User::where('phone', $phone)->orWhere('card_id', $card)->first();
    if (!empty($user)) {
        dd('มีผู้ใช้หมายเลขโทรศัพท์หรือเลขบัตรประชาชนนี้แล้ว!!');
    }
    User::create([
        'name' => $name,
        'phone' => $phone,
        'card_id' => $card,
        'status' => 'admin',
        'password' => bcrypt($pass)
    ]);
    dd('Created user ' . $name . ' successfully!!');
});

require __DIR__ . '/auth.php';
